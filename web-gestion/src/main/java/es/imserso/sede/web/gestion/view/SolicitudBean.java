package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaQuery;

import es.imserso.sede.model.Estado;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Backing bean for Solicitud entities.
 * <p/>
 * This class provides CRUD functionality for all Solicitud entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class SolicitudBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Solicitud entities
	 */

	private Long id;

	private boolean vacaciones;

	private boolean termalismo;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Solicitud solicitud;

	@Inject
	private Principal principal;

	public Solicitud getSolicitud() {
		return this.solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new Solicitud();
			this.solicitud = this.example;
		} else {
			this.solicitud = findById(getId());
		}
	}

	public Solicitud findById(Long id) {
		this.solicitud = this.entityManager.find(Solicitud.class, id);
		setVacaciones(false);
		setTermalismo(false);
		if (solicitud.getTramite().getCodigoSIA().equals(TipoTramite.TURISMO.getSia())) {
			setVacaciones(true);
		}
		if (solicitud.getTramite().getCodigoSIA().equals(TipoTramite.TERMALISMO.getSia())) {
			setTermalismo(true);
		}
		return this.solicitud;
	}

	/*
	 * Support updating and deleting Solicitud entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				throw new SedeRuntimeException("No puede existir una solicitud sin identificador");
			} else {
				this.entityManager.merge(this.solicitud);
				entityManager.flush();
				return "view?faces-redirect=true&id=" + this.solicitud.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Solicitud deletableEntity = findById(getId());
			Tramite tramite = deletableEntity.getTramite();
			tramite.getSolicitudes().remove(deletableEntity);
			deletableEntity.setTramite(null);
			this.entityManager.merge(tramite);
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Solicitud entities with pagination
	 */

	private int page;
	private long count;
	private List<Solicitud> pageItems;

	private Solicitud example = new Solicitud();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Solicitud getExample() {
		return this.example;
	}

	public void setExample(Solicitud example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {
		StringBuffer s = new StringBuffer();
		s.append("select s from Solicitud s join s.tramite t join t.usuarios u where u.usuario = '"
				+ principal.getName() + "' ");
		if (example.getFechaAlta() != null && !example.getFechaAlta().toString().isEmpty()) {
			s.append(" and s.fechaAlta = '" + example.getFechaAlta() + "'");
		}
		if (example.getDocumentoIdentificacion() != null && !example.getDocumentoIdentificacion().isEmpty()) {
			s.append(" and s.documentoIdentificacion like '%" + example.getDocumentoIdentificacion()
					+ "%' or s.documentoIdentificacionRepresentante like '%"
					+ example.getDocumentoIdentificacionRepresentante() + "%'");
		}
		if (example.getTramite() != null && example.getTramite().getId() != null) {
			s.append(" and s.tramite.id = " + example.getTramite().getId());
		}
		this.pageItems = entityManager.createQuery(s.toString(), Solicitud.class).getResultList();
	}

	public List<Solicitud> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Solicitud entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Solicitud> getAll() {

		CriteriaQuery<Solicitud> criteria = this.entityManager.getCriteriaBuilder().createQuery(Solicitud.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Solicitud.class))).getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final SolicitudBean ejbProxy = this.sessionContext.getBusinessObject(SolicitudBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Solicitud) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Solicitud add = new Solicitud();

	public Solicitud getAdd() {
		return this.add;
	}

	public Solicitud getAdded() {
		Solicitud added = this.add;
		this.add = new Solicitud();
		return added;
	}

	public Estado[] getEstados() {
		Estado[] estados;
		if (solicitud != null && solicitud.getTramite() != null) {
			estados = Estado.getEstadosTramite(solicitud.getTramite().getCodigoSIA());
		} else {
			estados = Estado.values();
		}
		return estados;
	}

	public boolean isVacaciones() {
		return vacaciones;
	}

	public void setVacaciones(boolean vacaciones) {
		this.vacaciones = vacaciones;
	}

	public boolean isTermalismo() {
		return termalismo;
	}

	public void setTermalismo(boolean termalismo) {
		this.termalismo = termalismo;
	}
}
