package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.Part;

import es.imserso.sede.model.PlantillaPdf;
import es.imserso.sede.util.Utils;
import es.imserso.sede.model.Idioma;

/**
 * Backing bean for PlantillaPdf entities.
 * <p/>
 * This class provides CRUD functionality for all PlantillaPdf entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PlantillaPdfBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Part uploadedFile;
	
	/*
	 * Support creating and retrieving PlantillaPdf entities
	 */

	public Part getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(Part uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private PlantillaPdf plantillaPdf;

	public PlantillaPdf getPlantillaPdf() {
		return this.plantillaPdf;
	}

	public void setPlantillaPdf(PlantillaPdf plantillaPdf) {
		this.plantillaPdf = plantillaPdf;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new PlantillaPdf();
			this.plantillaPdf = this.example;
		} else {
			this.plantillaPdf = findById(getId());
		}
	}

	public PlantillaPdf findById(Long id) {

		return this.entityManager.find(PlantillaPdf.class, id);
	}

	/*
	 * Support updating and deleting PlantillaPdf entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.plantillaPdf.setValor(Utils.getBytes(uploadedFile.getInputStream()));
				this.plantillaPdf.setIdioma(entityManager.find(Idioma.class, this.plantillaPdf.getIdioma().getId()));
				this.entityManager.persist(this.plantillaPdf);
				this.entityManager.flush();
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.plantillaPdf);
				return "view?faces-redirect=true&id="
						+ this.plantillaPdf.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			PlantillaPdf deletableEntity = findById(getId());
			Idioma idioma = deletableEntity.getIdioma();
			idioma.getPlantillasPdf().remove(deletableEntity);
			deletableEntity.setIdioma(null);
			this.entityManager.merge(idioma);
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching PlantillaPdf entities with pagination
	 */

	private int page;
	private long count;
	private List<PlantillaPdf> pageItems;

	private PlantillaPdf example = new PlantillaPdf();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public PlantillaPdf getExample() {
		return this.example;
	}

	public void setExample(PlantillaPdf example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<PlantillaPdf> root = countCriteria.from(PlantillaPdf.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<PlantillaPdf> criteria = builder
				.createQuery(PlantillaPdf.class);
		root = criteria.from(PlantillaPdf.class);
		TypedQuery<PlantillaPdf> query = this.entityManager
				.createQuery(criteria.select(root).where(
						getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<PlantillaPdf> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		byte[] valor = this.example.getValor();
		if (valor != null) {
			predicatesList.add(builder.equal(root.get("valor"), valor));
		}
		String nombre = this.example.getNombre();
		if (nombre != null && !"".equals(nombre)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("nombre")),
					'%' + nombre.toLowerCase() + '%'));
		}
		String descripcion = this.example.getDescripcion();
		if (descripcion != null && !"".equals(descripcion)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("descripcion")),
					'%' + descripcion.toLowerCase() + '%'));
		}
		Idioma idioma = this.example.getIdioma();
		if (idioma != null) {
			predicatesList.add(builder.equal(root.get("idioma"), idioma));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<PlantillaPdf> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back PlantillaPdf entities (e.g. from inside
	 * an HtmlSelectOneMenu)
	 */

	public List<PlantillaPdf> getAll() {

		CriteriaQuery<PlantillaPdf> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(PlantillaPdf.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(PlantillaPdf.class)))
				.getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final PlantillaPdfBean ejbProxy = this.sessionContext
				.getBusinessObject(PlantillaPdfBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((PlantillaPdf) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private PlantillaPdf add = new PlantillaPdf();

	public PlantillaPdf getAdd() {
		return this.add;
	}

	public PlantillaPdf getAdded() {
		PlantillaPdf added = this.add;
		this.add = new PlantillaPdf();
		return added;
	}
}
