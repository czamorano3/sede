package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import es.imserso.sede.model.JustificantePdf;
import es.imserso.sede.model.PlantillaEmail;
import es.imserso.sede.model.SolicitudPdf;
import es.imserso.sede.model.Tramite;

/**
 * Backing bean for Tramite entities.
 * <p/>
 * This class provides CRUD functionality for all Tramite entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class TramiteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Tramite entities
	 */

	private Long id;
	
	@Inject
	private Principal principal;
	

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Tramite tramite;

	public Tramite getTramite() {
		return this.tramite;
	}

	public void setTramite(Tramite tramite) {
		this.tramite = tramite;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new Tramite();
			this.tramite = this.example;
		} else {
			this.tramite = findById(getId());
		}
	}

	public Tramite findById(Long id) {

		return this.entityManager.find(Tramite.class, id);
	}

	/*
	 * Support updating and deleting Tramite entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				tramite.setJustificantePdf(entityManager.find(JustificantePdf.class, tramite.getJustificantePdf().getId()));
				tramite.setSolicitudPdf(entityManager.find(SolicitudPdf.class, tramite.getSolicitudPdf().getId()));
				tramite.setPlantillaEmail(entityManager.find(PlantillaEmail.class, tramite.getPlantillaEmail().getId()));				
				this.entityManager.persist(this.tramite);
				this.entityManager.flush();
				return "search?faces-redirect=true";
			} else {				
				tramite.setJustificantePdf(entityManager.find(JustificantePdf.class, tramite.getJustificantePdf().getId()));
				tramite.setSolicitudPdf(entityManager.find(SolicitudPdf.class, tramite.getSolicitudPdf().getId()));
				tramite.setPlantillaEmail(entityManager.find(PlantillaEmail.class, tramite.getPlantillaEmail().getId()));
				this.entityManager.merge(this.tramite);
				this.entityManager.flush();
				return "view?faces-redirect=true&id=" + this.tramite.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Tramite deletableEntity = findById(getId());			
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Tramite entities with pagination
	 */

	private int page;
	private long count;
	private List<Tramite> pageItems;

	private Tramite example = new Tramite();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Tramite getExample() {
		return this.example;
	}

	public void setExample(Tramite example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Tramite> root = countCriteria.from(Tramite.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Tramite> criteria = builder.createQuery(Tramite.class);
		root = criteria.from(Tramite.class);
		TypedQuery<Tramite> query = this.entityManager.createQuery(criteria
				.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
		System.out.println("");
	}

	private Predicate[] getSearchPredicates(Root<Tramite> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String codigoSIA = this.example.getCodigoSIA();
		if (codigoSIA != null && !"".equals(codigoSIA)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("codigoSIA")),
					'%' + codigoSIA.toLowerCase() + '%'));
		}
		String descripcion = this.example.getDescripcion();
		if (descripcion != null && !"".equals(descripcion)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("descripcion")),
					'%' + descripcion.toLowerCase() + '%'));
		}
		String nombre = this.example.getNombre();
		if (nombre != null && !"".equals(nombre)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("nombre")),
					'%' + nombre.toLowerCase() + '%'));
		}
		JustificantePdf justificantePdf = this.example.getJustificantePdf();
		if (justificantePdf != null) {
			predicatesList.add(builder.equal(root.get("justificantePdf"),
					justificantePdf));
		}
		SolicitudPdf solicitudPdf = this.example.getSolicitudPdf();
		if (solicitudPdf != null) {
			predicatesList.add(builder.equal(root.get("solicitudPdf"),
					solicitudPdf));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Tramite> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Tramite entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Tramite> getAll() {

		CriteriaQuery<Tramite> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Tramite.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Tramite.class))).getResultList();
	}
	
	/*
	 * Método que devuelve los trámites del usuario conectado
	 */
	public List<Tramite> getTramiteUsuarioPrincipal(){
		return this.entityManager.createQuery("select t from Tramite t inner join t.usuarios u where u.usuario = '"+principal.getName()+"'").getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final TramiteBean ejbProxy = this.sessionContext
				.getBusinessObject(TramiteBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Tramite) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Tramite add = new Tramite();

	public Tramite getAdd() {
		return this.add;
	}

	public Tramite getAdded() {
		Tramite added = this.add;
		this.add = new Tramite();
		return added;
	}
}
