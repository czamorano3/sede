package es.imserso.sede.web.gestion.util;


import javax.faces.bean.RequestScoped;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import javax.servlet.http.HttpServletRequest;


import org.jboss.logging.Logger;

@Named
@RequestScoped
public class LoginBean {
	
	@Inject
	private Logger log;
	




	
	/**
	 * termina la sesión
	 */
	public String logout() {
		log.info("se va a hacer el logout de la sesión...");
		
		((HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest()).getSession().invalidate();
		log.info("sesión invalidada!");
		
		return "home";
	}
}
