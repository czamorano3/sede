package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import es.imserso.sede.model.PlantillaEmail;
import es.imserso.sede.model.Idioma;

/**
 * Backing bean for PlantillaEmail entities.
 * <p/>
 * This class provides CRUD functionality for all PlantillaEmail entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PlantillaEmailBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving PlantillaEmail entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private PlantillaEmail plantillaEmail;

	public PlantillaEmail getPlantillaEmail() {
		return this.plantillaEmail;
	}

	public void setPlantillaEmail(PlantillaEmail plantillaEmail) {
		this.plantillaEmail = plantillaEmail;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}
	
	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new PlantillaEmail();
			this.plantillaEmail = this.example;
		} else {
			this.plantillaEmail = findById(getId());
		}
	}

	public PlantillaEmail findById(Long id) {

		return this.entityManager.find(PlantillaEmail.class, id);
	}

	/*
	 * Support updating and deleting PlantillaEmail entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				plantillaEmail.setIdioma(entityManager.find(Idioma.class, plantillaEmail.getIdioma().getId()));
				this.entityManager.persist(this.plantillaEmail);
				this.entityManager.flush();
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.plantillaEmail);
				this.entityManager.flush();
				return "view?faces-redirect=true&id="
						+ this.plantillaEmail.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			PlantillaEmail deletableEntity = findById(getId());
			Idioma idioma = deletableEntity.getIdioma();
			idioma.getPlantillasEmail().remove(deletableEntity);
			deletableEntity.setIdioma(null);
			this.entityManager.merge(idioma);
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching PlantillaEmail entities with pagination
	 */

	private int page;
	private long count;
	private List<PlantillaEmail> pageItems;

	private PlantillaEmail example = new PlantillaEmail();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public PlantillaEmail getExample() {
		return this.example;
	}

	public void setExample(PlantillaEmail example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<PlantillaEmail> root = countCriteria.from(PlantillaEmail.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<PlantillaEmail> criteria = builder
				.createQuery(PlantillaEmail.class);
		root = criteria.from(PlantillaEmail.class);
		TypedQuery<PlantillaEmail> query = this.entityManager
				.createQuery(criteria.select(root).where(
						getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<PlantillaEmail> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String texto = this.example.getTexto();
		if (texto != null && !"".equals(texto)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("texto")),
					'%' + texto.toLowerCase() + '%'));
		}
		Idioma idioma = this.example.getIdioma();
		if (idioma != null) {
			predicatesList.add(builder.equal(root.get("idioma"), idioma));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<PlantillaEmail> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back PlantillaEmail entities (e.g. from
	 * inside an HtmlSelectOneMenu)
	 */

	public List<PlantillaEmail> getAll() {

		CriteriaQuery<PlantillaEmail> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(PlantillaEmail.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(PlantillaEmail.class)))
				.getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final PlantillaEmailBean ejbProxy = this.sessionContext
				.getBusinessObject(PlantillaEmailBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((PlantillaEmail) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private PlantillaEmail add = new PlantillaEmail();

	public PlantillaEmail getAdd() {
		return this.add;
	}

	public PlantillaEmail getAdded() {
		PlantillaEmail added = this.add;
		this.add = new PlantillaEmail();
		return added;
	}
}
