package es.imserso.sede.config;

import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Esta clase y sus subclases son una forma de {@code InventarioException} que
 * indica problemas de configuración que deben ser controladas.
 * 
 * @author 11825775
 *
 */
public class ConfigurationException extends SedeRuntimeException {

	private static final long serialVersionUID = 2725609829560649698L;

	public ConfigurationException() {
		super("Ocurrió una excepción en la configuración de la aplicación");
	}

	public ConfigurationException(String message) {
		super(message);
	}

	public ConfigurationException(Exception e) {
		super(e.toString());
		this.setStackTrace(e.getStackTrace());
	}

}
