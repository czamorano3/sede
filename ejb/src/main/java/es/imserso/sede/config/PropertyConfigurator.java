package es.imserso.sede.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import es.imserso.sede.util.Utils;

/**
 * lleva al componente propertyComponent las diferentes propiedades de la
 * configuración: - {configuracion}/inventario/config/application.properties
 * <p>
 * Tras este componente se ejecutará el ConfigValidator
 * 
 * @author 11825775
 *
 */
@ApplicationScoped
@Named
public class PropertyConfigurator {

	private Logger logger = Logger.getLogger(this.getClass());

	@Inject
	private Configuracion configuracion;

	@Inject
	PropertyComponent propertyComponent;

	public PropertyConfigurator() {
	}

	/**
	 * Carga las propiedades en el componente PropertyComponent.
	 */
	public void configure() {
		String currentProperty;

		try {

			// application.conf.report.engineHome
			currentProperty = "application.conf.report.engineHome";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setReportEngineHome(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.JNDIuserTransactionName
			currentProperty = "application.conf.JNDIuserTransactionName";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setUserTransactionJndiName(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.JNDItransactionManagerName
			currentProperty = "application.conf.JNDItransactionManagerName";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setTransactionManagerJndiName(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.iterationsToFlush
			currentProperty = "application.conf.iterationsToFlush";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setIterationsToFlush(Integer.parseInt(configuracion.getPropiedad(currentProperty)));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.procesos.transactionTimeoutSeconds
			currentProperty = "application.conf.procesos.transactionTimeoutSeconds";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setProcessTransactionTimeoutSeconds(
						Integer.parseInt(configuracion.getPropiedad(currentProperty)));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.report.transactionTimeoutSeconds
			currentProperty = "application.conf.report.transactionTimeoutSeconds";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setReportTransactionTimeoutSeconds(
						Integer.parseInt(configuracion.getPropiedad(currentProperty)));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.data.input.folder
			currentProperty = "application.data.input.folder";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setInputFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.data.output.folder
			currentProperty = "application.data.output.folder";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setOutputFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.data.report.folder
			currentProperty = "application.data.report.folder";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setReportFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.data.temp.folder
			currentProperty = "application.data.temp.folder";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setTempFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.data.test.folder
			currentProperty = "application.data.test.folder";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setTestFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.data.output.bufferSize
			currentProperty = "application.data.output.bufferSize";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent
						.setDataOutputBufferSize(Integer.parseInt(configuracion.getPropiedad(currentProperty)));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.report.logFolder
			currentProperty = "application.report.logFolder";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setReportLogFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.resources.img.logoimserso
			currentProperty = "application.conf.resources.img.logoimserso";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setRutaImagenLogoImserso(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.resources.css.estilosImsersoBirt
			currentProperty = "application.conf.resources.css.estilosImsersoBirt";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setRutaHojaEstilosImserso(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.acceptedTypes
			currentProperty = "application.conf.acceptedTypes";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setAcceptedTypes(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.report.reportIterations
			currentProperty = "application.conf.report.reportIterations";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent
						.setReportScriptIterations(Integer.parseInt(configuracion.getPropiedad(currentProperty)));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.report.iterationsToNotifyReportProgressBar
			currentProperty = "application.conf.report.iterationsToNotifyReportProgressBar";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setReportIterationsToNotifyReportProgressBar(
						Integer.parseInt(configuracion.getPropiedad(currentProperty)));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.report.outputPath
			currentProperty = "application.conf.report.outputPath";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setReportOutputFolder(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			// application.conf.email.planificadores
			currentProperty = "application.conf.email.planificadores";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setEmailPlanificadores(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			// application.conf.email.errorAccount
			currentProperty = "application.conf.email.errorAccount";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setEmailSistemas(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			// correo.server
			currentProperty = "correo.server";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setMailServer(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.email.pop3Host
			currentProperty = "application.conf.email.pop3Host";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setPop3Host(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.email.pop3Port
			currentProperty = "application.conf.email.pop3Port";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setPop3Port(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.email.smtpHost
			currentProperty = "application.conf.email.smtpHost";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setSmtpHost(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.email.smtpPort
			currentProperty = "application.conf.email.smtpPort";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setSmtpPort(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			//application.conf.signature.test.url
			currentProperty = "application.conf.signature.test.url";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setUrlDocumentSignatureServicesTest(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			//application.conf.signature.alive.url
			currentProperty = "application.conf.signature.alive.url";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setUrlDocumentSignatureIsAlive(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.signature.url
			currentProperty = "application.conf.signature.url";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setUrlDocumentSignatureService(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			// application.conf.ucm.tramiteinfo.url
			currentProperty = "application.conf.ucm.tramiteinfo.url";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setTramiteUCMservice(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.registers.serviceName
			currentProperty = "application.conf.isicres.registers.serviceName";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresRegistersServiceName(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			
			// application.conf.isicres.registers.qname.nameSpaceURI
			currentProperty = "application.conf.isicres.registers.qname.nameSpaceURI";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresRegistersNamespaceURI(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			// application.conf.isicres.registers.qname.localPart
			currentProperty = "application.conf.isicres.registers.qname.localPart";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresRegistersQNameLocalPart(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.registers.targetNamespace
			currentProperty = "application.conf.isicres.registers.targetNamespace";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresRegistersTargetNamespace(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.registers.wsdlUrl
			currentProperty = "application.conf.isicres.registers.wsdlUrl";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresRegistersWsdlUrl(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.books.serviceName
			currentProperty = "application.conf.isicres.books.serviceName";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresBooksServiceName(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			// application.conf.isicres.books.qname.nameSpaceURI
			currentProperty = "application.conf.isicres.books.qname.nameSpaceURI";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresBooksNamespaceURI(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			// application.conf.isicres.books.qname.localPart
			currentProperty = "application.conf.isicres.books.qname.localPart";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresBooksQNameLocalPart(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.books.targetNamespace
			currentProperty = "application.conf.isicres.books.targetNamespace";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresBooksTargetNamespace(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.books.wsdlUrl
			currentProperty = "application.conf.isicres.books.wsdlUrl";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresBooksWsdlUrl(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.username
			currentProperty = "application.conf.isicres.service.username";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresUsername(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.password
			currentProperty = "application.conf.isicres.service.password";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresPassword(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.sender
			currentProperty = "application.conf.isicres.service.sender";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresSender(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.destination
			currentProperty = "application.conf.isicres.service.destination";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresDestination(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.transportType
			currentProperty = "application.conf.isicres.service.transportType";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresTransportType(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.transportNumber
			currentProperty = "application.conf.isicres.service.transportNumber";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresTransportNumber(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.matterType
			currentProperty = "application.conf.isicres.service.matterType";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setiSicresMatterType(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}

			// application.conf.isicres.service.nombreDocumentoSolicitud
//			currentProperty = "application.conf.isicres.service.nombreDocumentoSolicitud";
//			logger.debug("comprobando " + currentProperty + " ...");
//			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
//				propertyComponent.setiSicresNombreDocumentoSolicitud(configuracion.getPropiedad(currentProperty));
//			} else {
//				throw new ConfigurationException("El valor de la propiedad " + currentProperty
//						+ " del fichero application.properties es incorrecto");
//			}

			// application.conf.rest.resource.turismo.url
			currentProperty = "application.conf.rest.resource.turismo.url";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setTurismoResourcesURL(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			// application.conf.rest.resource.termalismo.url
			currentProperty = "application.conf.rest.resource.termalismo.url";
			logger.debug("comprobando " + currentProperty + " ...");
			if (!Utils.isEmpty(configuracion.getPropiedad(currentProperty))) {
				propertyComponent.setTermalismoResourcesURL(configuracion.getPropiedad(currentProperty));
			} else {
				throw new ConfigurationException("El valor de la propiedad " + currentProperty
						+ " del fichero application.properties es incorrecto");
			}
			

		} catch (ConfigurationException e) {
			Utils.configurationErrorDetected(Utils.getExceptionMessage(e));

		} catch (Exception e) {
			throw new ConfigurationException(e.toString());
		}
	}

	@AppConfig
	@ApplicationScoped
	@Produces
	public PropertyComponent getPropertyComponent() {
		return propertyComponent;
	}

}
