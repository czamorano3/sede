package es.imserso.sede.model;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.TemporalType.TIMESTAMP;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import es.imserso.sede.service.registration.event.RegistrationPhase;

/**
 * Entity implementation class for Entity: RegistroTramiteFase
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_REGISTRO_TRAMITE_FASE", initialValue = 1, allocationSize = 1, sequenceName = "SEC_REGISTRO_TRAMITE_FASE")
public class RegistroTramiteFase implements Serializable, Cloneable {

	private static final long serialVersionUID = 2730731840047908542L;

	private Long id;

	private RegistrationPhase phase;

	private Date fecha;

	private String datos;

	private RegistroTramite registroTramite;

	private Date oplock;

	public RegistroTramiteFase() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REGISTRO_TRAMITE_FASE")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPhase(RegistrationPhase fase) {
		this.phase = fase;
	}

	@Length(max = 1000)
	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = { PERSIST, MERGE, REFRESH })
	public RegistroTramite getRegistroTramite() {
		return registroTramite;
	}

	public void setRegistroTramite(RegistroTramite param) {
		this.registroTramite = param;
	}

	@Temporal(TIMESTAMP)
	@Version
	public Date getOplock() {
		return oplock;
	}

	public void setOplock(Date param) {
		this.oplock = param;
	}

	/**
	 * @return the fecha
	 */
	@Temporal(TIMESTAMP)
	@NotNull
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the phase
	 */
	@NotNull
	@Enumerated(STRING)
	public RegistrationPhase getPhase() {
		return phase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datos == null) ? 0 : datos.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((phase == null) ? 0 : phase.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroTramiteFase other = (RegistroTramiteFase) obj;
		if (datos == null) {
			if (other.datos != null)
				return false;
		} else if (!datos.equals(other.datos))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (phase != other.phase)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegistroTramiteFase [id=" + id + ", phase=" + phase + ", fecha=" + fecha + ", datos=" + datos + "]";
	}

}
