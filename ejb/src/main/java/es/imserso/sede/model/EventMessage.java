package es.imserso.sede.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.message.event.EventMessageSource;

/**
 * Representa un mensaje de un evento ocurrido en el sistema.
 * 
 * @author 11825775
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_EVENT_MESSAGE", initialValue = 1, allocationSize = 1, sequenceName = "SEC_EVENT_MESSAGE")
@Table(name = "ERROR_MESSAGE")
@NamedQueries({ @NamedQuery(name = "EventMessage.getAll", query = "select em from EventMessage em order by em.id desc"),
		@NamedQuery(name = "EventMessage.getError", query = "select em from EventMessage em where em.level = 4 order by em.id desc"),
		@NamedQuery(name = "EventMessage.getFatal", query = "select em from EventMessage em where em.level = 5 order by em.id desc"),
		@NamedQuery(name = "EventMessage.getErrorAndFatal", query = "select em from EventMessage em where em.level > 3 order by em.id desc") })
public class EventMessage implements Serializable {

	private static final long serialVersionUID = 8520627440003369675L;

	private Long id;

	/**
	 * origen del error
	 */
	private EventMessageSource source = EventMessageSource.APLICACION;

	/**
	 * nivel del error
	 */
	private EventMessageLevel level = EventMessageLevel.MESSAGE_LEVEL_ERROR;

	/**
	 * texto del mensaje
	 */
	private String message;

	/**
	 * descripción detallada del error
	 */
	private String description;

	/**
	 * fecha y hora del mensaje
	 */
	private Date logTime;

	/**
	 * usuario de la sesión (si hubiese cookie)
	 */
	private String usuario;

	/**
	 * valor del parámetro <b>locale</b>
	 */
	private String locale;

	/**
	 * valor del parámetro <b>action</b>
	 */
	private TipoAccion action;

	/**
	 * valor del parámetro <b>representante</b>
	 */
	private String representante;

	/**
	 * valor del parámetro <b>sia</b>
	 */
	private String sia;

	/**
	 * valor del parámetro <b>aut</b>
	 */
	private String aut;

	/**
	 * valor de jSessionId
	 */
	private String sessionId;

	private Date oplock;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EVENT_MESSAGE")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public EventMessage() {
		
	}

	/**
	 * @param source
	 * @param level nivel del error
	 * @param message mensaje de error
	 * @param description descripción detallada del error
	 */
	public EventMessage(EventMessageSource source, EventMessageLevel level, String message, String description) {
		setSource(source);
		setLevel(level);
		setMessage(message);
		setLogTime(new Date(System.currentTimeMillis()));
		setDescription(description);
	}

	/**
	 * @param source
	 * @param level nivel del error
	 * @param message mensaje de error
	 */
	public EventMessage(EventMessageSource source, EventMessageLevel level, String message) {
		setSource(source);
		setLevel(level);
		setMessage(message);
		setLogTime(new Date(System.currentTimeMillis()));
	}

	/**
	 * @param level nivel del error
	 * @param message mensaje de error
	 */
	public EventMessage(EventMessageLevel level, String message) {
		setSource(EventMessageSource.APLICACION);
		setLevel(level);
		setMessage(message);
		setLogTime(new Date(System.currentTimeMillis()));
	}
	
	/**
	 * @param level nivel del error
	 * @param message mensaje de error
	 * @param description descripción detallada del error
	 */
	public EventMessage(EventMessageLevel level, String message, String description) {
		setSource(EventMessageSource.APLICACION);
		setLevel(level);
		setMessage(message);
		setDescription(description);
		setLogTime(new Date(System.currentTimeMillis()));
	}

	/**
	 * @return origen del mensaje
	 */
	@Enumerated(EnumType.STRING)
	public EventMessageSource getSource() {
		return source;
	}

	/**
	 * @param source
	 */
	public void setSource(EventMessageSource source) {
		this.source = source;
	}

	/**
	 * @return nivel del mensaje
	 */
	@Enumerated(EnumType.STRING)
	@NotNull
	public EventMessageLevel getLevel() {
		return level;
	}

	/**
	 * @param level
	 */
	public void setLevel(EventMessageLevel level) {
		this.level = level;
	}

	/**
	 * @return mensaje
	 */
	@NotNull
	public String getMessage() {
		return message;
	}

	/**
	 * @param message mensaje de error
	 */
	public void setMessage(String message) {
		// message = Utils.encode(message);
		this.message = message;
	}

	/**
	 * @return descripción detallada del error
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description descripción detallada del error
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the logTime
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLogTime() {
		return logTime;
	}

	/**
	 * @param logTime
	 *            fecha del evevnto
	 */
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	@Version
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOplock() {
		return oplock;
	}

	public void setOplock(Date oplock) {
		this.oplock = oplock;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Enumerated(EnumType.STRING)
	public TipoAccion getAction() {
		return action;
	}

	public void setAction(TipoAccion action) {
		this.action = action;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getSia() {
		return sia;
	}

	public void setSia(String sia) {
		this.sia = sia;
	}

	public String getAut() {
		return aut;
	}

	public void setAut(String aut) {
		this.aut = aut;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
