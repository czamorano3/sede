/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Entidad que contiene los cambios de estado de las solicitudes
 *
 * @author 11825775
 */
@Entity
@SequenceGenerator(name = "SEQ_HISTORICOESTADOSOLICITUD", initialValue = 1, allocationSize = 1, sequenceName = "SEC_HISTORICOESTADOSOLICITUD")
public class HistoricoEstadoSolicitud implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public HistoricoEstadoSolicitud() {
	}

	/**
	 * ------------------------------------------ The primary key.
	 *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HISTORICOESTADOSOLICITUD")
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	private Long id = null;

	/**
	 * ------------------------------------------ usuario que ha realizado el
	 * cambio de estado de la solicitud
	 *
	 */
	@NotNull
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}

	private String usuario = null;

	/**
	 * ------------------------------------------ solicitud cuyo estado se ha
	 * cambiado
	 *
	 */
	@ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(final Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	private Solicitud solicitud = null;

	/**
	 * ------------------------------------------ fecha de la modificacion del
	 * estado de la solicitud
	 *
	 */
	@Temporal(value = TemporalType.TIMESTAMP)
	@NotNull
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(final Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	private Date fechaModificacion = null;

	/**
	 * ------------------------------------------ The optimistic lock property.
	 *
	 */
	@Version
	public Long getOplock() {
		return oplock;
	}

	public void setOplock(final Long oplock) {
		this.oplock = oplock;
	}

	private Long oplock = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 */
	@NotNull(message = "debe especificarse el estado")
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(final Estado estado) {
		this.estado = estado;
	}

	@Transient
	public boolean isPENDIENTE() {
		return Estado.PENDIENTE.equals(estado);
	}

	@Transient
	public boolean isAPROBADA() {
		return Estado.APROBADA.equals(estado);
	}

	@Transient
	public boolean isRECHAZADA() {
		return Estado.RECHAZADA.equals(estado);
	}

	@Transient
	public boolean isCANCELADA() {
		return Estado.CANCELADA.equals(estado);
	}

	@Transient
	public boolean isTRAMITANDOSE() {
		return Estado.TRAMITANDOSE.equals(estado);
	}

	@Transient
	public boolean isGESTIONADOPORAPLICACION() {
		return Estado.GESTIONADO_POR_APLICACION.equals(estado);
	}

	private Estado estado = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------
	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		builder.append("id", getId());
		builder.append("usuario", getUsuario());
		builder.append("fechaModificacion", getFechaModificacion());
		builder.append("oplock", getOplock());
		builder.append("estado", getEstado());
		builder.append("solicitud", getSolicitud());
		return builder.toString();
	}

	public HistoricoEstadoSolicitud deepClone() throws Exception {
		HistoricoEstadoSolicitud clone = (HistoricoEstadoSolicitud) super.clone();
		clone.setId(null);
		return clone;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HistoricoEstadoSolicitud))
			return false;
		final HistoricoEstadoSolicitud other = (HistoricoEstadoSolicitud) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}
