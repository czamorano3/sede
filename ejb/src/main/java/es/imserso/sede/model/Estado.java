/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

/**
 * Estados posibles de una solicitud.
 *
 * @author 11825775
 * @NOT generated
 */
public enum Estado {
	/** @NOT generated */
	PENDIENTE("Pendiente"),
	/** @NOT generated */
	APROBADA("Aprobada"),
	/** @NOT generated */
	RECHAZADA("Rechazada"),
	/** @NOT generated */
	CANCELADA("Cancelada"),
	/** @NOT generated */
	TRAMITANDOSE("Tramitándose"),
	/** @NOT generated */
	GESTIONADO_POR_APLICACION("Gestionado por aplicación");

	/** @NOT generated */
	private String descripcion;

	/** @NOT generated */
	Estado(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/** @NOT generated */
	public static Estado[] getEstadosTramite(String codigoSIA) {
		Estado[] values;
		if (codigoSIA.equals(TipoTramite.TURISMO.getSia())
				|| codigoSIA.equals(TipoTramite.TERMALISMO.toString())) {
			values = new Estado[] { Estado.GESTIONADO_POR_APLICACION };
		} else {
			values = new Estado[] { Estado.APROBADA, Estado.CANCELADA, Estado.PENDIENTE, Estado.RECHAZADA,
					Estado.TRAMITANDOSE };
		}
		return values;
	}

	/** @NOT generated */
	public static Estado getEstadoInicial(String codigoSIA) {
		Estado value;
		if (codigoSIA.equals(TipoTramite.TURISMO.getSia())
				|| codigoSIA.equals(TipoTramite.TERMALISMO.toString())) {
			value = Estado.GESTIONADO_POR_APLICACION;
		} else {
			value = Estado.PENDIENTE;
		}
		return value;
	}

}
