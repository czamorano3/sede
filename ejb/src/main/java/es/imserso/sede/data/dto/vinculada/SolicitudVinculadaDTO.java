package es.imserso.sede.data.dto.vinculada;

import java.io.Serializable;

import javax.enterprise.context.Dependent;

import org.apache.commons.lang.StringUtils;

/**
 * Datos para vincular la solicitud
 * 
 * @author 11825775
 *
 */
@Dependent
public class SolicitudVinculadaDTO implements SolicitudVinculadaDTOI, Serializable {

	private static final long serialVersionUID = 4362806142137856754L;

	private String nombre;
	private String apellido1;
	private String apellido2;
	private String documentoIdentificacion;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudVinculadaDTOI#getNombre()
	 */
	@Override
	public String getNombre() {
		return nombre;
	}

	@Override
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudVinculadaDTOI#getApellido1()
	 */
	@Override
	public String getApellido1() {
		return apellido1;
	}

	@Override
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudVinculadaDTOI#getApellido2()
	 */
	@Override
	public String getApellido2() {
		return apellido2;
	}

	@Override
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.data.dto.SolicitudVinculadaDTOI#getDocumentoIdentificacion()
	 */
	@Override
	public String getDocumentoIdentificacion() {
		return documentoIdentificacion;
	}

	@Override
	public void setDocumentoIdentificacion(String documentoIdentificacion) {
		this.documentoIdentificacion = documentoIdentificacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.Dirty#isDirty()
	 */
	@Override
	public Boolean isDirty() {
		return StringUtils.isNotBlank(getNombre()) || StringUtils.isNotBlank(getApellido1())
				|| StringUtils.isNotBlank(getApellido2()) || StringUtils.isNotBlank(getDocumentoIdentificacion());
	}

}
