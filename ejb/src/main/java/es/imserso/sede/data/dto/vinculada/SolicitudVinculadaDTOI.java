package es.imserso.sede.data.dto.vinculada;

import org.hibernate.validator.constraints.NotBlank;

import es.imserso.sede.data.Dirty;
import es.imserso.sede.data.validation.nif.NifNie;

/**
 * Datos de la solicitud vinculada
 * 
 * @author 11825775
 *
 */
public interface SolicitudVinculadaDTOI extends Dirty {

	@NotBlank(message = "Debe especificarse el nombre de la persona")
	String getNombre();

	@NotBlank(message = "Debe especificarse el primer apellido de la persona")
	String getApellido1();

	String getApellido2();

	@NotBlank(message = "Debe especificarse el documento de identificación de la persona")
	@NifNie(message = "El documento de identificación de la persona interesada no es válido")
	String getDocumentoIdentificacion();

	void setDocumentoIdentificacion(String documentoIdentificacion);

	void setApellido2(String apellido2);

	void setApellido1(String apellido1);

	void setNombre(String nombre);

}
