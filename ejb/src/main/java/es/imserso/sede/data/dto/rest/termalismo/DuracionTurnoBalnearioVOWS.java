package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para duracionTurnoBalnearioVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="duracionTurnoBalnearioVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idDuracionTurnoBalneario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="tipoDuracionTurno" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoDuracionTurnoVOWS" minOccurs="0"/&gt;
 *         &lt;element name="turnoBalneario" type="{http://solicitante.jaxws.termalismo.imserso.com/}turnoBalnearioVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "duracionTurnoBalnearioVOWS", propOrder = {
    "idDuracionTurnoBalneario",
    "tipoDuracionTurno",
    "turnoBalneario"
})
public class DuracionTurnoBalnearioVOWS {

    protected Long idDuracionTurnoBalneario;
    protected TipoDuracionTurnoVOWS tipoDuracionTurno;
    protected TurnoBalnearioVOWS turnoBalneario;

    /**
     * Obtiene el valor de la propiedad idDuracionTurnoBalneario.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdDuracionTurnoBalneario() {
        return idDuracionTurnoBalneario;
    }

    /**
     * Define el valor de la propiedad idDuracionTurnoBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdDuracionTurnoBalneario(Long value) {
        this.idDuracionTurnoBalneario = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDuracionTurno.
     * 
     * @return
     *     possible object is
     *     {@link TipoDuracionTurnoVOWS }
     *     
     */
    public TipoDuracionTurnoVOWS getTipoDuracionTurno() {
        return tipoDuracionTurno;
    }

    /**
     * Define el valor de la propiedad tipoDuracionTurno.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDuracionTurnoVOWS }
     *     
     */
    public void setTipoDuracionTurno(TipoDuracionTurnoVOWS value) {
        this.tipoDuracionTurno = value;
    }

    /**
     * Obtiene el valor de la propiedad turnoBalneario.
     * 
     * @return
     *     possible object is
     *     {@link TurnoBalnearioVOWS }
     *     
     */
    public TurnoBalnearioVOWS getTurnoBalneario() {
        return turnoBalneario;
    }

    /**
     * Define el valor de la propiedad turnoBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link TurnoBalnearioVOWS }
     *     
     */
    public void setTurnoBalneario(TurnoBalnearioVOWS value) {
        this.turnoBalneario = value;
    }

}
