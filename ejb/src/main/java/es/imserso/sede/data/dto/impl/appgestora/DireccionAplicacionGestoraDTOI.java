package es.imserso.sede.data.dto.impl.appgestora;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.direccion.DireccionDTOI;

/**
 * @author 11825775
 *
 */
public interface DireccionAplicacionGestoraDTOI extends DireccionDTOI {

	SimpleDTOI getSelectedProvincia();
	
	void setSelectedProvincia(SimpleDTOI provincia);

}
