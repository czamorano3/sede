package es.imserso.sede.data.dto.persona;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.apache.commons.lang.StringUtils;

import es.imserso.sede.data.dto.direccion.DireccionDTOI;
import es.imserso.sede.data.dto.email.EmailDTOI;
import es.imserso.sede.data.validation.nif.NifNie;

/**
 * Datos de una persona
 * <p>
 * Estos datos pueden ser del representante, del solicitante, del cónyuge, etc
 * 
 * @author 11825775
 *
 */
public interface PersonaDTOI {

	@Size(max = 50, message = "El documento de identificación no puede contener más de 50 caracteres")
	String getNombre();

	@Size(max = 50, message = "El documento de identificación no puede contener más de 50 caracteres")
	String getApellido1();

	@Size(max = 50, message = "El documento de identificación no puede contener más de 50 caracteres")
	String getApellido2();

	@NifNie(message = "El documento de identificación no es válido")
	@Size(min = 9, max = 15, message = "El documento de identificación debe tener entre 9 y 15 caracteres")
	String getDocumentoIdentificacion();

	@Past(message = "La fecha de nacimiento debe ser anterior a la fecha actual")
	@Temporal(TemporalType.DATE)
	Date getFechaNacimiento();

	String getSexo();

	String getEstadoCivil();

	String getTelefono();

	String getTelefono2();

	DireccionDTOI getDireccion();

	EmailDTOI getEmail();

	/**
	 * @return nombre y apellidos concatenados
	 */
	default String getNombreApellidos() {
		return String.join(" ",
				Arrays.asList(getNombre(), getApellido1(), StringUtils.defaultIfBlank(getApellido2(), StringUtils.EMPTY)));

	}

	void setEmail(EmailDTOI email);

//	void setDireccion(DireccionDTOI direccion);

	void setTelefono2(String telefono2);

	void setTelefono(String telefono);

	void setEstadoCivil(String estadoCivil);

	void setSexo(String sexo);

	void setFechaNacimiento(Date fechaNacimiento);

	void setDocumentoIdentificacion(String documentoIdentificacion);

	void setApellido2(String apellido2);

	void setApellido1(String apellido1);

	void setNombre(String nombre);

}