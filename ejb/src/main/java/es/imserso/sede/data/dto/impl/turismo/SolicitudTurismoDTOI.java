package es.imserso.sede.data.dto.impl.turismo;

import com.fasterxml.jackson.core.JsonProcessingException;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.sede.data.dto.datosEconomicos.DatosEconomicosDTOI;
import es.imserso.sede.data.dto.destino.DestinosDTOI;
import es.imserso.sede.data.dto.familiaNumerosa.FamiliaNumerosaDTOI;
import es.imserso.sede.data.dto.impl.appgestora.SolicitudAppGestoraDTOI;
import es.imserso.sede.data.dto.persona.HijoDTOI;
import es.imserso.sede.data.dto.vinculada.SolicitudVinculadaDTOI;

/**
 * @author 11825775
 *
 */
public interface SolicitudTurismoDTOI extends SolicitudAppGestoraDTOI {

	Long getId();

	void setId(Long id);

	SolicitanteTurismoDTOI getSolicitante();

	void setSolicitante(SolicitanteTurismoDTOI solicitante);

	ConyugeTurismoDTOI getConyuge();

	void setConyuge(ConyugeTurismoDTOI conyuge);

	HijoDTOI getHijo();

	void setHijo(HijoDTOI hijo);

	DireccionNotificacionTurismoDTOI getDireccionNotificacion();

	void setDireccionNotificacion(DireccionNotificacionTurismoDTOI direccionNotificacion);

	SolicitudVinculadaDTOI getSolicitudVinculada();

	void setSolicitudVinculada(SolicitudVinculadaDTOI solicitudVinculada);

	DestinosDTOI getDestinos();

	void setDestinos(DestinosDTOI destinos);

	FamiliaNumerosaDTOI getFamiliaNumerosa();

	void setFamiliaNumerosa(FamiliaNumerosaDTOI familiaNumerosa);

	DatosEconomicosDTOI getDatosEconomicos();

	void setDatosEconomicos(DatosEconomicosDTOI datosEconomicos);

	int datosEconomicosCount();

	Boolean getDenegarAutorizacionConsultas();

	void setDenegarAutorizacionConsultas(Boolean denegarAutorizacionConsultas);

	AcreditacionDTO getAcreditacion();

	void setAcreditacion(AcreditacionDTO acreditacion);

	PlazoDTO getPlazo();

	void setPlazo(PlazoDTO plazo);

	String getJSON() throws JsonProcessingException;

	boolean hasConyuge();

	boolean hasHijo();

	boolean hasVinculacion();

	boolean hasFamiliaNumerosa();

}
