package es.imserso.sede.data.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TramiteUCM {
	private String SIA;
	private String denominacion;
	private boolean permanente;
	private boolean cerradotemporalmente;
	private Date fechaapertura;
	private Date fechacierre;
	private boolean exigeautenticacionclave;
	private String objeto;
	
	public String getSIA() {
		return SIA;
	}
	public void setSIA(String sIA) {
		SIA = sIA;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public boolean isPermanente() {
		return permanente;
	}
	public void setPermanente(boolean permanente) {
		this.permanente = permanente;
	}
	public boolean isCerradotemporalmente() {
		return cerradotemporalmente;
	}
	public void setCerradotemporalmente(boolean cerradotemporalmente) {
		this.cerradotemporalmente = cerradotemporalmente;
	}
	public Date getFechaapertura() {
		return fechaapertura;
	}
	public void setFechaapertura(Date fechaapertura) {
		this.fechaapertura = fechaapertura;
	}
	public Date getFechacierre() {
		return fechacierre;
	}
	public void setFechacierre(Date fechacierre) {
		this.fechacierre = fechacierre;
	}
	public boolean isExigeautenticacionclave() {
		return exigeautenticacionclave;
	}
	public void setExigeautenticacionclave(boolean exigeautenticacionclave) {
		this.exigeautenticacionclave = exigeautenticacionclave;
	}
	public String getObjeto() {
		return objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	} 
	
}
