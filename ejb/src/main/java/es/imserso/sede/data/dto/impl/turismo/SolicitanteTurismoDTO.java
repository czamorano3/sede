package es.imserso.sede.data.dto.impl.turismo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.persona.SolicitanteDTO;
import es.imserso.sede.data.dto.qualifier.SolicitanteQ;
import es.imserso.sede.service.monitor.turismo.TurismoQ;

/**
 * @author 11825775
 *
 */
@SolicitanteQ
@TurismoQ
@Dependent
public class SolicitanteTurismoDTO extends SolicitanteDTO implements SolicitanteTurismoDTOI {

	private static final long serialVersionUID = -2315943578448271619L;

	private static final Logger log = Logger.getLogger(SolicitanteTurismoDTO.class.getName());

	private SimpleDTOI selectedProvincia;
	private SimpleDTOI selectedEstadoCivil;
	private SimpleDTOI selectedSexo;
	private List<DatoEconomicoDTOI> datosEconomicos;
	private DatoEconomicoDTOI datoEconomico;

	@PostConstruct
	public void onCreate() {
		datosEconomicos = new ArrayList<DatoEconomicoDTOI>();

		// para que salte el decorador y asigne los datos del certificado, si procede
		log.debug(getNombre());
		log.debug(getApellido1());
		log.debug(getApellido2());
		log.debug(getDocumentoIdentificacion());
	}

	@Override
	public SimpleDTOI getSelectedProvincia() {
		return selectedProvincia;
	}

	@Override
	public void setSelectedProvincia( SimpleDTOI provincia) {
		this.selectedProvincia = provincia;
		direccion.setProvincia(this.selectedProvincia == null ? null : selectedProvincia.getDescripcion());
	}

	@Override
	public SimpleDTOI getSelectedEstadoCivil() {
		return selectedEstadoCivil;
	}

	@Override
	public void setSelectedEstadoCivil( SimpleDTOI estadoCivil) {
		this.selectedEstadoCivil = estadoCivil;
		setEstadoCivil(this.selectedEstadoCivil == null ? null : selectedEstadoCivil.getDescripcion());
	}

	@Override
	public List<DatoEconomicoDTOI> getDatosEconomicos() {
		return datosEconomicos;
	}

	@Override
	public void setDatosEconomicos( List<DatoEconomicoDTOI> datosEconomicos) {
		this.datosEconomicos = datosEconomicos;

	}

	@Override
	public void addDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico) {
		this.datosEconomicos.add(datoEconomico);
	}

	@Override
	public void deleteDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico) {
		this.datosEconomicos.remove(datoEconomico);

	}

	@Override
	public SimpleDTOI getSelectedSexo() {
		return selectedSexo;
	}

	@Override
	public void setSelectedSexo(SimpleDTOI sexo) {
		this.selectedSexo = sexo;
		setSexo(this.selectedSexo == null ? null : selectedSexo.getDescripcion());
	}

	public DatoEconomicoDTOI getDatoEconomico() {
		return datoEconomico;
	}

	public void setDatoEconomico(DatoEconomicoDTOI datoEconomico) {
		this.datoEconomico = datoEconomico;
	}

}
