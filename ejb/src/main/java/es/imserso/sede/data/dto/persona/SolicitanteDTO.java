package es.imserso.sede.data.dto.persona;

import javax.enterprise.context.Dependent;

/**
 * @author 11825775
 *
 */
@Dependent
public class SolicitanteDTO extends PersonaDTO implements SolicitanteDTOI {

	private static final long serialVersionUID = 7846045655106530347L;

}
