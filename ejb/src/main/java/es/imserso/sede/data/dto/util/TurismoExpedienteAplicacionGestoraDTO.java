package es.imserso.sede.data.dto.util;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Datos del expediente de la aplicación gestora de Turismo.
 * 
 * @author 11825775
 *
 */
@XmlRootElement
public class TurismoExpedienteAplicacionGestoraDTO implements Serializable {

	private static final long serialVersionUID = 7572043604025842291L;

	private Logger log = Logger.getLogger(TurismoExpedienteAplicacionGestoraDTO.class);

	/**
	 * id identificador de la solicitud en Hermes
	 */
	private Long id;

	/**
	 * número de orden de la solicitud en Hermes
	 */
	private String numeroOrden;

	/**
	 * constructor vacío
	 */
	public TurismoExpedienteAplicacionGestoraDTO() {

	}
	
	/**
	 * @param id
	 *            identificador de la solicitud en Hermes
	 * @param numeroOrden
	 *            número de orden de la solicitud en Hermes
	 */
	public TurismoExpedienteAplicacionGestoraDTO(Long id, String numeroOrden) {
		setId(id);
		setNumeroOrden(numeroOrden);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	/**
	 * @return devuelve el objeto en formato JSON
	 * @throws JsonProcessingException
	 */
	@JsonIgnore
	public String getJSON() throws JsonProcessingException {
		log.debug("obtenemos el JSON del DTO...");
		return new ObjectMapper().writeValueAsString(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "id:" + this.id + ", numeroOrden: " + this.numeroOrden;
	}

}
