package es.imserso.sede.data.dto.impl.turismo;

import org.apache.commons.lang.StringUtils;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTO;

/**
 * @author 11825775
 *
 */
public class DatoEconomicoTurismoDTO extends DatoEconomicoDTO implements DatoEconomicoTurismoDTOI {

	private static final long serialVersionUID = 7078941966006268598L;

	private SimpleDTOI clasePension;
	private SimpleDTOI procedenciaPension;

	@Override
	public SimpleDTOI getClasePension() {
		return clasePension;
	}

	public void setClasePension(SimpleDTOI clasePension) {
		this.clasePension = clasePension;
	}

	@Override
	public SimpleDTOI getProcedenciaPension() {
		return procedenciaPension;
	}

	public void setProcedenciaPension(SimpleDTOI procedenciaPension) {
		this.procedenciaPension = procedenciaPension;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clasePension == null) ? 0 : clasePension.hashCode());
		result = prime * result + ((importe == null) ? 0 : importe.hashCode());
		result = prime * result + ((procedenciaPension == null) ? 0 : procedenciaPension.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatoEconomicoTurismoDTO other = (DatoEconomicoTurismoDTO) obj;
		if (clasePension == null) {
			if (other.getClasePension() != null)
				return false;
		} else if (!clasePension.equals(other.getClasePension()))
			return false;
		if (importe == null) {
			if (other.getImporte() != null)
				return false;
		} else if (!importe.equals(other.getImporte()))
			return false;
		if (procedenciaPension == null) {
			if (other.getProcedenciaPension() != null)
				return false;
		} else if (!procedenciaPension.equals(other.getProcedenciaPension()))
			return false;
		return true;
	}

	@Override
	public boolean isValid() {
		boolean valid = true;
		validationFailedMessage = StringUtils.EMPTY;
		if (clasePension == null || procedenciaPension == null || importe == null) {
			validationFailedMessage = "Debe introducir todos los datos de la prestación";
			valid = false;
		}
		return valid;
	}

}
