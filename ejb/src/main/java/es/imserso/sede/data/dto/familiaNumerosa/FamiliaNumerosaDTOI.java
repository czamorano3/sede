package es.imserso.sede.data.dto.familiaNumerosa;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;

/**
 * @author 11825775
 *
 */
public interface FamiliaNumerosaDTOI {

	String getNumeroCarnet();

	SimpleDTOI getCategoria();

}
