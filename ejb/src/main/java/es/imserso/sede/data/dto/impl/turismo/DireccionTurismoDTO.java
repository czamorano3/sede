package es.imserso.sede.data.dto.impl.turismo;

import javax.enterprise.context.Dependent;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.direccion.DireccionDTO;
import es.imserso.sede.data.dto.direccion.DireccionDTOI;
import es.imserso.sede.service.monitor.turismo.TurismoQ;

/**
 * @author 11825775
 *
 */
@TurismoQ
@Dependent
public class DireccionTurismoDTO extends DireccionDTO implements DireccionDTOI, DireccionTurismoDTOI {

	private static final long serialVersionUID = -6830690215334470722L;

	private SimpleDTOI selectedProvincia;

	@Override
	public SimpleDTOI getSelectedProvincia() {
		return selectedProvincia;
	}

	@Override
	public void setSelectedProvincia(SimpleDTOI selectedProvincia) {
		this.selectedProvincia = selectedProvincia;
	}

}
