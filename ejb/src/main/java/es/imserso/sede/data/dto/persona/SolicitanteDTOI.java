package es.imserso.sede.data.dto.persona;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import es.imserso.sede.data.dto.direccion.DireccionDTOI;
import es.imserso.sede.data.dto.email.EmailDTOI;
import es.imserso.sede.data.dto.qualifier.SolicitanteQ;
import es.imserso.sede.data.validation.nif.NifNie;

@SolicitanteQ
public interface SolicitanteDTOI extends PersonaDTOI {
	
	@NotBlank(message = "Debe especificarse el nombre del solicitante")
	@Size(max=35, message="El nombre del solicitante no puede contener más de 35 caracteres")
	String getNombre();

	@NotBlank(message = "Debe especificarse el primer apellido del solicitante")
	@Size(max=35, message="El primer apellido del solicitante no puede contener más de 35 caracteres")
	String getApellido1();

	@Size(max=35, message="El segundo apellido del solicitante no puede contener más de 35 caracteres")
	String getApellido2();

	@NotBlank(message = "Debe especificarse el documento de identificación del solicitante")
	@NifNie(message = "El documento de identificación del solicitante no es válido")
	@Size(min=9, max=15, message="El documento de identificación del solicitante debe tener entre 9 y 15 caracteres")
	String getDocumentoIdentificacion();

	@Past(message = "La fecha de nacimiento del solicitante debe ser anterior a la fecha actual")
	@Temporal(TemporalType.DATE)
	Date getFechaNacimiento();

	String getSexo();

	String getEstadoCivil();

	String getTelefono();

	String getTelefono2();

	DireccionDTOI getDireccion();

	EmailDTOI getEmail();

}
