package es.imserso.sede.data.dto.rest.termalismo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para estadoExpedienteVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="estadoExpedienteVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="diaDeSalida" type="{http://solicitante.jaxws.termalismo.imserso.com/}diaDeSalidaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="fechaEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idEstadoExpediente" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="motivoEstado" type="{http://solicitante.jaxws.termalismo.imserso.com/}motivoEstadoVOWS" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tipoEstadoExpediente" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoEstadoExpedienteVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "estadoExpedienteVOWS", propOrder = {
    "diaDeSalida",
    "fechaEstado",
    "idEstadoExpediente",
    "motivoEstado",
    "tipoEstadoExpediente"
})
public class EstadoExpedienteVOWS {

    protected DiaDeSalidaVOWS diaDeSalida;
    protected String fechaEstado;
    protected Long idEstadoExpediente;
    @XmlElement(nillable = true)
    protected List<MotivoEstadoVOWS> motivoEstado;
    protected TipoEstadoExpedienteVOWS tipoEstadoExpediente;

    /**
     * Obtiene el valor de la propiedad diaDeSalida.
     * 
     * @return
     *     possible object is
     *     {@link DiaDeSalidaVOWS }
     *     
     */
    public DiaDeSalidaVOWS getDiaDeSalida() {
        return diaDeSalida;
    }

    /**
     * Define el valor de la propiedad diaDeSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link DiaDeSalidaVOWS }
     *     
     */
    public void setDiaDeSalida(DiaDeSalidaVOWS value) {
        this.diaDeSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaEstado() {
        return fechaEstado;
    }

    /**
     * Define el valor de la propiedad fechaEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaEstado(String value) {
        this.fechaEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad idEstadoExpediente.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdEstadoExpediente() {
        return idEstadoExpediente;
    }

    /**
     * Define el valor de la propiedad idEstadoExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdEstadoExpediente(Long value) {
        this.idEstadoExpediente = value;
    }

    /**
     * Gets the value of the motivoEstado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the motivoEstado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotivoEstado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MotivoEstadoVOWS }
     * 
     * 
     */
    public List<MotivoEstadoVOWS> getMotivoEstado() {
        if (motivoEstado == null) {
            motivoEstado = new ArrayList<MotivoEstadoVOWS>();
        }
        return this.motivoEstado;
    }

    /**
     * Obtiene el valor de la propiedad tipoEstadoExpediente.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoExpedienteVOWS }
     *     
     */
    public TipoEstadoExpedienteVOWS getTipoEstadoExpediente() {
        return tipoEstadoExpediente;
    }

    /**
     * Define el valor de la propiedad tipoEstadoExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoExpedienteVOWS }
     *     
     */
    public void setTipoEstadoExpediente(TipoEstadoExpedienteVOWS value) {
        this.tipoEstadoExpediente = value;
    }

}
