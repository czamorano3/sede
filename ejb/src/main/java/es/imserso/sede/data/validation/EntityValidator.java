package es.imserso.sede.data.validation;

import javax.validation.constraints.NotNull;

import es.imserso.sede.util.exception.ValidationException;

/**
 * 
 * Ejecuta las validaciones de una entidad (se le llama desde el prepersist o
 * preupdate)
 * 
 * @author 02858341
 *
 */
public interface EntityValidator {

	/**
	 * @param o
	 *            entidad a validar
	 * @param acción
	 *            ejecutada sobre la entidad
	 * @throws ValidationException
	 */
	public void validate(@NotNull Object o, @NotNull PersistenceAction action)
			throws ValidationException;

}
