package es.imserso.sede.data.validation;

import java.lang.reflect.Field;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Ejecuta las validaciones de una entidad (se le llama desde el prepersist o
 * preupdate)
 * 
 * @author 02858341
 *
 */
public class EntityValidatorImpl implements EntityValidator {

	private static final long ENTITY_ID_ERROR = -1;

	@Inject
	private Logger log;

	public void validate(@NotNull Object o, @NotNull PersistenceAction action) throws ValidationException {

		
		//TODO implementar si es necesario
	}

	/**
	 * muestra en el log la acción llevada a cabo (alta/modificación)
	 */
	@SuppressWarnings("unused")
	private void logChanges(@NotNull Object o, @NotNull PersistenceAction action) {
		if (Utils.isEntity(o.getClass())) {
			Long id;
			if (action.equals(PersistenceAction.ACTION_PERSIST)) {
				// es un alta
				log.info("se da de alta la entidad " + o.getClass().getName() + ": " + o.toString());

			} else if (action.equals(PersistenceAction.ACTION_UPDATE)) {
				// es una modificación
				id = getEntityId(o);
				log.info("se modifica la entidad " + o.getClass().getName() + " con id=" + id + ": " + o.toString());

			} else if (action.equals(PersistenceAction.ACTION_REMOVE)) {
				// es una eliminación
				id = getEntityId(o);
				log.info("se elimina la entidad " + o.getClass().getName() + " con id=" + id + ": " + o.toString());

			}
		}

	}

	/**
	 * @param o
	 *            entidad de la cual obtener el identificador
	 * @return identificador de la entidad
	 */
	private Long getEntityId(@NotNull Object o) {
		Long id;
		try {
			Field fid = o.getClass().getDeclaredField("id");
			fid.setAccessible(true);
			id = (Long) fid.get(o);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			log.warn("no se ha podido obtener el id de la entidad: " + Utils.getExceptionMessage(e));
			id = ENTITY_ID_ERROR;
		}
		return id;
	}

}
