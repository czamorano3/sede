package es.imserso.sede.data.dto.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.util.DateUtil;

/**
 * Utilidades relacionadas con los DTOs.
 * 
 * @author 11825775
 *
 */
public class DTOUtils {

	private static Logger logger = Logger.getLogger(DTOUtils.class);

	/**
	 * @param hash valores recogidos del pdf
	 * @param dtoInstance DTO vacío que será rellenado con los valores de la hash
	 * @return DTO relleno con los valores de la hash
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws ParseException
	 */
	public static SolicitudTurismoDTO getSolicitudTurismoDTO(Hashtable<String, String> hash, SolicitudTurismoDTO dtoInstance)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, ParseException {
		String name;
		Object value;
		Class<?> type;

		// recorremos los campos del DTO y le asignamos los valores de la hash
		Class<?> classz = dtoInstance.getClass();
		Field[] fields = classz.getFields();
		while (classz.getSuperclass() != null) {
			fields = classz.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);

				name = f.getName();
				type = f.getType();
				value = hash.get(name);
				if (!PdfUtil.PassThroughFields.isInEnum(name)) {
					if (value != null) {
						// f.set(dtoInstance, value);
						if (type.equals(String.class)) {
							f.set(dtoInstance, (String) value);
						} else if (type.equals(Date.class)) {
							if (value instanceof String) {
								if (StringUtils.isNotEmpty((String) value)) {
									f.set(dtoInstance, DateUtil.parseDate((String) value));
								} else {
									f.set(dtoInstance, null);
								}
							} else {
								f.set(dtoInstance, (Date) value);
							}
						} else if (type.equals(Integer.class) || type.equals(int.class)) {
							if (value instanceof String) {
								if (StringUtils.isNotEmpty((String) value)) {
									f.set(dtoInstance, Integer.parseInt((String) value));
								} else {
									f.set(dtoInstance, null);
								}
							} else {
								f.set(dtoInstance, (Integer) value);
							}
						} else if (type.equals(Long.class) || type.equals(long.class)) {
							if (value instanceof String) {
								if (StringUtils.isNotEmpty((String) value)) {
									if (StringUtils.isNotEmpty((String) value)) {
										f.set(dtoInstance, Long.parseLong((String) value));
									} else {
										f.set(dtoInstance, null);
									}
								} else {
									f.set(dtoInstance, null);
								}
							} else {
								f.set(dtoInstance, (Long) value);
							}
						} else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
							if (value instanceof String) {
								if (StringUtils.isNotEmpty((String) value)) {
									f.set(dtoInstance, Boolean.parseBoolean((String) value));
								} else {
									f.set(dtoInstance, null);
								}
							} else {
								f.set(dtoInstance, (Boolean) value);
							}
						} else if (type.equals(BigDecimal.class)) {
							if (value instanceof String) {
								if (StringUtils.isNotEmpty((String) value)) {
									f.set(dtoInstance, new BigDecimal((String) value));
								} else {
									f.set(dtoInstance, null);
								}
							} else {
								f.set(dtoInstance, (BigDecimal) value);
							}
						} else {
							logger.warn("no se reconoce el tipo " + type + " para rellenar el DTO!!");
						}
					} else if (name.equals("attachedFiles")) {
						if (type.equals(String.class)) {
							f.set(dtoInstance, (String) value);
						}
					} else {
						if (hash.containsKey(name)) {
							logger.warn("no se encuentra el campo " + name + " en la hashtable!");
						} else {
							logger.debug("el campo " + name + " está vacío");
						}
					}
				}

				logger.debug("field name: " + name);
				logger.debug("field value: " + value);

			}

			classz = classz.getSuperclass();
		}

		return dtoInstance;
	}
}
