package es.imserso.sede.data.dto.familiaNumerosa;

import javax.enterprise.context.Dependent;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;

/**
 * @author 11825775
 *
 */
@Dependent
public class FamiliaNumerosaDTO implements FamiliaNumerosaDTOI {

	private String numeroCarnet;
	private SimpleDTOI categoria;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.FamiliaNumerosaDTOI#getNumeroCarnet()
	 */
	@Override
	public String getNumeroCarnet() {
		return numeroCarnet;
	}

	public void setNumeroCarnet(String numeroCarnet) {
		this.numeroCarnet = numeroCarnet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.FamiliaNumerosaDTOI#getCategoria()
	 */
	@Override
	public SimpleDTOI getCategoria() {
		return categoria;
	}

	public void setCategoria(SimpleDTOI categoria) {
		this.categoria = categoria;
	}

}
