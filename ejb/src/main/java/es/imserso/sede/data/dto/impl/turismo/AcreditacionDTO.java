package es.imserso.sede.data.dto.impl.turismo;

import java.io.Serializable;

/**
 * @author 11825775
 *
 */
public class AcreditacionDTO implements Serializable {

	private static final long serialVersionUID = -42201358839920514L;

	private Boolean acreditada;
	private String descripcionAcreditacion;

	public Boolean getAcreditada() {
		return acreditada;
	}

	public void setAcreditada(Boolean acreditada) {
		this.acreditada = acreditada;
	}

	public String getDescripcionAcreditacion() {
		return descripcionAcreditacion;
	}

	public void setDescripcionAcreditacion(String descripcionAcreditacion) {
		this.descripcionAcreditacion = descripcionAcreditacion;
	}

}
