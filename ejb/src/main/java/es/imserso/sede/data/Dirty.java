package es.imserso.sede.data;

/**
 * @author 11825775
 *
 */
public interface Dirty {

	/**
	 * @return es dirty si alguno de sus campos tiene valor
	 */
	Boolean isDirty();

}
