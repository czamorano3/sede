package es.imserso.sede.data.validation.conditional;

/**
 * Anotación para las validaciones donde si un campo tiene valor otros deben ser obligatorios
 * 
 * @author 11825775
 *
 */
public interface ConditionalValidation {

}
