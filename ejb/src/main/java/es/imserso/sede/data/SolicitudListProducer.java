package es.imserso.sede.data;

import java.lang.reflect.Member;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import es.imserso.sede.model.Solicitud;

@RequestScoped
public class SolicitudListProducer {

	@Inject
	private SolicitudRepository solicitudRepository;

	private List<Solicitud> solicitudes;

	// @Named provides access the return value via the EL variable name
	// "solicitudes" in the UI (e.g.,
	// Facelets or JSP view)
	@Produces
	@Named
	public List<Solicitud> getSolicitudes() {
		return solicitudes;
	}

	public void onMemberListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Member member) {
		retrieveAllSolicitudesOrderedByNumeroRegistro();
	}

	@PostConstruct
	public void retrieveAllSolicitudesOrderedByNumeroRegistro() {
		solicitudes = solicitudRepository.findAllOrderedByNumeroRegistro();
	}
}