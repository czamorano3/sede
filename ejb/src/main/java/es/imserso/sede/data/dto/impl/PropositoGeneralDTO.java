package es.imserso.sede.data.dto.impl;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;
import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.turismo.DireccionNotificacionTurismoDTOI;
import es.imserso.sede.data.dto.persona.RepresentanteDTOI;
import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.dto.qualifier.RepresentanteQ;
import es.imserso.sede.data.dto.solicitud.SolicitudDTO;

@XmlRootElement
@Dependent
public class PropositoGeneralDTO extends SolicitudDTO implements Serializable {

	private static final long serialVersionUID = 3217214452142789721L;

	private static final Logger log = Logger.getLogger(PropositoGeneralDTO.class.getName());

	@Inject
	@RepresentanteQ
	private RepresentanteDTOI representante;

	@Inject
	@NotificacionQ
	private DireccionNotificacionTurismoDTOI direccionNotificacion;

	@NotBlank(message = "Debe especificarse el asunto")
	@Size(max = 200, message = "El asunto no debe exceder los 200 caracteres")
	protected String asunto;

	@Size(max = 2000, message = "El texto para exponer no debe exceder los 2000 caracteres")
	@NotBlank(message = "Debe especificarse el texto que se expone")
	protected String expone;

	@Size(max = 2000, message = "El texto donde explica lo que solicita no debe exceder los 2000 caracteres")
	@NotBlank(message = "Debe especificarse el texto que indica lo que se solicita")
	protected String solicita;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getRepresentante()
	 */
	@Override
	public RepresentanteDTOI getRepresentante() {
		return representante;
	}

	@Override
	public void setRepresentante(RepresentanteDTOI representante) {
		this.representante = representante;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#hasRepresentante()
	 */
	@Override
	public boolean hasRepresentante() {
		return true;
	}

	public DireccionNotificacionTurismoDTOI getDireccionNotificacion() {
		return direccionNotificacion;
	}

	public void setDireccionNotificacion(DireccionNotificacionTurismoDTOI direccionNotificacion) {
		this.direccionNotificacion = direccionNotificacion;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getExpone() {
		return expone;
	}

	public void setExpone(String expone) {
		this.expone = expone;
	}

	public String getSolicita() {
		return solicita;
	}

	public void setSolicita(String solicita) {
		this.solicita = solicita;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getSolicitudEmail()
	 */
	@NotNull
	@Override
	public String getSolicitudEmail() {
		String email = null;
		
		if (solicitante.getEmail() != null) {
			email = solicitante.getEmail().getDireccion();
		}

		if (representante.getEmail() != null) {
			email = representante.getEmail().getDireccion();
		}

		if (medioNotificacion != null && medioNotificacion.getEmail() != null
				&& medioNotificacion.getEmail().getDireccion() != null) {
			email = medioNotificacion.getEmail().getDireccion();
		}

		log.debugv("Correo electrónico de la solicitud: {0}", email);
		return email;
	}

}
