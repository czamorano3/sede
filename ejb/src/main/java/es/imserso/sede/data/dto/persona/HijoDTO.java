package es.imserso.sede.data.dto.persona;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.qualifier.HijoQ;

/**
 * @author 11825775
 *
 */
@HijoQ
@Dependent
public class HijoDTO extends PersonaDTO implements HijoDTOI {

	private static final long serialVersionUID = 6259925303417964011L;

}
