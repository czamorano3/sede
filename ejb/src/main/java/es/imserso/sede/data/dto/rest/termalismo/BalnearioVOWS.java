
package es.imserso.sede.data.dto.rest.termalismo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para balnearioVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="balnearioVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codBalneario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idBalneario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="provincia" type="{http://solicitante.jaxws.termalismo.imserso.com/}provinciaVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "balnearioVOWS", propOrder = {
    "codBalneario",
    "idBalneario",
    "localidad",
    "nombre",
    "provincia"
})
public class BalnearioVOWS implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 2551618321728727469L;
	protected String codBalneario;
    protected Long idBalneario;
    protected String localidad;
    protected String nombre;
    protected ProvinciaVOWS provincia;

    /**
     * Obtiene el valor de la propiedad codBalneario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodBalneario() {
        return codBalneario;
    }

    /**
     * Define el valor de la propiedad codBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodBalneario(String value) {
        this.codBalneario = value;
    }

    /**
     * Obtiene el valor de la propiedad idBalneario.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdBalneario() {
        return idBalneario;
    }

    /**
     * Define el valor de la propiedad idBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdBalneario(Long value) {
        this.idBalneario = value;
    }

    /**
     * Obtiene el valor de la propiedad localidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * Define el valor de la propiedad localidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalidad(String value) {
        this.localidad = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad provincia.
     * 
     * @return
     *     possible object is
     *     {@link ProvinciaVOWS }
     *     
     */
    public ProvinciaVOWS getProvincia() {
        return provincia;
    }

    /**
     * Define el valor de la propiedad provincia.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvinciaVOWS }
     *     
     */
    public void setProvincia(ProvinciaVOWS value) {
        this.provincia = value;
    }
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BalnearioVOWS) {
			if (this.getNombre().equals(((BalnearioVOWS) obj).getNombre()) && this.getCodBalneario().equals(((BalnearioVOWS) obj).getCodBalneario())) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
