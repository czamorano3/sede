package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para expedienteVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="expedienteVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="anyo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="apellidosExpedienteRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="convocatoria" type="{http://solicitante.jaxws.termalismo.imserso.com/}convocatoriaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="conyuge" type="{http://solicitante.jaxws.termalismo.imserso.com/}personaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="diaDeSalida" type="{http://solicitante.jaxws.termalismo.imserso.com/}diaDeSalidaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="estadoCivilSolicitante" type="{http://solicitante.jaxws.termalismo.imserso.com/}estadoCivilVOWS" minOccurs="0"/&gt;
 *         &lt;element name="estadoExpediente" type="{http://solicitante.jaxws.termalismo.imserso.com/}estadoExpedienteVOWS" minOccurs="0"/&gt;
 *         &lt;element name="estadoRelacion" type="{http://solicitante.jaxws.termalismo.imserso.com/}estadoRelacionVOWS" minOccurs="0"/&gt;
 *         &lt;element name="familiaNumerosa" type="{http://solicitante.jaxws.termalismo.imserso.com/}familiaNumerosaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaRecepcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="informeMedico" type="{http://solicitante.jaxws.termalismo.imserso.com/}informeMedicoVOWS" minOccurs="0"/&gt;
 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreExpdienteRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numeroExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numeroExpedienteRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numeroNifExpdienteRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="provincia" type="{http://solicitante.jaxws.termalismo.imserso.com/}provinciaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="solicitante" type="{http://solicitante.jaxws.termalismo.imserso.com/}personaVOWS" minOccurs="0"/&gt;
 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telefono2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "expedienteVOWS", propOrder = {
    "anyo",
    "apellidosExpedienteRelacionado",
    "codigoPostal",
    "convocatoria",
    "conyuge",
    "diaDeSalida",
    "direccion",
    "email",
    "estadoCivilSolicitante",
    "estadoExpediente",
    "estadoRelacion",
    "familiaNumerosa",
    "fax",
    "fechaRecepcion",
    "idExpediente",
    "informeMedico",
    "localidad",
    "nombreExpdienteRelacionado",
    "numeroExpediente",
    "numeroExpedienteRelacionado",
    "numeroNifExpdienteRelacionado",
    "provincia",
    "solicitante",
    "telefono",
    "telefono2"
})
public class ExpedienteVOWS {

    protected Integer anyo;
    protected String apellidosExpedienteRelacionado;
    protected String codigoPostal;
    protected ConvocatoriaVOWS convocatoria;
    protected PersonaVOWS conyuge;
    protected DiaDeSalidaVOWS diaDeSalida;
    protected String direccion;
    protected String email;
    protected EstadoCivilVOWS estadoCivilSolicitante;
    protected EstadoExpedienteVOWS estadoExpediente;
    protected EstadoRelacionVOWS estadoRelacion;
    protected FamiliaNumerosaVOWS familiaNumerosa;
    protected String fax;
    protected String fechaRecepcion;
    protected Long idExpediente;
    protected InformeMedicoVOWS informeMedico;
    protected String localidad;
    protected String nombreExpdienteRelacionado;
    protected String numeroExpediente;
    protected String numeroExpedienteRelacionado;
    protected String numeroNifExpdienteRelacionado;
    protected ProvinciaVOWS provincia;
    protected PersonaVOWS solicitante;
    protected String telefono;
    protected String telefono2;

    /**
     * Obtiene el valor de la propiedad anyo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnyo() {
        return anyo;
    }

    /**
     * Define el valor de la propiedad anyo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnyo(Integer value) {
        this.anyo = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidosExpedienteRelacionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidosExpedienteRelacionado() {
        return apellidosExpedienteRelacionado;
    }

    /**
     * Define el valor de la propiedad apellidosExpedienteRelacionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidosExpedienteRelacionado(String value) {
        this.apellidosExpedienteRelacionado = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoPostal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Define el valor de la propiedad codigoPostal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPostal(String value) {
        this.codigoPostal = value;
    }

    /**
     * Obtiene el valor de la propiedad convocatoria.
     * 
     * @return
     *     possible object is
     *     {@link ConvocatoriaVOWS }
     *     
     */
    public ConvocatoriaVOWS getConvocatoria() {
        return convocatoria;
    }

    /**
     * Define el valor de la propiedad convocatoria.
     * 
     * @param value
     *     allowed object is
     *     {@link ConvocatoriaVOWS }
     *     
     */
    public void setConvocatoria(ConvocatoriaVOWS value) {
        this.convocatoria = value;
    }

    /**
     * Obtiene el valor de la propiedad conyuge.
     * 
     * @return
     *     possible object is
     *     {@link PersonaVOWS }
     *     
     */
    public PersonaVOWS getConyuge() {
        return conyuge;
    }

    /**
     * Define el valor de la propiedad conyuge.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaVOWS }
     *     
     */
    public void setConyuge(PersonaVOWS value) {
        this.conyuge = value;
    }

    /**
     * Obtiene el valor de la propiedad diaDeSalida.
     * 
     * @return
     *     possible object is
     *     {@link DiaDeSalidaVOWS }
     *     
     */
    public DiaDeSalidaVOWS getDiaDeSalida() {
        return diaDeSalida;
    }

    /**
     * Define el valor de la propiedad diaDeSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link DiaDeSalidaVOWS }
     *     
     */
    public void setDiaDeSalida(DiaDeSalidaVOWS value) {
        this.diaDeSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCivilSolicitante.
     * 
     * @return
     *     possible object is
     *     {@link EstadoCivilVOWS }
     *     
     */
    public EstadoCivilVOWS getEstadoCivilSolicitante() {
        return estadoCivilSolicitante;
    }

    /**
     * Define el valor de la propiedad estadoCivilSolicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link EstadoCivilVOWS }
     *     
     */
    public void setEstadoCivilSolicitante(EstadoCivilVOWS value) {
        this.estadoCivilSolicitante = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoExpediente.
     * 
     * @return
     *     possible object is
     *     {@link EstadoExpedienteVOWS }
     *     
     */
    public EstadoExpedienteVOWS getEstadoExpediente() {
        return estadoExpediente;
    }

    /**
     * Define el valor de la propiedad estadoExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link EstadoExpedienteVOWS }
     *     
     */
    public void setEstadoExpediente(EstadoExpedienteVOWS value) {
        this.estadoExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoRelacion.
     * 
     * @return
     *     possible object is
     *     {@link EstadoRelacionVOWS }
     *     
     */
    public EstadoRelacionVOWS getEstadoRelacion() {
        return estadoRelacion;
    }

    /**
     * Define el valor de la propiedad estadoRelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link EstadoRelacionVOWS }
     *     
     */
    public void setEstadoRelacion(EstadoRelacionVOWS value) {
        this.estadoRelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad familiaNumerosa.
     * 
     * @return
     *     possible object is
     *     {@link FamiliaNumerosaVOWS }
     *     
     */
    public FamiliaNumerosaVOWS getFamiliaNumerosa() {
        return familiaNumerosa;
    }

    /**
     * Define el valor de la propiedad familiaNumerosa.
     * 
     * @param value
     *     allowed object is
     *     {@link FamiliaNumerosaVOWS }
     *     
     */
    public void setFamiliaNumerosa(FamiliaNumerosaVOWS value) {
        this.familiaNumerosa = value;
    }

    /**
     * Obtiene el valor de la propiedad fax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Define el valor de la propiedad fax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRecepcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRecepcion() {
        return fechaRecepcion;
    }

    /**
     * Define el valor de la propiedad fechaRecepcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRecepcion(String value) {
        this.fechaRecepcion = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdExpediente(Long value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad informeMedico.
     * 
     * @return
     *     possible object is
     *     {@link InformeMedicoVOWS }
     *     
     */
    public InformeMedicoVOWS getInformeMedico() {
        return informeMedico;
    }

    /**
     * Define el valor de la propiedad informeMedico.
     * 
     * @param value
     *     allowed object is
     *     {@link InformeMedicoVOWS }
     *     
     */
    public void setInformeMedico(InformeMedicoVOWS value) {
        this.informeMedico = value;
    }

    /**
     * Obtiene el valor de la propiedad localidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * Define el valor de la propiedad localidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalidad(String value) {
        this.localidad = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreExpdienteRelacionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreExpdienteRelacionado() {
        return nombreExpdienteRelacionado;
    }

    /**
     * Define el valor de la propiedad nombreExpdienteRelacionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreExpdienteRelacionado(String value) {
        this.nombreExpdienteRelacionado = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroExpediente() {
        return numeroExpediente;
    }

    /**
     * Define el valor de la propiedad numeroExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroExpediente(String value) {
        this.numeroExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroExpedienteRelacionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroExpedienteRelacionado() {
        return numeroExpedienteRelacionado;
    }

    /**
     * Define el valor de la propiedad numeroExpedienteRelacionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroExpedienteRelacionado(String value) {
        this.numeroExpedienteRelacionado = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroNifExpdienteRelacionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroNifExpdienteRelacionado() {
        return numeroNifExpdienteRelacionado;
    }

    /**
     * Define el valor de la propiedad numeroNifExpdienteRelacionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroNifExpdienteRelacionado(String value) {
        this.numeroNifExpdienteRelacionado = value;
    }

    /**
     * Obtiene el valor de la propiedad provincia.
     * 
     * @return
     *     possible object is
     *     {@link ProvinciaVOWS }
     *     
     */
    public ProvinciaVOWS getProvincia() {
        return provincia;
    }

    /**
     * Define el valor de la propiedad provincia.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvinciaVOWS }
     *     
     */
    public void setProvincia(ProvinciaVOWS value) {
        this.provincia = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitante.
     * 
     * @return
     *     possible object is
     *     {@link PersonaVOWS }
     *     
     */
    public PersonaVOWS getSolicitante() {
        return solicitante;
    }

    /**
     * Define el valor de la propiedad solicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaVOWS }
     *     
     */
    public void setSolicitante(PersonaVOWS value) {
        this.solicitante = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Define el valor de la propiedad telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono2() {
        return telefono2;
    }

    /**
     * Define el valor de la propiedad telefono2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono2(String value) {
        this.telefono2 = value;
    }

}
