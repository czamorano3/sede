package es.imserso.sede.data.validation.exception;

public class WrongFormatException extends ValidatorException {
	
	private static final long serialVersionUID = -3300965266115036446L;
	
	//TODO externalizar el mensaje
	private static final String msg = "Formato incorrecto"; 

	public WrongFormatException() {
		super(msg);
	}

	public WrongFormatException(String message) {
		super(message);
	}

}
