package es.imserso.sede.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import es.imserso.sede.model.EventMessage;

@Stateless
public class EventMessageRepository {

	@Inject
	private EntityManager em;

	public EventMessage getById(Long id) {
		return em.find(EventMessage.class, id);
	}

	/**
	 * Persiste el evento en base de datos
	 * 
	 * @param eventMessage
	 */
	public void persist(EventMessage eventMessage) {
		em.persist(eventMessage);
		em.flush();
	}

	/**
	 * @return todos los eventos persistidos ordenados por id descendientemente
	 */
	public List<EventMessage> getAll() {
		return em.createNamedQuery("EventMessage.getAll", EventMessage.class).getResultList();
	}
	
	/**
	 * @return todos los eventos de nivel ERROR persistidos ordenados por id descendientemente
	 */
	public List<EventMessage> getErrors() {
		return em.createNamedQuery("EventMessage.getError", EventMessage.class).getResultList();
	}
	
	/**
	 * @return todos los eventos de nivel FATAL persistidos ordenados por id descendientemente
	 */
	public List<EventMessage> getFatal() {
		return em.createNamedQuery("EventMessage.getFatal", EventMessage.class).getResultList();
	}
	
	/**
	 * @return todos los eventos de nivel ERROR y FATAL persistidos ordenados por id descendientemente
	 */
	public List<EventMessage> getErrorsAndFatal() {
		return em.createNamedQuery("EventMessage.getErrorAndFatal", EventMessage.class).getResultList();
	}

}
