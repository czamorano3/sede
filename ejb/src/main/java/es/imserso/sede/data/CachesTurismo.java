package es.imserso.sede.data;

/**
 * Nombres de cachés de datos auxiliares de Hermes para las vistas de turismo.
 * 
 * @author 11825775
 *
 */
public enum CachesTurismo {
	provincias, estados_civiles, sexos, categorias_familia_numerosa, destinos, clases_pensiones, procedencias_pensiones, plazo, turno, temporada, cartas_acreditacion_accesibles

}
