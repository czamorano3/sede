package es.imserso.sede.data.dto.impl.turismo;

import java.util.List;

import javax.validation.constraints.NotNull;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.persona.SolicitanteDTOI;

public interface SolicitanteTurismoDTOI extends SolicitanteDTOI {

	SimpleDTOI getSelectedProvincia();

	void setSelectedProvincia(SimpleDTOI provincia);

	SimpleDTOI getSelectedEstadoCivil();

	void setSelectedEstadoCivil(SimpleDTOI estadoCivil);

	List<DatoEconomicoDTOI> getDatosEconomicos();

	void setDatosEconomicos(List<DatoEconomicoDTOI> datosEconomicos);

	void addDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico);

	void deleteDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico);

	SimpleDTOI getSelectedSexo();

	void setSelectedSexo(SimpleDTOI sexo);

}
