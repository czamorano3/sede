package es.imserso.sede.data.dto.util;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author 02858341
 *
 */
@XmlRootElement
public class OfficialDateDTO {
	private Date date;
	
	/**
	 * @return the date
	 */
	@XmlElement
	public Date getDate() {
		return date;
	}
	/**
	 * @param fechaInicioEntregaSolicitud the fechaInicioEntregaSolicitud to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OfficialDateDTO [date=" + date + "]";
	}
}
