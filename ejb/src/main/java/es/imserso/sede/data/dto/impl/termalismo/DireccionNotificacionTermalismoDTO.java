package es.imserso.sede.data.dto.impl.termalismo;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.validation.constraint.NotificationAddress;
import es.imserso.sede.service.monitor.termalismo.TermalismoQ;

/**
 * @author 11825775
 *
 */
@TermalismoQ
@NotificationAddress
@NotificacionQ
@Dependent
public class DireccionNotificacionTermalismoDTO extends DireccionTermalismoDTO implements DireccionNotificacionTermalismoDTOI {

	private static final long serialVersionUID = 5946831653983117980L;

}
