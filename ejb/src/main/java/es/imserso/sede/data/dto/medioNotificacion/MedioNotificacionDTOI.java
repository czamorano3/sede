package es.imserso.sede.data.dto.medioNotificacion;

import es.imserso.sede.data.dto.email.EmailDTOI;
import es.imserso.sede.data.dto.qualifier.NotificacionQ;

/**
 * Interface para datos de notificación al usuario.
 * <p>
 * El email es obligatorio
 * 
 * @author 11825775
 *
 */
@NotificacionQ
public interface MedioNotificacionDTOI {

	/**
	 * @return dirección de corrreo electrónico para los avisos de notificación
	 */
	
	EmailDTOI getEmail();

	/**
	 * @return número de móvil para los avisos de notificación
	 */
	String getDispositivoElectronico();

}
