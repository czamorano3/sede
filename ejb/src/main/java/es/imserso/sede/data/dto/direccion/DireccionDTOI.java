package es.imserso.sede.data.dto.direccion;

import es.imserso.sede.data.Dirty;

/**
 * Datos de la dirección de una persona (solicitante, representante,
 * notificación, ...)
 * 
 * @author 11825775
 *
 */
public interface DireccionDTOI extends Dirty {

	/**
	 * País por defecto
	 */
	static final String DEFAULT_PAIS = "ESPAÑA";

	String getDomicilio();

	String getLocalidad();

	String getProvincia();

	String getCodigoPostal();

	default String getPais() {
		return DEFAULT_PAIS;
	}

	void setCodigoPostal(String codigoPostal);

	void setPais(String pais);

	void setProvincia(String provincia);

	void setLocalidad(String localidad);

	void setDomicilio(String domicilio);

}
