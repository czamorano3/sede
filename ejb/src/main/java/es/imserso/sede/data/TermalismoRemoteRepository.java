package es.imserso.sede.data;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import org.jboss.logging.Logger;
import es.imserso.hermes.session.webservice.dto.SimpleDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.rest.termalismo.ComunidadAutonomaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.EstadoCivilVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.SexoVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoFamiliaNumerosaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoPensionVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoTurnoVOWS;
import es.imserso.sede.service.cache.hermes.CrunchifyInMemoryCache;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.termalismo.TermalismoRESTClient;

/**
 * Gestiona los datos que se obtienen de Termalismo de forma remota.
 * <p>
 * Precisamente por obtenerse de forma remota y el coste que eso conlleva, este
 * componente optimiza los recursos utilizando una caché para aquellos datos que
 * apenas son modificados (provincias, estados civiles, etc)
 * 
 *
 */
@ApplicationScoped
public class TermalismoRemoteRepository {

	private static final int HOURS_1 = 3600;

	@Inject
	Instance<TermalismoRESTClient> termalismoRestClientInstance;

	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoEstadosCiviles;
	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoSexos;
	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoProvincias;
	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoTiposTurno;
	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoComunidadesAutonomas;
	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoTiposPensiones;	
	CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>> termalismoTiposFamiliaNum;
	
	private static final Logger log = Logger.getLogger(TermalismoRemoteRepository.class.getName());


	@PostConstruct
	public void init() {
		termalismoEstadosCiviles = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1,
				10);
		termalismoSexos = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1, 10);
		termalismoProvincias = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1, 10);
		termalismoTiposTurno = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1, 10);
		termalismoComunidadesAutonomas = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1, 10);
		termalismoTiposPensiones = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1, 10);
		termalismoTiposFamiliaNum = new CrunchifyInMemoryCache<CachesTermalismo, List<SimpleDTOI>>(HOURS_1, HOURS_1, 10); 
	}


	
	public List<SimpleDTOI> getProvincias() throws SedeException {
		List<SimpleDTOI> list = termalismoProvincias.get(CachesTermalismo.provincias);
		if (list == null) {
			
			if ((list = convert2DTOI(getProvinciasDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoProvincias.put(CachesTermalismo.provincias, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoProvincias.put(CachesTermalismo.provincias, list);
				
			}
		}
		return list;
	}

	
	private List<ProvinciaVOWS> getProvinciasDTO() throws SedeException{
		return termalismoRestClientInstance.get().getProvincias();
	}
	
	public List<SimpleDTOI> getEstadosCiviles() throws SedeException {
		List<SimpleDTOI> list = termalismoEstadosCiviles.get(CachesTermalismo.estadosCiviles);
		if (list == null) {
			
			if ((list = convert2DTOI(getEstadosCivilesDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoEstadosCiviles.put(CachesTermalismo.estadosCiviles, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoEstadosCiviles.put(CachesTermalismo.estadosCiviles, list);
				
			}
		}
		return list;
	}
	
	
	private List<EstadoCivilVOWS> getEstadosCivilesDTO(){
		return termalismoRestClientInstance.get().getEstadosCiviles();
	}
	
	public List<SimpleDTOI> getSexos() throws SedeException {
		List<SimpleDTOI> list = termalismoSexos.get(CachesTermalismo.sexos);
		if (list == null) {
			
			if ((list = convert2DTOI(getSexosDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoSexos.put(CachesTermalismo.sexos, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoSexos.put(CachesTermalismo.sexos, list);			
			}
		}
		return list;
	}
	
	
	private List<SexoVOWS> getSexosDTO(){
		return termalismoRestClientInstance.get().getSexos();
	}
	
	public List<SimpleDTOI> getTiposTurno() throws SedeException {
		List<SimpleDTOI> list = termalismoTiposTurno.get(CachesTermalismo.tiposTurno);
		if (list == null) {
			
			if ((list = convert2DTOI(getTiposTurnoDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoTiposTurno.put(CachesTermalismo.tiposTurno, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoTiposTurno.put(CachesTermalismo.tiposTurno, list);			
			}
		}
		return list;
	}

	
	private List<TipoTurnoVOWS> getTiposTurnoDTO(){
		return termalismoRestClientInstance.get().getTiposTurno();
	}	
	
	
	
	public List<SimpleDTOI> getBalneariosPorCCAA(Long idComunidad) throws SedeException {
 
			//return convertirDTOBalneario(termalismoRestClientInstance.get().getBalneariosPorCCAA(idComunidad));
		List<SimpleDTOI> list = new ArrayList<>();
		list = convert2DTOI(termalismoRestClientInstance.get().getBalneariosPorCCAA(idComunidad));
		log.debug("la lista se ha obtenido de Termalismo");
		return list;
	}


	
	public List<SimpleDTOI> getComunidadesAutonomas() throws SedeException {
		List<SimpleDTOI> list = termalismoComunidadesAutonomas.get(CachesTermalismo.comunidadesAutonomas);
		if (list == null) {
			
			if ((list = convert2DTOI(getComunidadesAutonomasDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoComunidadesAutonomas.put(CachesTermalismo.comunidadesAutonomas, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoComunidadesAutonomas.put(CachesTermalismo.comunidadesAutonomas, list);			
			}
		}
		return list;
	}

	private List<ComunidadAutonomaVOWS> getComunidadesAutonomasDTO(){
		return termalismoRestClientInstance.get().getComunidadesAutonomas();
	}	
	
	public List<SimpleDTOI> getTiposPension() throws SedeException {
		List<SimpleDTOI> list = termalismoTiposPensiones.get(CachesTermalismo.tiposPension);
		if (list == null) {
			
			if ((list = convert2DTOI(getTiposPensionDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoTiposPensiones.put(CachesTermalismo.tiposPension, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoTiposPensiones.put(CachesTermalismo.tiposPension, list);			
			}
		}
		return list;
	}
	
	
	private List<TipoPensionVOWS> getTiposPensionDTO(){
		return termalismoRestClientInstance.get().getTiposPensiones();
	}	
	

	public List<SimpleDTOI> getTiposFamiliaNum() throws SedeException {
		List<SimpleDTOI> list = termalismoTiposPensiones.get(CachesTermalismo.tiposFamiliaNum);
		if (list == null) {
			
			if ((list = convert2DTOI(getTiposFamiliaNumDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Termalismo, por lo usamos la última buena");
				termalismoTiposFamiliaNum.put(CachesTermalismo.tiposFamiliaNum, list);

			} else {
				log.debug("la lista se ha obtenido de Termalismo");
				termalismoTiposFamiliaNum.put(CachesTermalismo.tiposFamiliaNum, list);			
			}
		}
		return list;
	}

	
	private List<TipoFamiliaNumerosaVOWS> getTiposFamiliaNumDTO(){
		return termalismoRestClientInstance.get().getTiposFamiliaNumerosa();
	}

	private List<SimpleDTOI> convert2DTOI(List<?> list) {
		List<SimpleDTOI> dtoList = null;
		try {
			dtoList = new ArrayList<SimpleDTOI>();
			for (Object o : list) {
				SimpleDTO dto = new SimpleDTO();
				dto.refactorByReflectionTermalismo(o);
				dtoList.add(dto);
			}

		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
		}
		return dtoList;
	}
}
