package es.imserso.sede.data.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Clase con los atributos que nos devuelve validaClave en la cookie.
 * <p>
 * No confundir con la clase Usuario del modelo.
 * 
 * @author 11825775
 *
 */
@XmlRootElement
public class Usuario implements Serializable {

	private static final long serialVersionUID = 7869654349657970094L;

	String NIF_CIF = "";
	String nombre = "";
	String apellidos = "";
	String apellido1 = "";
	String apellido2 = "";

	public String getNIF_CIF() {
		return NIF_CIF;
	}

	@XmlElement
	public void setNIF_CIF(String nIF_CIF) {
		NIF_CIF = nIF_CIF;
	}

	public String getNombre() {
		return nombre;
	}

	@XmlElement
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return Stream.of(this.apellido1, this.apellido2).filter(Objects::nonNull).collect(Collectors.joining(" ")).trim();
	}
	
	public String getNombreCompleto() {
		return Stream.of(this.nombre, getApellidos()).filter(Objects::nonNull).collect(Collectors.joining(" ")).trim();
	}

	@XmlElement
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getApellido1() {
		return apellido1;
	}

	@XmlElement
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	@XmlElement
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

}
