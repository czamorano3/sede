

package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para turnoBalnearioVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="turnoBalnearioVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="balneario" type="{http://solicitante.jaxws.termalismo.imserso.com/}balnearioVOWS" minOccurs="0"/&gt;
 *         &lt;element name="idTurnoBalneario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="tipoTurno" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoTurnoVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "turnoBalnearioVOWS", propOrder = {
    "balneario",
    "idTurnoBalneario",
    "tipoTurno"
})
public class TurnoBalnearioVOWS {

    protected BalnearioVOWS balneario;
    protected Long idTurnoBalneario;
    protected TipoTurnoVOWS tipoTurno;

    /**
     * Obtiene el valor de la propiedad balneario.
     * 
     * @return
     *     possible object is
     *     {@link BalnearioVOWS }
     *     
     */
    public BalnearioVOWS getBalneario() {
        return balneario;
    }

    /**
     * Define el valor de la propiedad balneario.
     * 
     * @param value
     *     allowed object is
     *     {@link BalnearioVOWS }
     *     
     */
    public void setBalneario(BalnearioVOWS value) {
        this.balneario = value;
    }

    /**
     * Obtiene el valor de la propiedad idTurnoBalneario.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdTurnoBalneario() {
        return idTurnoBalneario;
    }

    /**
     * Define el valor de la propiedad idTurnoBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdTurnoBalneario(Long value) {
        this.idTurnoBalneario = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoTurno.
     * 
     * @return
     *     possible object is
     *     {@link TipoTurnoVOWS }
     *     
     */
    public TipoTurnoVOWS getTipoTurno() {
        return tipoTurno;
    }

    /**
     * Define el valor de la propiedad tipoTurno.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTurnoVOWS }
     *     
     */
    public void setTipoTurno(TipoTurnoVOWS value) {
        this.tipoTurno = value;
    }

}
