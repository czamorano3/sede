package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para estadoRelacionVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="estadoRelacionVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="descripcionRel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoEstadoRelacion" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoEstadoRelacionVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "estadoRelacionVOWS", propOrder = {
    "descripcionRel",
    "tipoEstadoRelacion"
})
public class EstadoRelacionVOWS {

    protected String descripcionRel;
    protected TipoEstadoRelacionVOWS tipoEstadoRelacion;

    /**
     * Obtiene el valor de la propiedad descripcionRel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionRel() {
        return descripcionRel;
    }

    /**
     * Define el valor de la propiedad descripcionRel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionRel(String value) {
        this.descripcionRel = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoEstadoRelacion.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoRelacionVOWS }
     *     
     */
    public TipoEstadoRelacionVOWS getTipoEstadoRelacion() {
        return tipoEstadoRelacion;
    }

    /**
     * Define el valor de la propiedad tipoEstadoRelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoRelacionVOWS }
     *     
     */
    public void setTipoEstadoRelacion(TipoEstadoRelacionVOWS value) {
        this.tipoEstadoRelacion = value;
    }

}
