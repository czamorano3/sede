package es.imserso.sede.data.dto.rest.termalismo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para informeMedicoVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="informeMedicoVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="balnearios" type="{http://solicitante.jaxws.termalismo.imserso.com/}balnearioVOWS" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="idInforme" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="solicitudPara" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoAfectadoVOWS" minOccurs="0"/&gt;
 *         &lt;element name="tipoDuracionTurno" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoDuracionTurnoVOWS" minOccurs="0"/&gt;
 *         &lt;element name="turnos" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoTurnoVOWS" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "informeMedicoVOWS", propOrder = {
    "balnearios",
    "idInforme",
    "solicitudPara",
    "tipoDuracionTurno",
    "turnos"
})
public class InformeMedicoVOWS {

    @XmlElement(nillable = true)
    protected List<BalnearioVOWS> balnearios;
    protected Long idInforme;
    protected TipoAfectadoVOWS solicitudPara;
    protected TipoDuracionTurnoVOWS tipoDuracionTurno;
    @XmlElement(nillable = true)
    protected List<TipoTurnoVOWS> turnos;

    /**
     * Gets the value of the balnearios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balnearios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalnearios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BalnearioVOWS }
     * 
     * 
     */
    public List<BalnearioVOWS> getBalnearios() {
        if (balnearios == null) {
            balnearios = new ArrayList<BalnearioVOWS>();
        }
        return this.balnearios;
    }

    /**
     * Obtiene el valor de la propiedad idInforme.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdInforme() {
        return idInforme;
    }

    /**
     * Define el valor de la propiedad idInforme.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdInforme(Long value) {
        this.idInforme = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudPara.
     * 
     * @return
     *     possible object is
     *     {@link TipoAfectadoVOWS }
     *     
     */
    public TipoAfectadoVOWS getSolicitudPara() {
        return solicitudPara;
    }

    /**
     * Define el valor de la propiedad solicitudPara.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAfectadoVOWS }
     *     
     */
    public void setSolicitudPara(TipoAfectadoVOWS value) {
        this.solicitudPara = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDuracionTurno.
     * 
     * @return
     *     possible object is
     *     {@link TipoDuracionTurnoVOWS }
     *     
     */
    public TipoDuracionTurnoVOWS getTipoDuracionTurno() {
        return tipoDuracionTurno;
    }

    /**
     * Define el valor de la propiedad tipoDuracionTurno.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDuracionTurnoVOWS }
     *     
     */
    public void setTipoDuracionTurno(TipoDuracionTurnoVOWS value) {
        this.tipoDuracionTurno = value;
    }

    /**
     * Gets the value of the turnos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the turnos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTurnos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoTurnoVOWS }
     * 
     * 
     */
    public List<TipoTurnoVOWS> getTurnos() {
        if (turnos == null) {
            turnos = new ArrayList<TipoTurnoVOWS>();
        }
        return this.turnos;
    }

}
