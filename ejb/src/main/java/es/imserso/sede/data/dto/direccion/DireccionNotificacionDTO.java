package es.imserso.sede.data.dto.direccion;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.validation.constraint.NotificationAddress;

@Dependent
@NotificationAddress
@NotificacionQ
public class DireccionNotificacionDTO extends DireccionDTO implements DireccionNotificacionDTOI {

	private static final long serialVersionUID = 3062135784433740563L;

}
