package es.imserso.sede.data.dto.impl.turismo;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.validation.constraint.NotificationAddress;
import es.imserso.sede.service.monitor.turismo.TurismoQ;

/**
 * @author 11825775
 *
 */
@TurismoQ
@NotificationAddress
@NotificacionQ
@Dependent
public class DireccionNotificacionTurismoDTO extends DireccionTurismoDTO implements DireccionNotificacionTurismoDTOI {

	private static final long serialVersionUID = 5946831653983117980L;

}
