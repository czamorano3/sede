package es.imserso.sede.data.dto.datosEconomicos;

import java.util.List;

/**
 * Datos económicos de una solicitud
 * 
 * @author 11825775
 *
 */
public interface DatosEconomicosDTOI {

	/**
	 * @return Datos económicos del solicitante
	 */
	List<DatoEconomicoDTOI> getDatosEconomicosSolicitante();

	/**
	 * @return Datos económicos del cónyuge
	 */
	List<DatoEconomicoDTOI> getDatosEconomicosConyuge();

	/**
	 * @return número de pensiones declaradas
	 */
	int count();

}
