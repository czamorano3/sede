package es.imserso.sede.data.dto.util;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 11825775
 *
 */
public class IdentificadorRegistroWrapper {

	/**
	 * construye una instancia de {@link IdentificadorRegistroDTO} a partir del
	 * objeto JSON
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public IdentificadorRegistroDTO build(@NotNull String jsonObject)
			throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(jsonObject, IdentificadorRegistroDTO.class);
	}

}