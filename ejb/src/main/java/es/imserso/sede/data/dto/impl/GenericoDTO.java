package es.imserso.sede.data.dto.impl;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.sede.data.dto.impl.turismo.DireccionNotificacionTurismoDTOI;
import es.imserso.sede.data.dto.persona.RepresentanteDTOI;
import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.dto.qualifier.RepresentanteQ;
import es.imserso.sede.data.dto.solicitud.SolicitudDTO;

@XmlRootElement
@Dependent
public class GenericoDTO extends SolicitudDTO implements Serializable {

	private static final long serialVersionUID = 3217214452142789721L;

	private static final Logger log = Logger.getLogger(PropositoGeneralDTO.class.getName());

	@Inject
	@RepresentanteQ
	protected RepresentanteDTOI representante;
	
	@Inject
	@NotificacionQ
	private DireccionNotificacionTurismoDTOI direccionNotificacion;

	/**
	 * Solicitud en formato PDF que debe haber rellenado y firmado el usuario
	 */
	@NotNull(message = "Debe enviar la solicitud rellena y firmada")
	protected DocumentoRegistrado solicitudPdf;

	public DocumentoRegistrado getSolicitudPdf() {
		return solicitudPdf;
	}

	public void setSolicitudPdf(DocumentoRegistrado solicitudPdf) {
		this.solicitudPdf = solicitudPdf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getRepresentante()
	 */
	@Override
	public RepresentanteDTOI getRepresentante() {
		return representante;
	}

	@Override
	public void setRepresentante(RepresentanteDTOI representante) {
		this.representante = representante;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#hasRepresentante()
	 */
	@Override
	public boolean hasRepresentante() {
		return true;
	}

	public DireccionNotificacionTurismoDTOI getDireccionNotificacion() {
		return direccionNotificacion;
	}

	public void setDireccionNotificacion(DireccionNotificacionTurismoDTOI direccionNotificacion) {
		this.direccionNotificacion = direccionNotificacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getSolicitudEmail()
	 */
	@NotNull
	@Override
	public String getSolicitudEmail() {
		String email = null;

		if (solicitante.getEmail() != null) {
			email = solicitante.getEmail().getDireccion();
		}

		if (representante.getEmail() != null) {
			email = representante.getEmail().getDireccion();
		}

		if (medioNotificacion != null && medioNotificacion.getEmail() != null
				&& medioNotificacion.getEmail().getDireccion() != null) {
			email = medioNotificacion.getEmail().getDireccion();
		}

		log.debugv("Correo electrónico de la solicitud: {0}", email);
		return email;
	}

}
