package es.imserso.sede.data.validation.exception;

public class NullPostalCodeException extends ValidatorException {
	
	private static final long serialVersionUID = 8246920241291394302L;
	
	//TODO externalizar el mensaje
	private static final String msg = "El Código Postal no puede estar vacío."; 

	public NullPostalCodeException() {
		super(msg);
	}

	public NullPostalCodeException(String message) {
		super(message);
	}

}
