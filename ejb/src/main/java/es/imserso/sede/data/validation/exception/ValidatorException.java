package es.imserso.sede.data.validation.exception;

public class ValidatorException extends Exception {
	
	private static final long serialVersionUID = -4039954838497980119L;
	
	//TODO externalizar el mensaje
	private static final String msg = "El objeto no es válido"; 

	public ValidatorException() {
		super(msg);
	}

	public ValidatorException(String message) {
		super(message);
	}


}
