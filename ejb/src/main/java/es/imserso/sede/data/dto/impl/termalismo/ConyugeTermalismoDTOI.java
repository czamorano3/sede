package es.imserso.sede.data.dto.impl.termalismo;

import java.util.List;

import javax.validation.constraints.NotNull;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.persona.ConyugeDTOI;

/**
 * @author 11825775
 *
 */
public interface ConyugeTermalismoDTOI extends ConyugeDTOI {



	List<DatoEconomicoDTOI> getDatosEconomicos();

	void setDatosEconomicosDTO( List<DatoEconomicoDTOI> datosEconomicos);

	void addDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico);

	void deleteDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico);

	SimpleDTOI getSelectedSexo();

	void setSelectedSexo( SimpleDTOI sexo);

}
