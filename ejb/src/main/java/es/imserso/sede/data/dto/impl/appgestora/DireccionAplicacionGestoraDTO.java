package es.imserso.sede.data.dto.impl.appgestora;

import javax.enterprise.context.Dependent;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.direccion.DireccionDTO;
import es.imserso.sede.data.dto.qualifier.AplicacionGestoraQ;

/**
 * @author 11825775
 *
 */
@Dependent
@AplicacionGestoraQ
public class DireccionAplicacionGestoraDTO extends DireccionDTO implements DireccionAplicacionGestoraDTOI {

	private static final long serialVersionUID = -1582303377417437117L;
	
	private SimpleDTOI selectedProvincia;

	@Override
	public SimpleDTOI getSelectedProvincia() {
		return selectedProvincia;
	}

	@Override
	public void setSelectedProvincia(SimpleDTOI provincia) {
		this.selectedProvincia = provincia;
	}

}
