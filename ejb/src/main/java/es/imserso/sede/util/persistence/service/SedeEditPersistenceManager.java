package es.imserso.sede.util.persistence.service;

/*
 * Utilizamos esta clase para manejar nostros la transacción en la edición de inventario.
 * De esta manera evitamos problemas de las transacciones que empieza jsf con los eventos ajax que ejecuta flush al terminar el ciclo
 */
public class SedeEditPersistenceManager  extends AbstractPersistenceManager {
	
	private static final long serialVersionUID = -8479807204511137091L;

	/* (non-Javadoc)
	 * @see es.imserso.inventario.util.persistence.service.AbstractPersistenceManager#getTransactionTimeoutSeconds()
	 */
	@Override
	public int getTransactionTimeoutSeconds() {
		return 100;
	}

}
