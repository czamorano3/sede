package es.imserso.sede.util.cdi;

import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Utilidades de CDI.
 * 
 * @author 11825775
 *
 */
public class UtilsCDI {

	/**
	 * @return BeanManager de CDI obtenido por JNDI
	 */
	public static BeanManager getBeanManager() {

		try {
			return (BeanManager) InitialContext
					.doLookup("java:comp/BeanManager");

		} catch (NamingException e) {
			throw new SedeRuntimeException(
					"No se pudo obtener el BeanManager de CDI por JNDI: "
							+ Utils.getExceptionMessage(e));
		}
	}

	/**
	 * Obtiene un bean manejado por cdi mediante referencia
	 * 
	 * @param c
	 * @return
	 */
	public static Object getBeanByReference(Class<?> c) {
		BeanManager beanManager = getBeanManager();
		@SuppressWarnings("unchecked")
		Bean<Object> bean = (Bean<Object>) beanManager.resolve(beanManager
				.getBeans(c));
		Object ref = (Object) beanManager.getReference(bean,
				bean.getBeanClass(), beanManager.createCreationalContext(bean));
		return ref;
	}

	/**
	 * Obtiene un bean manejado por cdi mediante el contexto
	 * 
	 * @param c
	 * @return
	 */
	public static Object getBeanByContext(Class<?> c) {
		BeanManager beanManager = UtilsCDI.getBeanManager();
		@SuppressWarnings("unchecked")
		Bean<Object> bean = (Bean<Object>) beanManager.resolve(beanManager
				.getBeans(c));
		Object ref = beanManager.getContext(bean.getScope()).get(bean,
				beanManager.createCreationalContext(bean));
		return ref;
	}

}
