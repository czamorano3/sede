package es.imserso.sede.util.rest.client.hermes;

import java.util.List;

import javax.ws.rs.core.GenericType;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Obtiene las solicitudes de un usuario mediante su documento de identificación.
 * 
 * @author 11825775
 *
 */
public class SolicitudesUsuario extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981232141987853780L;

	private static final Logger log = Logger.getLogger(SolicitudesUsuario.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		List<SolicitudTurismoDTO> list = null;

		try {

			log.debug("leyendo response ...");
			GenericType<List<SolicitudTurismoDTO>> genericType = new GenericType<List<SolicitudTurismoDTO>>() {
			};
			list = response.readEntity(genericType);

			if (list != null) {
				log.debugv("se han recibido {0} solicitudes para el NIF/NIE {1}", list.size(), param);
			} else {
				log.warnv("la lista de solicitudes para el NIF/NIE {0} es nula!", param);
			}

		} catch (Exception e) {
			String errmsg = String
					.format("Error al obtener las solicitudes del documento %s: " + Utils.getExceptionMessage(e));
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) list;
	}

}
