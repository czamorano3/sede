package es.imserso.sede.util;

public class Global {
	
	/**
	 * nombre del security domain de la aplicación definido en el container
	 */
	public static final String INV_SECURITY_DOMAIN_NAME = "imserso-ldapExtended-security-domain";
	
	/**
	 * nombre de la aplicación (para la ruta al directorio de la configuración)
	 */
	public static final String APP_CONFIGURATION_PATH = "sedecdi";
	
	
	public static final String CODIGO_SIA_TURISMO = "994874";
	public static final String CODIGO_SIA_TERMALISMO = "022670";
	public static final String CODIGO_SIA_PROPOSITO_GENERAL = "999999";
	public static final String CODIGO_SIA_GENERICO = "";
	
	public static final String CONTENT_TYPE_APPLICATION_PDF = "application/pdf";
	
	public static final String NOMBRE_FICHERO_CARTA_DE_ACREDITACION_TURISMO = "CartaAcreditacionTurismo.pdf";

	//Se comentan el resto de los trámites (genéricos) pues no deben estar como constantes sino en la
	//base de datos de la Sede para que puedan darse de alta más trámites genéricos sin necesidad de
	//tocar el código
//	public static final String CODIGO_SIA_CALIFICACION_GRADO_DISCAPACIDAD = "022400";
//	public static final String CODIGO_SIA_RECONOCIMIENTO_SITUACION_DEPENDENCIA = "994457";
//	public static final String CODIGO_SIA_ASISTENCIA_SANITARIA_Y_FARMACEUTICA = "022370";
//	public static final String CODIGO_SIA_PENSION_NO_CONTRIBUTIVA_INVALIDEZ = "022380";
//	public static final String CODIGO_SIA_PENSION_NO_CONTRIBUTIVA_JUBILACION = "022390";
//	public static final String CODIGO_SIA_SUBSIDIO_MOVILIDAD = "025809";
//	public static final String CODIGO_SIA_DECLARACION_ANUAL_INGRESOS_INVALIDEZ_NO_CONTRIBUTIVA = "201891";
//	public static final String CODIGO_SIA_DECLARACION_ANUAL_INGRESOS_JUBILACION_NO_CONTRIBUTIVA = "201892";
//	public static final String CODIGO_SIA_DECLARACION_ANUAL_INGRESOS_SUBSIDIO_GARANTIA_INGRESOS_MINIMOS = "201893";
//	public static final String CODIGO_SIA_DECLARACION_ANUAL_INGRESOS_SUBSIDIO_AYUDA_TERCERA_PERSONA = "201894";
//	public static final String CODIGO_SIA_DECLARACION_ANUAL_INGRESOS_SUBSIDIO_MOVILIDAD_TRANSPORTE = "201895";
//	public static final String CODIGO_SIA_COMPLEMENTO_JUBILACION_INVALIDEZ_NO_CONTRIB_ALQUILER = "995338";
//	public static final String CODIGO_SIA_SUBVENCION_ATENCION_MAYORES = "025751";
//	public static final String CODIGO_SIA_SUBVENCION_MAYORES_CEUTA_MELILLA = "025752";
//	public static final String CODIGO_SIA_SUBVENCION_PROYECTOS_INVESTIGACION_CIENTIFICA = "994434";
//	public static final String CODIGO_SIA_SUBVENCION_TURISMO_TERMALISMO_DISCAPACIDAD = "994794";
//	public static final String CODIGO_SIA_SUBVENCION_DISCAPACIDAD_CEUTA_MELILLA = "994795";
//	public static final String CODIGO_SIA_SUBVENCION_CENTROS_ESTATALES_DISCAPACIDAD = "994992";
//	public static final String CODIGO_SIA_SUBVENCION_DEPENDENCIA_CEUTA_MELILLA = "995102";
//	public static final String CODIGO_SIA_CENTRO_ATENCION_PERSONAS_DISCAPACIDAD_FISICA = "022680";
//	public static final String CODIGO_SIA_RESIDENCIA_MAYORES = "025750";
//	public static final String CODIGO_SIA_CENTRO_RECUPERACION_DISCAPACIDAD_FISICA = "025804";
//	public static final String CODIGO_SIA_ACREDITACION_CENTROS_SERVICIOS_ENTIDADES_PRIVADAS_CEUTA_MELILLA = "n/d";
	

}
