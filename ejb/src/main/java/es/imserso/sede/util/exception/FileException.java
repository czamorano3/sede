package es.imserso.sede.util.exception;



public class FileException extends SedeException {
	
	private static final long serialVersionUID = 2029916464326702327L;
	
	public static final String DEFAULT_MESSAGE = "Error en manejo de fichero";

	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public FileException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public FileException(String message) {
		super(message);
	}
	
}

