package es.imserso.sede.util.rest.client.hermes;

import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Modifica una solicitud en Hermes.
 * 
 * @author 11825775
 *
 */
public class UpdateSolicitud extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -8873554390617797183L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#execute()
	 */
	@Override
	public Object execute() throws SedeException {
		return (Object) response.readEntity(String.class);
	}

}