package es.imserso.sede.util.mail;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import es.imserso.sede.config.PropertyComponent;

@Named
public class MailManager implements Serializable {

	public static final String NOREPLY = "noreply@imserso.es";

	public static final String EMAIL_SUBJECT_INCIDENCIA = "Sede Electrónica: se detectó una incidencia";
	public static final String EMAIL_SUBJECT_IMSERSO_ALTA_DE_SOLICITUD = "IMSERSO. Alta de Solicitud";
	public static final String EMAIL_SUBJECT_IMSERSO_MODIFICACION_DE_SOLICITUD = "IMSERSO. Modificación de Solicitud";

	private static final long serialVersionUID = -7987368820400284500L;

	@Inject
	private MailEngine mailEngine;

	@Inject
	private PropertyComponent propertyComponent;

	/**
	 * Método para enviar un email de incidencia al administrador de sistemas
	 * 
	 * @param messageText
	 * @throws Exception
	 */
	public void sendNotificationToSistemas(@NotNull String messageText) throws Exception {
		mailEngine.send(EMAIL_SUBJECT_INCIDENCIA, messageText, NOREPLY, propertyComponent.getEmailSistemas());
	}

	/**
	 * Método para enviar un email de incidencia a los planificadores
	 * 
	 * @param messageText
	 * @throws Exception
	 */
	public void sendNotificationToPlanificadores(@NotNull String messageText) throws Exception {
		mailEngine.send(EMAIL_SUBJECT_INCIDENCIA, messageText, NOREPLY, propertyComponent.getEmailPlanificadores());
	}

	/**
	 * Método para enviar un email al usuario notificando el alta de su solicitud
	 * 
	 * @param messageText
	 *            texto del email confirmando el alta de la solicitud
	 * @param to
	 *            email del usuario
	 * @throws Exception
	 */
	public void sendNotificationToUserNew(@NotNull String messageText, @NotNull String to) throws Exception {
		mailEngine.send(EMAIL_SUBJECT_IMSERSO_ALTA_DE_SOLICITUD, messageText, NOREPLY, to);
	}

	/**
	 * Método para enviar un email al usuario notificando la modificación de su
	 * solicitud
	 * 
	 * @param messageText
	 *            texto del email confirmando la modificación de la solicitud
	 * @param to
	 *            email del usuario
	 * @throws Exception
	 */
	public void sendNotificationToUserUpdated(@NotNull String messageText, @NotNull String to) throws Exception {
		mailEngine.send(EMAIL_SUBJECT_IMSERSO_MODIFICACION_DE_SOLICITUD, messageText, NOREPLY, to);
	}

	/**
	 * Método para enviar un email
	 * 
	 * @param subject
	 * @param messageText
	 * @param to
	 * @throws Exception
	 */
	public void send(@NotNull String subject, @NotNull String messageText, @NotNull String to) throws Exception {
		mailEngine.send(subject, messageText, NOREPLY, to);
	}

}
