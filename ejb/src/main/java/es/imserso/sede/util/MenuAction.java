package es.imserso.sede.util;

import java.io.Serializable;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "menuAction")
@ConversationScoped
public class MenuAction implements Serializable {

	private static final long serialVersionUID = 4698513479512280449L;

	@Inject
	private Modo modo;

	public String abrirPagina(String pagina, String acceso) {
		// El acceso será: alta, edicion o consulta
		modo.setAcceso(acceso);
		return pagina;
	}

	public String abrirPagina(String pagina) {
		return abrirPagina(pagina, null);
	}
}
