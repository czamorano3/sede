package es.imserso.sede.util.rest.client;

import java.io.Serializable;

import es.imserso.sede.util.exception.SedeException;

/**
 * Interfaz para llamadas a los servicios REST de Hermes
 * 
 * @author 11825775
 *
 */
public interface RestClient extends Serializable {

	public static final String URL_SUFFIX_PLAZOTURNO_POR_DEFECTO = "plazoturnopordefecto";
	public static final String URL_SUFFIX_TEMPORADATURNO_POR_DEFECTO = "temporadaturnopordefecto";
	public static final String URL_SUFFIX_TURNO_POR_DEFECTO = "turnopordefecto";
	public static final String URL_SUFFIX_PROVINCIAS = "provincias";
	public static final String URL_SUFFIX_ESTADOS_CIVILES = "estadosciviles";
	public static final String URL_SUFFIX_SEXOS = "sexos";
	public static final String URL_SUFFIX_OPCIONES = "opciones";
	public static final String URL_SUFFIX_CLASES_PENSIONES = "clasespensiones";
	public static final String URL_SUFFIX_PROCEDENCIA_PENSIONES = "procedenciaspensiones";
	public static final String URL_SUFFIX_CATEGORIAS_FAMILIA_NUMEROSA = "categoriasfamilianumerosa";

	/**
	 * path del método del endpoint de Hermes para saber si la aplicación está
	 * bloqueada
	 */
	public static final String HERMES_IS_UNLOCKED_REST_PATH = "isunlocked";

	/**
	 * path del método del endpoint de Hermes para saber si el turno por defecto
	 * está abierto
	 */
	public static final String HERMES_IS_TURNO_OPEN_REST_PATH = "isTurnoOpen";

	/**
	 * path del método del endpoint de Hermes para comprobar si se puede crear una
	 * solicitud en el plazo por defecto
	 */
	public static final String HERMES_GET_ACCESIBLE_ALTA_SOLICITUD_PLAZO_POR_DEFECTO_REST_PATH = "isAccesibleAltaSolicitud";

	/**
	 * path del método del endpoint de Hermes para comprobar si se puede crear una
	 * solicitud en el plazo especificado por parámetro
	 */
	public static final String HERMES_GET_ACCESIBLE_ALTA_SOLICITUD_PLAZO_REST_PATH = "isAccesibleAltaSolicitud/";

	/**
	 * path del método del endpoint de Hermes para saber si la aplicación está
	 * operativa
	 */
	public static final String HERMES_IS_ALIVE_REST_PATH = "isalive";

	/**
	 * path del método del endpoint de Hermes para obtener una solicitud por su id
	 */
	public static final String HERMES_GET_SOLICITUD_REST_PATH = "";

	/**
	 * path del método del endpoint de Hermes para obtener una solicitud por su id
	 */
	public static final String HERMES_GET_SOLICITUDES_BY_DI_REST_PATH = "buscarPorDi/";

	/**
	 * path del método del endpoint de Hermes para obtener el estado de una
	 * solicitud por su id
	 */
	public static final String HERMES_GET_ESTADO_SOLICITUD_REST_PATH = "estado/";

	/**
	 * path del método del endpoint de Hermes para crear una solicitud
	 */
	public static final String HERMES_CREATE_SOLICITUD_REST_PATH = "createsolicitud";

	/**
	 * path del método del endpoint de Hermes para modificar una solicitud
	 */
	public static final String HERMES_UPDATE_SOLICITUD_REST_PATH = "updateSolicitud";

	/**
	 * path del método del endpoint de Hermes para crear una solicitud
	 */
	public static final String HERMES_ACREDITACION_DESCARGABLE_REST_PATH = "acreditaciondescargable";

	/**
	 * path del método del endpoint de Hermes para crear una solicitud
	 */
	public static final String HERMES_GET_CARTA_ACREDITACION_REST_PATH = "cartaacreditacion/";

	/**
	 * @param url
	 *            URL de la petición al servicio REST
	 * @param method
	 *            Método Http de la petición
	 * @param param
	 *            En el caso de que la petición tenga un parámetro, como un
	 *            identificador. Se dejará a null si la petición no necesita
	 *            parámetro.
	 * @return Se devuelve a sí mismo
	 * @throws SedeException
	 */
	RestClient call(String url, HttpMethod method, Object param) throws SedeException;

	/**
	 * @return objeto montado a partir de la respuesta del servicio REST
	 * @throws SedeException
	 */
	Object execute() throws SedeException;

}
