package es.imserso.sede.util.persistence.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceUnit;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;

import org.hibernate.Session;
import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Este componente se encarga de proporcionar contextos de persistencia y
 * transacciones del contenedor.
 * 
 * @author 11825775
 * 
 * 
 */
@Stateless
public class PersistenceService {

	@Inject
	private Logger log;

	@PersistenceUnit(unitName = "primary")
	private EntityManagerFactory emf;

	private InitialContext ic;

	private TransactionManager transactionManager;

	@Inject
	private PropertyComponent propertyComponent;

	/**
	 * @return un nuevo contexto de persistencia del contenedor
	 */
	public EntityManager getNewContainerEntityManager() {
		// return
		// Persistence.createEntityManagerFactory("hermes").createEntityManager();
		EntityManager ex = emf.createEntityManager();

		// try {
		// Session session = ex.unwrap(Session.class);
		// SessionFactoryImplementor sfi = (SessionFactoryImplementor)
		// session.getSessionFactory();
		// ConnectionProvider cp = sfi.getConnectionProvider();
		// Connection conn = cp.getConnection();
		// boolean autocommit = conn.getAutoCommit();
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// por defecto, el flushmode está en AUTO. como estamos en un hilo
		// aparte de seam, las propiedades del components.xml como
		// default-flush-mode se han perdido.
		ex.setFlushMode(FlushModeType.COMMIT);
		return ex;
	}

	/**
	 * @return Session de hibernate de un nuevo contexto de persistencia del
	 *         contenedor
	 */
	public Session getNewContainerHibernateSession() {
		return ((Session) getNewContainerEntityManager().unwrap(Session.class));
	}

	/**
	 * @param em
	 *            contexto de persistencia del cual obtener el objeto Session de
	 *            Hibernate
	 * @return Session de hibernate del contexto de persistencia especificado
	 */
	public Session getContainerHibernateSession(EntityManager em) {
		return ((Session) em.unwrap(Session.class));
	}

	/**
	 * @param em
	 *            contexto de persistencia del cual obtener el objeto Session de
	 *            Hibernate
	 * @return nueva Session de hibernate del contexto de persistencia
	 *         especificado
	 */
	public Session getNewContainerHibernateSession(EntityManager em) {
		return ((Session) em.unwrap(Session.class)).getSessionFactory()
				.openSession();
	}

	/**
	 * @return TransactionManager del container obtenido por JNDI
	 */
	public TransactionManager getTransactionManager() {
		try {
			if (transactionManager == null) {
				log.debug("buscando transacción en JNDI con el nombre: "
						+ propertyComponent.getTransactionManagerJndiName());
				transactionManager = (TransactionManager) getInitialContext()
						.lookup(propertyComponent
								.getTransactionManagerJndiName());
			}
			return transactionManager;

		} catch (NamingException e) {
			throw new SedeRuntimeException(Utils.getExceptionMessage(e));
		}
	}

	public void setTransactionManager(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	/**
	 * @return UserTransaction del container obtenido por JNDI
	 * @throws NamingException
	 */
	private UserTransaction getUserTransaction() throws NamingException {
		log.debug("buscando transacción en JNDI con el nombre: "
				+ propertyComponent.getUserTransactionJndiName());
		return (UserTransaction) getInitialContext().lookup(
				propertyComponent.getUserTransactionJndiName());
	}

	/**
	 * @return transacción nueva con el timeout por defecto
	 */
	public UserTransaction getNewTransaction() {
		return getNewTransaction(0);
	}

	/**
	 * @param timeoutInSeconds
	 *            tiempo en segundos de espera para que termine la transacción
	 * @return transacción nueva con el timeout especificado
	 */
	public UserTransaction getNewTransaction(int timeoutInSeconds) {
		UserTransaction utx;
		try {
			utx = getUserTransaction();
			utx.setTransactionTimeout(timeoutInSeconds);

		} catch (SystemException e) {
			throw new SedeRuntimeException(Utils.getExceptionMessage(e));
		} catch (NamingException e) {
			throw new SedeRuntimeException(Utils.getExceptionMessage(e));
		}
		return utx;
	}

	/**
	 * @return contexto inicial inicializado
	 */
	private InitialContext getInitialContext() {
		if (ic == null) {
			try {
				ic = new InitialContext();

			} catch (NamingException e) {
				throw new SedeRuntimeException(
						Utils.getExceptionMessage(e));
			}
		}
		return ic;
	}

	/**
	 * @param _userTransaction
	 *            UserTransaction
	 */
	public void logTransactionStatus(UserTransaction _userTransaction) {
		if (_userTransaction != null) {
			String msg;
			try {
				switch (_userTransaction.getStatus()) {
				case Status.STATUS_ACTIVE:
					msg = "ACTIVE";
					break;
				case Status.STATUS_UNKNOWN:
					msg = "STATUS_UNKNOWN";
					break;
				case Status.STATUS_COMMITTED:
					msg = "STATUS_COMMITTED";
					break;
				case Status.STATUS_COMMITTING:
					msg = "STATUS_COMMITTING";
					break;
				case Status.STATUS_MARKED_ROLLBACK:
					msg = "STATUS_MARKED_ROLLBACK";
					break;
				case Status.STATUS_NO_TRANSACTION:
					msg = "STATUS_NO_TRANSACTION";
					break;
				case Status.STATUS_PREPARED:
					msg = "STATUS_PREPARED";
					break;
				case Status.STATUS_PREPARING:
					msg = "STATUS_PREPARING";
					break;
				case Status.STATUS_ROLLEDBACK:
					msg = "STATUS_ROLLEDBACK";
					break;
				case Status.STATUS_ROLLING_BACK:
					msg = "STATUS_ROLLING_BACK";
					break;
				default:
					msg = "valor desconocido: " + _userTransaction.getStatus();
					break;
				}

				log.trace("estado de la transacción: " + msg);
			} catch (SystemException e) {
				throw new SedeRuntimeException(
						Utils.getExceptionMessage(e));
			}
		} else {
			log.warn("no se pudo obtener información de la transacción puesto que es null");
		}

	}

}
