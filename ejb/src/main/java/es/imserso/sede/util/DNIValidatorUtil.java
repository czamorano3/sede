package es.imserso.sede.util;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.data.validation.exception.DNIvalidationException;
import es.imserso.sede.data.validation.exception.NIEvalidationException;
import es.imserso.sede.data.validation.exception.NIFvalidationException;
import es.imserso.sede.data.validation.exception.ValidatorException;

public class DNIValidatorUtil implements Serializable {

	private static final long serialVersionUID = 8787468148263278421L;

	private static final String PATTERN_DNI = "^[0-9]{8}$";
	private static final String PATTERN_NIF = "^[0-9]{8}[A-Z]$";
	private static final String PATTERN_NIE = "^[XYZ][0-9]{7}[A-Z]$";

	private static final String STRING_Z = "z";

	private static final String STRING_Y = "y";

	private static final String STRING_X = "x";


	private static String[] letras = new String[] { "t", "r", "w", "a", "g", "m", STRING_Y, "f", "p", "d", STRING_X,
			"b", "n", "j", STRING_Z, "s", "q", "v", "h", "l", "c", "k", "e" };

	private static Integer dniInt;
	private static char[] letra1;
	private static String letra;
	private static String letraCorrecta;
	private static String dniNumeros;
	private static String nieNumeros;
	private static String letra_inicial;
	private static int ii;

	private static Logger log = Logger.getLogger(DNIValidatorUtil.class);

	private static final String STRING_DE_ASOCIACION_NIF = "TRWAGMYFPDXBNJZSQVHLCKE";

	/**
	 * @param documento
	 * @return si el documento tiene formato de DNI
	 */
	public static synchronized boolean isDniValidFormat(String documento) {
		return documento == null || documento.matches(PATTERN_DNI);
	}

	/**
	 * @param documento
	 * @return si el documento tiene formato de NIF
	 */
	public static synchronized boolean isNifValidFormat(String documento) {
		return documento == null || documento.matches(PATTERN_NIF);
	}

	/**
	 * @param documento
	 * @return si el documento tiene formato de NIE
	 */
	public static synchronized boolean isNieValidFormat(String documento) {
		return documento == null || documento.matches(PATTERN_NIE);
	}

	/**
	 * @param documento
	 * @return si el documento tiene formato de NIF o de NIE
	 */
	public static synchronized boolean isNifOrNieValidFormat(String documento) {
		return isNifValidFormat(documento) || isNieValidFormat(documento);
	}

	/**
	 * @param documento
	 * @return si el documento tiene formato de NIF o de NIE o de DNI
	 */
	public static synchronized boolean isNifOrNieOrDniValidFormat(String documento) {
		return isNifValidFormat(documento) || isNieValidFormat(documento) || isDniValidFormat(documento);
	}

	/**
	 * @param documento
	 * @return si el documento tiene formato de NIF o de NIE o de DNI
	 */
	public static synchronized boolean isNifOrNieOrDniOrDni00ValidFormat(String documento) {
		return isNifValidFormat(documento) || isNieValidFormat(documento) || isDniValidFormat(documento);
	}

	/**
	 * Comprueba si el valor especificado es un NIF
	 * 
	 * @param nif
	 * @return
	 */
	public static boolean isNIF(String nif) {
		try {
			return validarNIF(nif);
		} catch (NIFvalidationException e) {
			log.warn(e);
			return false;
		}
	}

	/**
	 * Comprueba si el valor especificado es un NIE
	 * 
	 * @param nie
	 * @return
	 */
	public static boolean isNIE(String nie) {
		try {
			return validarNIE(nie);
		} catch (NIEvalidationException e) {
			log.warn(e);
			return false;
		}
	}

	/**
	 * Comprueba si el valor especificado es un NIF o un NIE
	 * 
	 * @param documento
	 * @return
	 */
	public synchronized static boolean isNIFNIE(String documento) {
		return isNIF(documento) || isNIE(documento);
	}

	/**
	 * Calcula el NIF (DNI + letra) para un número de identificación dado
	 * 
	 * @param numerosDni
	 *            - Cadena de 8 números de un dni
	 * @return NIF con la letra calculada
	 * @throws ValidatorException
	 */
	public synchronized static String calcularNif(String numerosDni) throws ValidatorException {
		return calcularNif(numerosDni, false);
	}

	/**
	 * Calcula el NIF (DNI + letra) para un número de identificación dado
	 * 
	 * @param numerosDni
	 *            - Cadena de 8 números de un dni
	 * @param returnNullOnError
	 *            indica si, cuando no se puede calcular el NIF, se lanza
	 *            excepción o se devuelve nulo
	 * @return NIF con la letra calculada
	 * @throws ValidatorException
	 */
	public synchronized static String calcularNif(String numerosDni, boolean returnNullOnError)
			throws ValidatorException {
		if (numerosDni == null) {
			if (returnNullOnError) {
				return null;
			} else {
				throw new ValidatorException("no se puede calcular la letra del NIF de una cadena nula");
			}
		}
		if (numerosDni.length() > 8) {
			if (returnNullOnError) {
				return null;
			} else {
				throw new ValidatorException("el DNI debe tener un máximo de 8 caracteres numéricos");
			}
		}
		if (!StringUtils.isNumeric(numerosDni)) {
			if (returnNullOnError) {
				return null;
			} else {
				throw new ValidatorException("el DNI debe ser numérico para calcular la letra del NIF");
			}
		}
		if (numerosDni.length() < 2) {
			if (returnNullOnError) {
				return null;
			} else {
				throw new ValidatorException("el DNI debe tener al menos 1 caracter numéricos");
			}
		}

		int numeros = Integer.valueOf(numerosDni);
		return String.valueOf(numerosDni) + STRING_DE_ASOCIACION_NIF.charAt(numeros % 23);
	}
	
	public static boolean siLetraCorrecta(String documento, String numeroNumeros) {
		dniInt = Integer.parseInt(numeroNumeros);
		letra1 = new char[] { documento.charAt(documento.length() - 1) };
		letra = new String(letra1);
		letraCorrecta = letras[dniInt % 23];
		return letraCorrecta.equals(letra);
	}

	public static boolean validarNIF(String nif) throws NIFvalidationException {
		if (nif == null) {
			throw new NIFvalidationException("El Documento no puede estar vac\u00EDo.");
		}

		if (nif.length() != 9) {
			throw new NIFvalidationException("La longitud debe ser de 9 d\u00EDgitos.");
		}

		if (!isNifValidFormat(nif)) {
			throw new NIFvalidationException("El formato no corresponde a un NIF");
		}

		nif = nif.toLowerCase();
		dniNumeros = nif.substring(0, nif.length() - 1);

		if (!siLetraCorrecta(nif, dniNumeros)) {
			throw new NIFvalidationException("Letra incorrecta");
		}

		return true;

	}

	public static boolean validarDNI(String dni) throws DNIvalidationException {
		if (dni == null) {
			throw new DNIvalidationException("El Documento no puede estar vac\u00EDo.");
		}

		if (!isNumericInteger(dni)) {
			throw new DNIvalidationException("El DNI debe ser num\u00E9rico");
		}

		if (dni.length() > 8) {
			throw new DNIvalidationException("El DNI no puede exceder de 8 caracteres");
		}

		if (!dni.equals(quitarCaracterIzquierda(dni, '0').trim())) {
			throw new DNIvalidationException(
					"El DNI no puede tener ceros a la izquierda ni espacios en blanco a la derecha");
		}
		return true;
	}

	public static boolean validarNIE(String nie) throws NIEvalidationException {
		if (nie == null) {
			throw new NIEvalidationException("El Documento no puede estar vac\u00EDo.");
		}

		if (nie.length() != 9) {
			throw new NIEvalidationException("La longitud debe ser de 9 d\u00EDgitos.");
		}

		if (!isNieValidFormat(nie)) {
			throw new NIEvalidationException("El formato no corresponde a un NIE");
		}

		nie = nie.toLowerCase().trim();
		nieNumeros = nie.substring(1, nie.length() - 1);
		if (!isNumericInteger(nieNumeros)) {
			throw new NIEvalidationException("Formato incorrecto");
		}

		letra_inicial = nie.substring(0, 1);
		if (letra_inicial.equals(STRING_X) || letra_inicial.equals(STRING_Y) || letra_inicial.equals(STRING_Z)) {
			switch (letra_inicial) {
			case STRING_X:
				nieNumeros = "0" + nieNumeros;
				break;
			case STRING_Y:
				nieNumeros = "1" + nieNumeros;
				break;
			case STRING_Z:
				nieNumeros = "2" + nieNumeros;
				break;
			}

			if (!siLetraCorrecta(nie, nieNumeros)) {
				throw new NIEvalidationException("Letra final incorrecta");
			}

		} else {
			throw new NIEvalidationException("Letra inicial incorrecta");
		}
		return true;
	}

	public static boolean isNumericInteger(String n) {
		try {
			Long.parseLong(n);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}

	}
	
	public static String quitarCaracterIzquierda(String cadena, char character) {
		ii = -1;

		while (cadena.charAt(++ii) == character)
			;

		// en i está el índice del primer carácter no igual a character
		return cadena.substring(ii, cadena.length());

	}

	
}
