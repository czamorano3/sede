package es.imserso.sede.util.persistence;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;

import es.imserso.sede.data.validation.EntityValidator;
import es.imserso.sede.data.validation.PersistenceAction;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Gestiona las validaciones a realizar antes de persistir una entidad.
 * 
 * @author 11825775
 *
 */
public class PreListener {

	/**
	 * Es invocado antes de actualizar una entidad.
	 * 
	 * @param o
	 *            entidad que se va a actualizar
	 * @throws ValidationException
	 */
	@PreUpdate
	public void validateBeforeUpdate(@NotNull Object o) throws ValidationException {
		validate(o, PersistenceAction.ACTION_UPDATE);

	}

	/**
	 * Es invocado antes de persistir una entidad.
	 * 
	 * @param o
	 *            entidad que se va a persistir
	 * @throws ValidationException
	 */
	@PrePersist
	public void validateBeforePersist(@NotNull Object o) throws ValidationException {
		validate(o, PersistenceAction.ACTION_PERSIST);
	}

	/**
	 * Es invocado antes de eliminar una entidad.
	 * 
	 * @param o
	 *            entidad que se va a eliminar
	 * @throws ValidationException
	 */
	@PreRemove
	public void validateBeforeRemove(@NotNull Object o) throws ValidationException {
		validate(o, PersistenceAction.ACTION_REMOVE);
	}

	/**
	 * Realiza las validaciones oportunas sobre la entidad.
	 * 
	 * @param o
	 *            entidad
	 * @param acción
	 *            ejecutada sobre la entidad
	 * @throws ValidationException
	 */
	private void validate(@NotNull Object o, PersistenceAction action) throws ValidationException {
		(((EntityValidator) UtilsCDI.getBeanByContext(EntityValidator.class))).validate(o, action);
	}

}
