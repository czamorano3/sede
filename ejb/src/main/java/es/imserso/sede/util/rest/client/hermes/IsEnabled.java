/**
 * 
 */
package es.imserso.sede.util.rest.client.hermes;

import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Comprueba si los servicios REST de Hermes están escuchando.
 * 
 * @author 11825775
 *
 */
public class IsEnabled extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -8859087490617797183L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#execute()
	 */
	@Override
	public Object execute() throws SedeException {
		return (Object) ((responseCode) > 199 && (responseCode < 300));
	}

}
