package es.imserso.sede.util.rest.client.hermes;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Crea una nueva solicitud normal en Hermes.
 * 
 * @author 11825775
 *
 */
public class CreateSolicitud extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -8873587490617797183L;

	private static final Logger log = Logger.getLogger(CreateSolicitud.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#execute()
	 */
	@Override
	public Object execute() throws SedeException {
		
		SolicitudTurismoDTO outputDTO = response.readEntity(SolicitudTurismoDTO.class);
		if (outputDTO != null) {
			log.debug("se ha recibido correctamente el dto con la solicitud dada de alta en Hermes con el número de registro: "
					+ outputDTO.getNumeroRegistro());
		} else {
			log.warn("el DTO recibido del alta de solicitud en Hermes es nulo!!!");
		}

		return (Object) outputDTO;
	}

}