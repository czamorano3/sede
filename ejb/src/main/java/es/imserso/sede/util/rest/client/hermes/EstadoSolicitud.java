package es.imserso.sede.util.rest.client.hermes;

import org.jboss.logging.Logger;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Obtiene el estado de la solicitud especificada como parámetro (en {link
 * AbstractRestClient})
 * 
 * @author 11825775
 *
 */
public class EstadoSolicitud extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981232140987853780L;

	private static final Logger log = Logger.getLogger(EstadoSolicitud.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		String estado = null;

		try {
			estado = response.readEntity(String.class);
			if (estado != null) {
				log.debug(String.format("se ha recibido correctamente el estado de la solicitud: %s", estado));
			} else {
				log.warn("el estado de la solicitud recibido es nulo!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener el estado de la solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) estado;
	}

}
