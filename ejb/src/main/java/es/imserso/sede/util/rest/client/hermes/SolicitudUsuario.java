package es.imserso.sede.util.rest.client.hermes;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Obtiene la solicitud especificada como parámetro (en {link
 * AbstractRestClient})
 * 
 * @author 11825775
 *
 */
public class SolicitudUsuario extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -8765232141724853780L;

	private static final Logger log = Logger.getLogger(SolicitudUsuario.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		SolicitudTurismoDTO solicitudTurismoDTO = null;

		try {
			log.debug("leyendo response ...");
			solicitudTurismoDTO = response.readEntity(SolicitudTurismoDTO.class);
			if (solicitudTurismoDTO != null) {
				log.debug(String.format(
						"se ha recibido correctamente el dto con la solicitud dada de alta con el número de registro %s",
						solicitudTurismoDTO.getNumeroOrden()));
			} else {
				log.warn("el DTO recibido del alta de solicitud es nulo!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener carta de acreditación de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) solicitudTurismoDTO;
	}

}