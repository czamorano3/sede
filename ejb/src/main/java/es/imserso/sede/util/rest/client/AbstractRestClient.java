/**
 * 
 */
package es.imserso.sede.util.rest.client;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * @author 11825775
 *
 */
public abstract class AbstractRestClient implements RestClient {

	private static final long serialVersionUID = 8982681276152630391L;

	@Inject
	private Logger log;
	
	@Inject
	protected Event<EventMessage> eventMessage;

	protected ResteasyClient client = null;
	protected Response response = null;
	protected String responseMessageFromServer = "";
	protected int responseCode;
	protected String responseStatusMessage;
	protected Object param;

	/**
	 * Por defecto {@link HttpMethod.GET}
	 * @param url
	 * @return
	 * @throws SedeException
	 */
	public RestClient call(String url) throws SedeException {
		return call(url, null);
	}
	
	/**
	 * Por defecto {@link HttpMethod.POST}
	 * @param url
	 * @return
	 * @throws SedeException
	 */
	public RestClient call(String url, Object _param) throws SedeException {
		return call(url, HttpMethod.POST, _param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#call()
	 */
	@Override
	public RestClient call(String url, HttpMethod method, Object _param) throws SedeException {

		log.debug("iniciando cliente REST: " + url);
		
		param = _param;
		if (_param == null) {
			response = openClient().target(url).request().get();

		} else {
			String jsonDTO = buildJsonFromParam(_param);
			log.debug(jsonDTO);

			if (HttpMethod.GET.equals(method)) {
				//TODO implementar añadiendo al target el queryParam
				log.warn("funcionalidad a implementar");
			} else if (HttpMethod.POST.equals(method)) {
				response = openClient().target(url).request().post(Entity.entity(jsonDTO, MediaType.APPLICATION_JSON));
			} else if (HttpMethod.PUT.equals(method)) {
				response = openClient().target(url).request().put(Entity.entity(jsonDTO, MediaType.APPLICATION_JSON));
			} else {
				log.warnv("Método HTTP desconocido: {0}", method);
			}
		}

		manageResponse();

		return this;
	}

	/**
	 * Gestionamos la respuesta de forma genérica.
	 * 
	 * @throws SedeException
	 */
	protected void manageResponse() throws SedeException {

		responseCode = response.getStatus();
		log.debug("código de repuesta del servicio REST de la aplicación remota: " + responseCode);

		responseStatusMessage = response.getStatusInfo().getReasonPhrase();
		log.debug("mensaje de respuesta del servicio REST de la aplicación remota: " + responseStatusMessage);

		if (responseCode < 200 || responseCode > 299) {

			// Algo ha ido mal en la petición y gestionamos el error
			responseMessageFromServer = response.readEntity(String.class);
			log.debug("mensaje detallado de respuesta del servicio REST de la aplicación remota: "
					+ responseMessageFromServer);

			String errmsg = "(HTTP error : " + responseCode + "-" + responseStatusMessage + ") "
					+ responseMessageFromServer;
			log.error(errmsg);
			closeResponse();
			throw new SedeException(errmsg);
		}
	}

	/**
	 * Abre el cliente
	 */
	protected ResteasyClient openClient() {
		return (client = new ResteasyClientBuilder().build());
	}

	/**
	 * Cierra el cliente si está abierto
	 */
	protected void closeClient() {
		if (client != null) {
			client.close();
		}
	}

	/**
	 * Cierra la resouesta si está abierta
	 */
	protected void closeResponse() {
		if (response != null) {
			response.close();
		}
	}

	/**
	 * En el caso de que la petición tenga un parámetro, como un identificador, es
	 * necesario mapear el parámetro a JSON.
	 * 
	 * @param param
	 * @return
	 * @throws SedeException
	 */
	protected String buildJsonFromParam(Object param) throws SedeException {
		try {
			return new ObjectMapper().writeValueAsString(param);

		} catch (JsonProcessingException e) {
			closeResponse();
			String errmsg = "Error al mapear el parámetro a JSON: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg);
		}
	}

}
