package es.imserso.sede.util.persistence.service;

import javax.inject.Inject;

import es.imserso.sede.config.PropertyComponent;

/**
 * Este componente se encarga de la gestión del contexto de persistencia y las
 * transacciones de un proceso.
 * <p>
 * Como el contexto de persistencia del proceso ha de ser visible para algunos
 * componentes de la aplicación (que deben estar en la misma transacción del
 * proceso para trabajar con los datos correctos), este componente se expone en
 * el contexto SESSION.
 * <p>
 * Cada proceso tendrá asociada su propia instancia de este componente.
 * 
 * @author 11825775
 * 
 * 
 */
public class ProcessPersistenceManager extends AbstractPersistenceManager {

	private static final long serialVersionUID = -5429546708863482731L;

	@Inject
	protected PropertyComponent propertyComponent;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.hermes.session.util.persistence.service.
	 * AbstractPersistenceManager #getTransactionTimeoutSeconds()
	 */
	@Override
	public int getTransactionTimeoutSeconds() {
		return propertyComponent.getProcessTransactionTimeoutSeconds();
	}

}
