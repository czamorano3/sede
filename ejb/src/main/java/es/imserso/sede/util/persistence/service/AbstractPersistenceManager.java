package es.imserso.sede.util.persistence.service;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Status;
import javax.transaction.UserTransaction;

import org.jboss.logging.Logger;

import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

abstract public class AbstractPersistenceManager implements Serializable {

	private static final long serialVersionUID = 2982742575816236929L;

	@Inject
	protected Logger log;
	/**
	 * para obtener el contexto de persistencia y la transacción del proceso.
	 */
	@Inject
	protected PersistenceService persistenceService;

	/**
	 * transacción del contenedor
	 */
	protected UserTransaction containerTransaction;
	/**
	 * contexto de persistencia del contenedor
	 */
	protected EntityManager containerEntityManager;

	public AbstractPersistenceManager() {
		super();
	}

	/**
	 * @return contexto de persistencia del proceso con la transacción del
	 *         proceso
	 */
	public EntityManager getContainerEntityManager() {
		if (containerEntityManager == null) {

			// obtenemos un nuevo contexto de persistencia del contenedor
			containerEntityManager = persistenceService
					.getNewContainerEntityManager();
		}
		return containerEntityManager;
	}

	public void setContainerEntityManager(EntityManager entityManager) {
		this.containerEntityManager = entityManager;
	}

	/**
	 * @return transacción del contenedor
	 */
	public UserTransaction getContainerTransaction() {
		return containerTransaction;
	}

	public void setContainerTransaction(UserTransaction containerTransaction) {
		this.containerTransaction = containerTransaction;
	}

	/**
	 * Empieza la transacción del proceso.
	 * 
	 * @throws ProcesoException
	 */
	public void beginTransaction() throws SedeRuntimeException {
		try {

			if (containerTransaction == null) {
				log.debug("obtenemos una nueva transacción");
				containerTransaction = persistenceService.getNewTransaction();
			}

			persistenceService.logTransactionStatus(containerTransaction);

			if (containerTransaction.getStatus() == Status.STATUS_ACTIVE) {
				log.debug("hacemos rollback de la transacción para poder establecer el timeout y volver a iniciarla");
				containerTransaction.rollback();
			}

			log.debug("estableciendo timeout a "
					+ getTransactionTimeoutSeconds() + " segundos...");

			containerTransaction
					.setTransactionTimeout(getTransactionTimeoutSeconds());
			log.debug("\tcomienzando transacción...");

			containerTransaction.begin();
			log.debug("\ttransacción comenzada!!");

			if (containerTransaction.getStatus() != Status.STATUS_ACTIVE) {
				log.warn("la transacción debería estar en estado ACTIVE, pero está en estado "
						+ containerTransaction.getStatus());
			}
			getContainerEntityManager().joinTransaction();
			log.debug("\tcontexto de persistencia enlazado con la transacción");

		} catch (Exception e) {
			throw new SedeRuntimeException(
					"excepción al hacer el begin de la transacción: "
							+ Utils.getExceptionMessage(e));
		}
	}

	abstract public int getTransactionTimeoutSeconds();

	/**
	 * Hace commit de la transacción del proceso.
	 * 
	 * @throws ProcesoException
	 */
	public void commitTransaction() throws ProcessException {
		try {
			if (containerTransaction != null) {
				log.trace("\thaciendo commit de la transacción...");
				// getProcessEntityManager().joinTransaction();
				containerTransaction.commit();
				log.debug("\tcommit de la transacción realizado con éxito!!");

			} else {
				throw new ProcessException(
						"al intentar hacer el commit se ha detectado que la transacción es null");
			}
		} catch (Exception e) {
			throw new ProcessException(
					"excepción al hacer el commit de la transacción: "
							+ Utils.getExceptionMessage(e));
		}
	}

	/**
	 * Hace rollback de la transacción del proceso.
	 * 
	 * @throws ProcesoException
	 */
	public void rollbackTransaction() throws ProcessException {
		try {
			if (containerTransaction != null) {
				log.debug("\n\thaciendo rollback de la transacción...\n");
				// getProcessEntityManager().joinTransaction();
				containerTransaction.rollback();
				log.debug("\n\trollback de la transacción realizado con éxito!!\n");

			} else {
				throw new ProcessException(
						"al intentar hacer el rollback se ha detectado que la transacción es null");
			}

		} catch (Exception e) {
			throw new ProcessException(
					"excepción al hacer el rollback de la transacción: "
							+ Utils.getExceptionMessage(e));
		}
	}

}