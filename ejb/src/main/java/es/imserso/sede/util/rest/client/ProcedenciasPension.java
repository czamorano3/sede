package es.imserso.sede.util.rest.client;

import java.util.List;

import javax.ws.rs.core.GenericType;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.ProcedenciaPensionDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Obtiene la lista de provincias
 * 
 * @author 11825775
 *
 */
public class ProcedenciasPension extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981237771987852340L;

	private static final Logger log = Logger.getLogger(ProcedenciasPension.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		List<ProcedenciaPensionDTO> list = null;

		try {
			log.debug("leyendo response ...");
			GenericType<List<ProcedenciaPensionDTO>> genericType = new GenericType<List<ProcedenciaPensionDTO>>() {
			};
			list = response.readEntity(genericType);

		} catch (Exception e) {
			String errmsg = String.format("Error al obtener las procedencias de pensión: %s",
					Utils.getExceptionMessage(e));
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) list;
	}

}
