package es.imserso.sede.service.registration.solicitud.documentos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;
import es.imserso.sede.util.exception.SedeException;

public class DocumentosRegistradosManager implements Serializable {

	private static final long serialVersionUID = 2372478774840732159L;

	private List<DocumentoRegistrado> adjuntos;
	
	public DocumentosRegistradosManager(List<DocumentoRegistrado> documentos) {
		this.adjuntos = documentos;
	}

	public DocumentoRegistrado getDocumentoSolicitud() throws SedeException {
		DocumentoRegistrado adjunto = null;
		for (DocumentoRegistrado documento : adjuntos) {
			if (TipoDocumentoRegistrado.SOLICITUD.equals(documento.getTipoDocumentoRegistrado())) {
				adjunto = documento;
				break;
			}
		}
		return adjunto;
	}
	
	public DocumentoRegistrado getDocumentoSolicitudAdjunta() throws SedeException {
		DocumentoRegistrado adjunto = null;
		for (DocumentoRegistrado documento : adjuntos) {
			if (TipoDocumentoRegistrado.SOLICITUD_ADJUNTA.equals(documento.getTipoDocumentoRegistrado())) {
				adjunto = documento;
				break;
			}
		}
		return adjunto;
	}

	public DocumentoRegistrado getDocumentoJustificante() throws SedeException {
		DocumentoRegistrado adjunto = null;
		for (DocumentoRegistrado documento : adjuntos) {
			if (TipoDocumentoRegistrado.JUSTIFICANTE.equals(documento.getTipoDocumentoRegistrado())) {
				adjunto = documento;
				break;
			}
		}
		return adjunto;
	}

	public List<DocumentoRegistrado> getDocumentosAdjuntados() throws SedeException {
		List<DocumentoRegistrado> attached = new ArrayList<DocumentoRegistrado>();
		for (DocumentoRegistrado documento : adjuntos) {
			if (TipoDocumentoRegistrado.PERSONAL.equals(documento.getTipoDocumentoRegistrado())) {
				attached.add(documento);
			}
		}
		return attached;
	}
}