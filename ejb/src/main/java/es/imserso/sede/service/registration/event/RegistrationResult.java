package es.imserso.sede.service.registration.event;

import java.io.Serializable;

public class RegistrationResult implements Serializable {
	
	private static final long serialVersionUID = 1105849907153565178L;

	public enum Result {
		RESULT_OK, RESULT_KO
	}
	
	private Result result;
	private String data;
	
	public RegistrationResult(Result result) {
		this.result = result;
	}
	
	public RegistrationResult(Result result, String data) {
		this.result = result;
		this.data = data;
	}
	
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}

