package es.imserso.sede.service.registration.registry.bean;

import javax.xml.datatype.XMLGregorianCalendar;

public interface InputRegisterResponseI {

	String getNumber();

	void setNumber(String number);

	XMLGregorianCalendar getDate();

	void setDate(XMLGregorianCalendar date);

	String getUserName();

	void setUserName(String userName);

	XMLGregorianCalendar getSystemDate();

	void setSystemDate(XMLGregorianCalendar systemDate);

	String getOffice();

	void setOffice(String office);

	String getOfficeName();

	void setOfficeName(String officeName);

	int getBookId();

	void setBookId(int bookId);

	int getFolderId();

	void setFolderId(int folderId);

	int getState();

	void setState(int state);

}