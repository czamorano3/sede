package es.imserso.sede.service.message;

import es.imserso.sede.util.exception.SedeException;

/**
 * Esta clase y sus subclases son una forma de {@code Exception} que indica que
 * no se ha podido realizar satisfactoriamente la gestión de los mensajes de la
 * aplicación.
 * 
 * @author 11825775
 *
 */
public class MessageException extends SedeException {

	private static final long serialVersionUID = -7155085688438793846L;

	protected static final String DEFAULT_MESSAGE = "Error en validación";

	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public MessageException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public MessageException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public MessageException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public MessageException(Throwable cause) {
		super(cause);
	}
}
