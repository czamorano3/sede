package es.imserso.sede.service.message.event;

/**
 * Origen de los mensajes de evento.
 * 
 * @author 11825775
 *
 */
public enum EventMessageSource {
	APLICACION("APLICACION"), 
	ALTA("ALTA DE SOLICITUD"), 
	MODIFICACION("MODIFICACION DE SOLICITUD"), 
	CONSULTA("CONSULTA DE SOLICITUDES"), 
	SYNC_HERMES("SINCRONIZACION CON HERMES"), 
	REST_HERMES("SERVICIOS REST DE HERMES"), 
	REST_TERMALISMO("SERVICIOS REST DE TERMALISMO"), 
	REST_UCM("SERVICIOS REST DE UCM"), 
	SYNC_TERM("SINCRONIZACION CON TERMALISMO"), 
	ADMIN("ADMINISTRACION DE LA SEDE"),
	WEB_SERVICES_MONITOR("MONITOR DE LOS SERVICIOS WEB DE LAS APLICACIONES REMOTAS");

	String description;

	private EventMessageSource(String description) {
		this.description = description;
	}

	/**
	 * @return descripción del origen de mensaje de evento.
	 */
	public String getDescription() {
		return this.description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.description;
	}

}
