
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class RegistersObjectFactory {

    private final static QName _Security_QNAME = new QName("http://schemas.xmlsoap.org/ws/2002/04/secext", "Security");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client
     * 
     */
    public RegistersObjectFactory() {
    }

    /**
     * Create an instance of {@link WSCancelOutputRegister }
     * 
     */
    public WSCancelOutputRegister createWSCancelOutputRegister() {
        return new WSCancelOutputRegister();
    }

    /**
     * Create an instance of {@link WSNewInputRegister }
     * 
     */
    public WSNewInputRegister createWSNewInputRegister() {
        return new WSNewInputRegister();
    }

    /**
     * Create an instance of {@link WSParamInputRegisterEx }
     * 
     */
    public WSParamInputRegisterEx createWSParamInputRegisterEx() {
        return new WSParamInputRegisterEx();
    }

    /**
     * Create an instance of {@link WSUpdateInputRegisterResponse }
     * 
     */
    public WSUpdateInputRegisterResponse createWSUpdateInputRegisterResponse() {
        return new WSUpdateInputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSLoadOutputRegistersEx }
     * 
     */
    public WSLoadOutputRegistersEx createWSLoadOutputRegistersEx() {
        return new WSLoadOutputRegistersEx();
    }

    /**
     * Create an instance of {@link WSImportOutputRegister }
     * 
     */
    public WSImportOutputRegister createWSImportOutputRegister() {
        return new WSImportOutputRegister();
    }

    /**
     * Create an instance of {@link WSParamOutputRegisterEx }
     * 
     */
    public WSParamOutputRegisterEx createWSParamOutputRegisterEx() {
        return new WSParamOutputRegisterEx();
    }

    /**
     * Create an instance of {@link WSLoadInputRegistersResponse }
     * 
     */
    public WSLoadInputRegistersResponse createWSLoadInputRegistersResponse() {
        return new WSLoadInputRegistersResponse();
    }

    /**
     * Create an instance of {@link WSInputRegistersResponse }
     * 
     */
    public WSInputRegistersResponse createWSInputRegistersResponse() {
        return new WSInputRegistersResponse();
    }

    /**
     * Create an instance of {@link WSLoadOutputRegisterFromId }
     * 
     */
    public WSLoadOutputRegisterFromId createWSLoadOutputRegisterFromId() {
        return new WSLoadOutputRegisterFromId();
    }

    /**
     * Create an instance of {@link WSLoadInputRegisters }
     * 
     */
    public WSLoadInputRegisters createWSLoadInputRegisters() {
        return new WSLoadInputRegisters();
    }

    /**
     * Create an instance of {@link WSCancelInputRegister }
     * 
     */
    public WSCancelInputRegister createWSCancelInputRegister() {
        return new WSCancelInputRegister();
    }

    /**
     * Create an instance of {@link WSImportInputRegisterResponse }
     * 
     */
    public WSImportInputRegisterResponse createWSImportInputRegisterResponse() {
        return new WSImportInputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSRegister }
     * 
     */
    public WSRegister createWSRegister() {
        return new WSRegister();
    }

    /**
     * Create an instance of {@link WSAttachPageResponse }
     * 
     */
    public WSAttachPageResponse createWSAttachPageResponse() {
        return new WSAttachPageResponse();
    }

    /**
     * Create an instance of {@link WSUpdateOutputRegister }
     * 
     */
    public WSUpdateOutputRegister createWSUpdateOutputRegister() {
        return new WSUpdateOutputRegister();
    }

    /**
     * Create an instance of {@link ArrayOfWSParamField }
     * 
     */
    public ArrayOfWSParamField createArrayOfWSParamField() {
        return new ArrayOfWSParamField();
    }

    /**
     * Create an instance of {@link WSAttachPageExResponse }
     * 
     */
    public WSAttachPageExResponse createWSAttachPageExResponse() {
        return new WSAttachPageExResponse();
    }

    /**
     * Create an instance of {@link WSDocumentsResponse }
     * 
     */
    public WSDocumentsResponse createWSDocumentsResponse() {
        return new WSDocumentsResponse();
    }

    /**
     * Create an instance of {@link WSLoadInputRegisterFromId }
     * 
     */
    public WSLoadInputRegisterFromId createWSLoadInputRegisterFromId() {
        return new WSLoadInputRegisterFromId();
    }

    /**
     * Create an instance of {@link WSImportInputRegister }
     * 
     */
    public WSImportInputRegister createWSImportInputRegister() {
        return new WSImportInputRegister();
    }

    /**
     * Create an instance of {@link WSLoadOutputRegisters }
     * 
     */
    public WSLoadOutputRegisters createWSLoadOutputRegisters() {
        return new WSLoadOutputRegisters();
    }

    /**
     * Create an instance of {@link WSLoadOutputRegistersExResponse }
     * 
     */
    public WSLoadOutputRegistersExResponse createWSLoadOutputRegistersExResponse() {
        return new WSLoadOutputRegistersExResponse();
    }

    /**
     * Create an instance of {@link WSOutputRegistersResponse }
     * 
     */
    public WSOutputRegistersResponse createWSOutputRegistersResponse() {
        return new WSOutputRegistersResponse();
    }

    /**
     * Create an instance of {@link WSUpdateOutputRegisterResponse }
     * 
     */
    public WSUpdateOutputRegisterResponse createWSUpdateOutputRegisterResponse() {
        return new WSUpdateOutputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSGetPageExResponse }
     * 
     */
    public WSGetPageExResponse createWSGetPageExResponse() {
        return new WSGetPageExResponse();
    }

    /**
     * Create an instance of {@link WSGetPageResponse }
     * 
     */
    public WSGetPageResponse createWSGetPageResponse() {
        return new WSGetPageResponse();
    }

    /**
     * Create an instance of {@link WSLoadOutputRegisterFromIdResponse }
     * 
     */
    public WSLoadOutputRegisterFromIdResponse createWSLoadOutputRegisterFromIdResponse() {
        return new WSLoadOutputRegisterFromIdResponse();
    }

    /**
     * Create an instance of {@link WSOutputRegister }
     * 
     */
    public WSOutputRegister createWSOutputRegister() {
        return new WSOutputRegister();
    }

    /**
     * Create an instance of {@link WSNewOutputRegister }
     * 
     */
    public WSNewOutputRegister createWSNewOutputRegister() {
        return new WSNewOutputRegister();
    }

    /**
     * Create an instance of {@link WSNewOutputRegisterResponse }
     * 
     */
    public WSNewOutputRegisterResponse createWSNewOutputRegisterResponse() {
        return new WSNewOutputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSLoadInputRegistersExResponse }
     * 
     */
    public WSLoadInputRegistersExResponse createWSLoadInputRegistersExResponse() {
        return new WSLoadInputRegistersExResponse();
    }

    /**
     * Create an instance of {@link WSUpdateInputRegister }
     * 
     */
    public WSUpdateInputRegister createWSUpdateInputRegister() {
        return new WSUpdateInputRegister();
    }

    /**
     * Create an instance of {@link WSGetPageEx }
     * 
     */
    public WSGetPageEx createWSGetPageEx() {
        return new WSGetPageEx();
    }

    /**
     * Create an instance of {@link WSImportOutputRegisterResponse }
     * 
     */
    public WSImportOutputRegisterResponse createWSImportOutputRegisterResponse() {
        return new WSImportOutputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSNewInputRegisterResponse }
     * 
     */
    public WSNewInputRegisterResponse createWSNewInputRegisterResponse() {
        return new WSNewInputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSAttachPageEx }
     * 
     */
    public WSAttachPageEx createWSAttachPageEx() {
        return new WSAttachPageEx();
    }

    /**
     * Create an instance of {@link ArrayOfWSParamDocument }
     * 
     */
    public ArrayOfWSParamDocument createArrayOfWSParamDocument() {
        return new ArrayOfWSParamDocument();
    }

    /**
     * Create an instance of {@link WSAttachPage }
     * 
     */
    public WSAttachPage createWSAttachPage() {
        return new WSAttachPage();
    }

    /**
     * Create an instance of {@link WSLoadInputRegisterFromIdResponse }
     * 
     */
    public WSLoadInputRegisterFromIdResponse createWSLoadInputRegisterFromIdResponse() {
        return new WSLoadInputRegisterFromIdResponse();
    }

    /**
     * Create an instance of {@link WSInputRegister }
     * 
     */
    public WSInputRegister createWSInputRegister() {
        return new WSInputRegister();
    }

    /**
     * Create an instance of {@link WSGetPage }
     * 
     */
    public WSGetPage createWSGetPage() {
        return new WSGetPage();
    }

    /**
     * Create an instance of {@link WSCancelOutputRegisterResponse }
     * 
     */
    public WSCancelOutputRegisterResponse createWSCancelOutputRegisterResponse() {
        return new WSCancelOutputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSLoadInputRegistersEx }
     * 
     */
    public WSLoadInputRegistersEx createWSLoadInputRegistersEx() {
        return new WSLoadInputRegistersEx();
    }

    /**
     * Create an instance of {@link WSCancelInputRegisterResponse }
     * 
     */
    public WSCancelInputRegisterResponse createWSCancelInputRegisterResponse() {
        return new WSCancelInputRegisterResponse();
    }

    /**
     * Create an instance of {@link WSLoadOutputRegistersResponse }
     * 
     */
    public WSLoadOutputRegistersResponse createWSLoadOutputRegistersResponse() {
        return new WSLoadOutputRegistersResponse();
    }

    /**
     * Create an instance of {@link WSParamPerson }
     * 
     */
    public WSParamPerson createWSParamPerson() {
        return new WSParamPerson();
    }

    /**
     * Create an instance of {@link ArrayOfWSParamPerson }
     * 
     */
    public ArrayOfWSParamPerson createArrayOfWSParamPerson() {
        return new ArrayOfWSParamPerson();
    }

    /**
     * Create an instance of {@link WSDocument }
     * 
     */
    public WSDocument createWSDocument() {
        return new WSDocument();
    }

    /**
     * Create an instance of {@link WSParamOutputRegister }
     * 
     */
    public WSParamOutputRegister createWSParamOutputRegister() {
        return new WSParamOutputRegister();
    }

    /**
     * Create an instance of {@link WSParamDocument }
     * 
     */
    public WSParamDocument createWSParamDocument() {
        return new WSParamDocument();
    }

    /**
     * Create an instance of {@link ArrayOfWSInputRegister }
     * 
     */
    public ArrayOfWSInputRegister createArrayOfWSInputRegister() {
        return new ArrayOfWSInputRegister();
    }

    /**
     * Create an instance of {@link ArrayOfWSDocument }
     * 
     */
    public ArrayOfWSDocument createArrayOfWSDocument() {
        return new ArrayOfWSDocument();
    }

    /**
     * Create an instance of {@link ArrayOfWSPerson }
     * 
     */
    public ArrayOfWSPerson createArrayOfWSPerson() {
        return new ArrayOfWSPerson();
    }

    /**
     * Create an instance of {@link WSParamField }
     * 
     */
    public WSParamField createWSParamField() {
        return new WSParamField();
    }

    /**
     * Create an instance of {@link ArrayOfWSOutputRegister }
     * 
     */
    public ArrayOfWSOutputRegister createArrayOfWSOutputRegister() {
        return new ArrayOfWSOutputRegister();
    }

    /**
     * Create an instance of {@link ArrayOfWSAddField }
     * 
     */
    public ArrayOfWSAddField createArrayOfWSAddField() {
        return new ArrayOfWSAddField();
    }

    /**
     * Create an instance of {@link WSPerson }
     * 
     */
    public WSPerson createWSPerson() {
        return new WSPerson();
    }

    /**
     * Create an instance of {@link WSAddField }
     * 
     */
    public WSAddField createWSAddField() {
        return new WSAddField();
    }

    /**
     * Create an instance of {@link WSPage }
     * 
     */
    public WSPage createWSPage() {
        return new WSPage();
    }

    /**
     * Create an instance of {@link ArrayOfWSPage }
     * 
     */
    public ArrayOfWSPage createArrayOfWSPage() {
        return new ArrayOfWSPage();
    }

    /**
     * Create an instance of {@link WSParamInputRegister }
     * 
     */
    public WSParamInputRegister createWSParamInputRegister() {
        return new WSParamInputRegister();
    }

    /**
     * Create an instance of {@link Security }
     * 
     */
    public Security createSecurity() {
        return new Security();
    }

    /**
     * Create an instance of {@link UsernameTokenClass }
     * 
     */
    public UsernameTokenClass createUsernameTokenClass() {
        return new UsernameTokenClass();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Security }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.xmlsoap.org/ws/2002/04/secext", name = "Security")
    public JAXBElement<Security> createSecurity(Security value) {
        return new JAXBElement<Security>(_Security_QNAME, Security.class, null, value);
    }

}
