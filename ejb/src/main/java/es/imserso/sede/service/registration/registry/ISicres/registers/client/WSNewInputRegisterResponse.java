
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSNewInputRegisterResult" type="{http://www.invesicres.org}WSRegister" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsNewInputRegisterResult"
})
@XmlRootElement(name = "WSNewInputRegisterResponse")
public class WSNewInputRegisterResponse {

    @XmlElement(name = "WSNewInputRegisterResult")
    protected WSRegister wsNewInputRegisterResult;

    /**
     * Obtiene el valor de la propiedad wsNewInputRegisterResult.
     * 
     * @return
     *     possible object is
     *     {@link WSRegister }
     *     
     */
    public WSRegister getWSNewInputRegisterResult() {
        return wsNewInputRegisterResult;
    }

    /**
     * Define el valor de la propiedad wsNewInputRegisterResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSRegister }
     *     
     */
    public void setWSNewInputRegisterResult(WSRegister value) {
        this.wsNewInputRegisterResult = value;
    }

}
