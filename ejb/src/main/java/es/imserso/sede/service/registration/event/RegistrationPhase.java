package es.imserso.sede.service.registration.event;

/**
 * Fases del registro de una solicitud.
 * 
 * @author 11825775
 *
 */
public enum RegistrationPhase {
	BEFORE_INIT, INIT, AFTER_INIT, VIEW_LOAD, VIEW_SAVE, BEFORE_VALIDATION, VALIDATION, AFTER_VALIDATION, BEFORE_CONVERT_TO_PDF, CONVERT_TO_PDF, AFTER_CONVERT_TO_PDF, BEFORE_PERSIST_ON_BBDD, PERSIST_ON_BBDD, AFTER_PERSIST_ON_BBDD, BEFORE_PERSIST_ON_REGISTER, PERSIST_ON_REGISTER, AFTER_PERSIST_ON_REGISTER, GENERATE_RECEIPT, SIGN_RECEIPT, BEFORE_PERSIST_RECEIPT_ON_REGISTER, PERSIST_RECEIPT_ON_REGISTER, AFTER_PERSIST_RECEIPT_ON_REGISTER, BEFORE_UPDATE_ON_BBDD, UPDATE_ON_BBDD, AFTER_UPDATE_ON_BBDD, BEFORE_TERMINATE, TERMINATE_OK, TERMINATE_KO, AFTER_TERMINATE, REGISTERED_OK, REGISTERED_KO;
}
