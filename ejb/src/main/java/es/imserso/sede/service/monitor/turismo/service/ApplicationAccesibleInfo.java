package es.imserso.sede.service.monitor.turismo.service;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.util.exception.SedeException;

/**
 * Información sobre la accesibilidad a la aplicación Hermes
 * <p>
 * Por ejemplo, la aplicación puede estar bloqueada
 * 
 * @author 11825775
 *
 */
@Dependent
public class ApplicationAccesibleInfo implements ServiceInfo {

	private static final long serialVersionUID = -3501092722567399456L;

	@Inject
	TurismoRepository turismoRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isCritical()
	 */
	@Override
	public Boolean isCritical() {
		return Boolean.TRUE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return "Aplicación desbloqueada";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceDescription()
	 */
	@Override
	public String getServiceDescription() {
		return "Información sobre si la aplicación está accesible/desbloqueada";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isEnabled()
	 */
	@Override
	public Boolean isEnabled() {
		try {
			return turismoRepository.isAplicacionDesbloqueada();
		} catch (SedeException e) {
			return Boolean.FALSE;
		}
	}

}
