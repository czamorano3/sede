package es.imserso.sede.service.registration.registry.ISicres.registers.bean;

import javax.xml.datatype.XMLGregorianCalendar;

import es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI;

/**
 * Bean que contiene la información devuelta por el registro al crear una nueva
 * entrada.
 * 
 * @author 02858341
 *
 */
public class InputRegisterResponse implements InputRegisterResponseI {

	protected String number;
	protected XMLGregorianCalendar date;
	protected String userName;
	protected XMLGregorianCalendar systemDate;
	protected String office;
	protected String officeName;
	protected int bookId;
	protected int folderId;
	protected int state;

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getNumber()
	 */
	@Override
	public String getNumber() {
		return number;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setNumber(java.lang.String)
	 */
	@Override
	public void setNumber(String number) {
		this.number = number;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getDate()
	 */
	@Override
	public XMLGregorianCalendar getDate() {
		return date;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setDate(javax.xml.datatype.XMLGregorianCalendar)
	 */
	@Override
	public void setDate(XMLGregorianCalendar date) {
		this.date = date;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getUserName()
	 */
	@Override
	public String getUserName() {
		return userName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setUserName(java.lang.String)
	 */
	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getSystemDate()
	 */
	@Override
	public XMLGregorianCalendar getSystemDate() {
		return systemDate;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setSystemDate(javax.xml.datatype.XMLGregorianCalendar)
	 */
	@Override
	public void setSystemDate(XMLGregorianCalendar systemDate) {
		this.systemDate = systemDate;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getOffice()
	 */
	@Override
	public String getOffice() {
		return office;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setOffice(java.lang.String)
	 */
	@Override
	public void setOffice(String office) {
		this.office = office;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getOfficeName()
	 */
	@Override
	public String getOfficeName() {
		return officeName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setOfficeName(java.lang.String)
	 */
	@Override
	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getBookId()
	 */
	@Override
	public int getBookId() {
		return bookId;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setBookId(int)
	 */
	@Override
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getFolderId()
	 */
	@Override
	public int getFolderId() {
		return folderId;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setFolderId(int)
	 */
	@Override
	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#getState()
	 */
	@Override
	public int getState() {
		return state;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI#setState(int)
	 */
	@Override
	public void setState(int state) {
		this.state = state;
	}

}
