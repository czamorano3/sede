
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSAttachPageExResult" type="{http://www.invesicres.org}WSDocumentsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsAttachPageExResult"
})
@XmlRootElement(name = "WSAttachPageExResponse")
public class WSAttachPageExResponse {

    @XmlElement(name = "WSAttachPageExResult")
    protected WSDocumentsResponse wsAttachPageExResult;

    /**
     * Obtiene el valor de la propiedad wsAttachPageExResult.
     * 
     * @return
     *     possible object is
     *     {@link WSDocumentsResponse }
     *     
     */
    public WSDocumentsResponse getWSAttachPageExResult() {
        return wsAttachPageExResult;
    }

    /**
     * Define el valor de la propiedad wsAttachPageExResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSDocumentsResponse }
     *     
     */
    public void setWSAttachPageExResult(WSDocumentsResponse value) {
        this.wsAttachPageExResult = value;
    }

}
