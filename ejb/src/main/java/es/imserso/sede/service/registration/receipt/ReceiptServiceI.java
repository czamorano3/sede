package es.imserso.sede.service.registration.receipt;

/**
 * Interfaz que ofrece las funcionalidades relacionadas con los justificantes.
 * 
 * @author 11825775
 *
 */
public interface ReceiptServiceI {
	
	/**
	 * Comprueba si el servicio está activo.
	 * 
	 * @return Si los servicios web están o no escuchando.
	 * @throws ReceiptServiceException
	 */
	public Boolean isAlive() throws ReceiptServiceException;
	
	/**
	 * Comprueba si funcionan los servicios
	 * 
	 * @return Si los servicios web están o no funcionando.
	 * @throws ReceiptServiceException
	 */
	public Boolean testServices() throws ReceiptServiceException;

	/**
	 * Firma el justificante.
	 * 
	 * @param receipt
	 *            justificante a firmar
	 * @return justificante firmado
	 * @throws ReceiptServiceException
	 */
	public byte[] signReceipt(byte[] receipt) throws ReceiptServiceException;

}
