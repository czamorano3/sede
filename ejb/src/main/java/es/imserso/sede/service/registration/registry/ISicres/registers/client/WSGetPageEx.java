
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RegisterIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DocumentId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PageId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookIdentification",
    "registerIdentification",
    "documentId",
    "pageId"
})
@XmlRootElement(name = "WSGetPageEx")
public class WSGetPageEx {

    @XmlElement(name = "BookIdentification")
    protected int bookIdentification;
    @XmlElement(name = "RegisterIdentification")
    protected int registerIdentification;
    @XmlElement(name = "DocumentId")
    protected int documentId;
    @XmlElement(name = "PageId")
    protected int pageId;

    /**
     * Obtiene el valor de la propiedad bookIdentification.
     * 
     */
    public int getBookIdentification() {
        return bookIdentification;
    }

    /**
     * Define el valor de la propiedad bookIdentification.
     * 
     */
    public void setBookIdentification(int value) {
        this.bookIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad registerIdentification.
     * 
     */
    public int getRegisterIdentification() {
        return registerIdentification;
    }

    /**
     * Define el valor de la propiedad registerIdentification.
     * 
     */
    public void setRegisterIdentification(int value) {
        this.registerIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad documentId.
     * 
     */
    public int getDocumentId() {
        return documentId;
    }

    /**
     * Define el valor de la propiedad documentId.
     * 
     */
    public void setDocumentId(int value) {
        this.documentId = value;
    }

    /**
     * Obtiene el valor de la propiedad pageId.
     * 
     */
    public int getPageId() {
        return pageId;
    }

    /**
     * Define el valor de la propiedad pageId.
     * 
     */
    public void setPageId(int value) {
        this.pageId = value;
    }

}
