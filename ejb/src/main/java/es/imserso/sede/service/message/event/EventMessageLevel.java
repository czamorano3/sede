package es.imserso.sede.service.message.event;

/**
 * Niveles de los mensajes de evento.
 * 
 * @author 11825775
 *
 */
public enum EventMessageLevel {

	MESSAGE_LEVEL_TRACE(0, "TRACE", "green"), MESSAGE_LEVEL_DEBUG(1, "DEBUG", "green"), MESSAGE_LEVEL_INFO(2, "INFO",
			"blue"), MESSAGE_LEVEL_WARN(3, "WARN",
					"orange"), MESSAGE_LEVEL_ERROR(4, "ERROR", "red"), MESSAGE_LEVEL_FATAL(5, "FATAL", "red");

	int priority;
	String description;
	String associatedColor;

	private EventMessageLevel(int priority, String description, String associatedColor) {
		this.priority = priority;
		this.description = description;
		this.associatedColor = associatedColor;
	}

	/**
	 * @return prioridad del nivel de mensaje de evento.
	 */
	public int getPriority() {
		return this.priority;
	}

	/**
	 * @return descripción del nivel de mensaje de evento.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @return color asociado al nivel de mensaje de evento.
	 */
	public String getAssociatedColor() {
		return this.associatedColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.description;
	}

}
