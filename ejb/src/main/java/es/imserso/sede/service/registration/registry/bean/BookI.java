package es.imserso.sede.service.registration.registry.bean;

public interface BookI {

	/**
	 * @return the name
	 */
	String getName();

	/**
	 * @param name
	 *            the name to set
	 */
	void setName(String name);

	/**
	 * @return the id
	 */
	int getId();

	/**
	 * @param id
	 *            the id to set
	 */
	void setId(int id);

	/**
	 * @return the type
	 */
	int getType();

	/**
	 * @param type
	 *            the type to set
	 */
	void setType(int type);

}