package es.imserso.sede.service.registration.solicitud.documentos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSPage;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Gestiona los documentos de la solicitud para que sea más cómodo manejarlos
 * 
 * @author 11825775
 *
 */
public class DocumentosRegistradosService implements Serializable {

	private static final long serialVersionUID = 1267293620078244366L;

	@Inject
	Logger log;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	protected RegistryServiceI registryService;

	public DocumentosRegistradosManager getDocumentosAdjuntos(Long idSolicitud) throws SedeException {
		return getDocumentosAdjuntos(solicitudRepository.findById(idSolicitud));
	}

	public DocumentosRegistradosManager getDocumentosAdjuntos(Solicitud solicitud) throws SedeException {
		List<DocumentoRegistrado> documentosAdjuntos = new ArrayList<DocumentoRegistrado>();
		try {

			IdentificadorRegistroDTO identificadorRegistro = solicitud.getIdentificadorRegistro();
			InputRegisterI inputRegister = registryService.getInputRegister(
					identificadorRegistro.getBookIdentification(), identificadorRegistro.getRegisterIdentification());

			ArrayOfWSDocument documents = inputRegister.getDocuments();

			log.debug("obteniendo los documentos del registro ...");

			int documentIndex;
			int pageIndex;
			int dsize = documents.getWSDocument().size() ;
			for (documentIndex = 0; documentIndex < dsize; documentIndex++) {
				WSDocument document = documents.getWSDocument().get(documentIndex);
				// int psize = document.getPages().getWSPage().size();

				pageIndex = 0;
				Iterator<WSPage> itPages = document.getPages().getWSPage().iterator();
				while (itPages.hasNext()) {
					WSPage wsPage = itPages.next();
					pageIndex++;

					DocumentoRegistrado documentoAdjunto = new DocumentoRegistrado(document.getName(), wsPage.getName(),
							registryService.getAttachedDocument(identificadorRegistro.getBookIdentification(),
									identificadorRegistro.getRegisterIdentification(), documentIndex+1, pageIndex));
					log.info("documento obtenido del registro: " + documentoAdjunto.toString());
					documentosAdjuntos.add(documentoAdjunto);
				}
			}

		} catch (Exception e) {
			log.error(String.format("error al obtener los documentos del registro de entrada: %s",
					Utils.getExceptionMessage(e)));
			throw new SedeException(e);
		}
		return new DocumentosRegistradosManager(documentosAdjuntos);
	}

}
