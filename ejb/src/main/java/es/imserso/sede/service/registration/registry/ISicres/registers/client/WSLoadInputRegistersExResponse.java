
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSLoadInputRegistersExResult" type="{http://www.invesicres.org}WSInputRegistersResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsLoadInputRegistersExResult"
})
@XmlRootElement(name = "WSLoadInputRegistersExResponse")
public class WSLoadInputRegistersExResponse {

    @XmlElement(name = "WSLoadInputRegistersExResult")
    protected WSInputRegistersResponse wsLoadInputRegistersExResult;

    /**
     * Obtiene el valor de la propiedad wsLoadInputRegistersExResult.
     * 
     * @return
     *     possible object is
     *     {@link WSInputRegistersResponse }
     *     
     */
    public WSInputRegistersResponse getWSLoadInputRegistersExResult() {
        return wsLoadInputRegistersExResult;
    }

    /**
     * Define el valor de la propiedad wsLoadInputRegistersExResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSInputRegistersResponse }
     *     
     */
    public void setWSLoadInputRegistersExResult(WSInputRegistersResponse value) {
        this.wsLoadInputRegistersExResult = value;
    }

}
