
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfWSInputRegister complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWSInputRegister">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSInputRegister" type="{http://www.invesicres.org}WSInputRegister" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWSInputRegister", propOrder = {
    "wsInputRegister"
})
public class ArrayOfWSInputRegister {

    @XmlElement(name = "WSInputRegister", nillable = true)
    protected List<WSInputRegister> wsInputRegister;

    /**
     * Gets the value of the wsInputRegister property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsInputRegister property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSInputRegister().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSInputRegister }
     * 
     * 
     */
    public List<WSInputRegister> getWSInputRegister() {
        if (wsInputRegister == null) {
            wsInputRegister = new ArrayList<WSInputRegister>();
        }
        return this.wsInputRegister;
    }

}
