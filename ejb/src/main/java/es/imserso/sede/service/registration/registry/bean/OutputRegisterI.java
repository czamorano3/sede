package es.imserso.sede.service.registration.registry.bean;

import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSAddField;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSPerson;

public interface OutputRegisterI extends RegisterI {

	String getSender();

	void setSender(String sender);

	String getSenderName();

	void setSenderName(String senderName);

	String getDestination();

	void setDestination(String destination);

	String getDestinationName();

	void setDestinationName(String destinationName);

	String getTransportType();

	void setTransportType(String transportType);

	String getTransportNumber();

	void setTransportNumber(String transportNumber);

	String getMatterType();

	void setMatterType(String matterType);

	String getMatterTypeName();

	void setMatterTypeName(String matterTypeName);

	String getMatter();

	void setMatter(String matter);

	ArrayOfWSPerson getPersons();

	void setPersons(ArrayOfWSPerson persons);

	ArrayOfWSDocument getDocuments();

	void setDocuments(ArrayOfWSDocument documents);

	ArrayOfWSAddField getAddFields();

	void setAddFields(ArrayOfWSAddField addFields);

}