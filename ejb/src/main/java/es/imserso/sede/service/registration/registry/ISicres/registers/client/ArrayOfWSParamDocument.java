
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfWSParamDocument complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWSParamDocument">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSParamDocument" type="{http://www.invesicres.org}WSParamDocument" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWSParamDocument", propOrder = {
    "wsParamDocument"
})
public class ArrayOfWSParamDocument {

    @XmlElement(name = "WSParamDocument", nillable = true)
    protected List<WSParamDocument> wsParamDocument;

    /**
     * Gets the value of the wsParamDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsParamDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSParamDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSParamDocument }
     * 
     * 
     */
    public List<WSParamDocument> getWSParamDocument() {
        if (wsParamDocument == null) {
            wsParamDocument = new ArrayList<WSParamDocument>();
        }
        return this.wsParamDocument;
    }

}
