
package es.imserso.sede.service.registration.registry.ISicres.books.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WSGetBookSchemaResult" type="{http://www.invesicres.org}ArrayOfWSField" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsGetBookSchemaResult"
})
@XmlRootElement(name = "WSGetBookSchemaResponse")
public class WSGetBookSchemaResponse {

    @XmlElement(name = "WSGetBookSchemaResult")
    protected ArrayOfWSField wsGetBookSchemaResult;

    /**
     * Obtiene el valor de la propiedad wsGetBookSchemaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSField }
     *     
     */
    public ArrayOfWSField getWSGetBookSchemaResult() {
        return wsGetBookSchemaResult;
    }

    /**
     * Define el valor de la propiedad wsGetBookSchemaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSField }
     *     
     */
    public void setWSGetBookSchemaResult(ArrayOfWSField value) {
        this.wsGetBookSchemaResult = value;
    }

}
