package es.imserso.sede.process.scheduled.sync;

import java.io.Serializable;

/**
 * Evento para informar del estado del proceso de sincronización con una
 * aplicación gestora.
 * 
 * @author 11825775
 *
 */
public class SyncEvent implements Serializable {

	private static final long serialVersionUID = 8481820424470774048L;

	private SyncEventPhase phase;

	public SyncEvent(SyncEventPhase _phase) {
		this.phase = _phase;
	}
	
	public SyncEventPhase getPhase() {
		return phase;
	}

	public void setPhase(SyncEventPhase phase) {
		this.phase = phase;
	}
	
	public enum SyncEventPhase {
		SYNC_START, SYNC_END, SYNC_EXPEDIENTE_START, SYNC_EXPEDIENTE_END, SYNC_EXPEDIENTE_SUCCESSFUL, SYNC_EXPEDIENTE_UNSUCCESSFUL
	}


}
