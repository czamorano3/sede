package es.imserso.sede.web.service.message.manager;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.message.event.EventMessageSource;
import es.imserso.sede.service.registration.event.RegistrationPhase;
import es.imserso.sede.service.registration.event.RegistrationPhaseEvent;

/**
 * Gestiona los mensajes de la sesión
 * 
 * @author 11825775
 * 
 */
@SessionScoped
public class RegistrationMessageManager extends AbstractMessageManager implements Serializable {

	private static final long serialVersionUID = -3268678114987160111L;

	@Inject
	private Logger log;

	/**
	 * constructor
	 */
	public RegistrationMessageManager() {
		super();
	}

	public void onMessageEvent(@Observes EventMessage message) {
		addMessage(message);
	}

	/**
	 * Captura de evento lanzado cuando se está ejecutando una de las fases del
	 * registro de una solicitud
	 * 
	 * @param phase
	 *            fase que se está ejecutando
	 */
	public void onRegistrationPhaseEvent(@Observes RegistrationPhaseEvent registrationPhaseEvent) {

		if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_INIT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.INIT)) {
			clearMessageList();
			addMessage(new EventMessage(EventMessageSource.ALTA, EventMessageLevel.MESSAGE_LEVEL_INFO,
					"se va a registrar una solicitud"));
		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_INIT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.VIEW_LOAD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.VIEW_SAVE)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_VALIDATION)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.VALIDATION)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_VALIDATION)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_PERSIST_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.PERSIST_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_PERSIST_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_PERSIST_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.PERSIST_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_PERSIST_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.GENERATE_RECEIPT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.SIGN_RECEIPT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase()
				.equals(RegistrationPhase.BEFORE_PERSIST_RECEIPT_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase()
				.equals(RegistrationPhase.PERSIST_RECEIPT_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase()
				.equals(RegistrationPhase.AFTER_PERSIST_RECEIPT_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_UPDATE_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.UPDATE_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_UPDATE_ON_BBDD)) {

		}

	}

	public void afterTransactionSuccess(@Observes(during = TransactionPhase.AFTER_SUCCESS) RegistrationPhase event) {
		log.info("afterTransactionSuccess event detected: " + event.name());
	}

	public void afterTransactionFailure(@Observes(during = TransactionPhase.AFTER_FAILURE) RegistrationPhase event) {
		log.info("afterTransactionFailure event detected: " + event.name());
	}

	@Produces
	@Named
	@RequestScoped
	public List<EventMessage> getProcessMessages() {
		return getMessagesFromDefaultLevel();
	}

}
