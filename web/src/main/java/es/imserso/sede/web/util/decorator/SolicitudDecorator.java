package es.imserso.sede.web.util.decorator;

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.interceptor.Interceptor;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.web.util.route.ParamValues;

@Decorator
@Priority(Interceptor.Priority.APPLICATION)
public abstract class SolicitudDecorator implements SolicitudDTOI {

	private Logger log = Logger.getLogger(SolicitudDecorator.class.getName());

	@Inject
	@Delegate
	@Any
	SolicitudDTOI solicitud;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	boolean decorate;

	@PostConstruct
	public void onCreate() {
		if (decorate = (paramValues.getSia() != null)) {
			log.info("???????????????????????????????????????????????????????????????????????????????????");
			log.info("decoramos el DTO de la solicitud");
		} else {
			log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaInteresadaDTOI#getCodigoSIA()
	 */
	@Override
	public String getCodigoSIA() {
		String value = null;
		if (decorate) {
			value = paramValues.getSia();
		} else {
			value = solicitud.getCodigoSIA();
		}
		return value;
	}

}
