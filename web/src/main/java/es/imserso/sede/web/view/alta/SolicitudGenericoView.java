package es.imserso.sede.web.view.alta;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;
import es.imserso.sede.data.dto.impl.GenericoDTO;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.impl.generico.GenericoSolicitudRegistrationQ;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de genérico
 * 
 * @author 11825775
 *
 */
@Named(value = "genericoView")
@ViewScoped
@Secure
public class SolicitudGenericoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 2248273270416839828L;

	@Inject
	Logger log;

	@Inject
	@GenericoSolicitudRegistrationQ
	private SolicitudRegistrationI genericoSolicitudRegistration;

	@Inject
	private GenericoDTO dto;

	private StreamedContent formularioPdf;

	@PostConstruct
	public void onCreate() {
		try {
			super.init();

			// FIXME mirar de dónde se saca el fichero
			// this.formularioPdf = new DefaultStreamedContent(
			// new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitud().getFichero()),
			// Global.CONTENT_TYPE_APPLICATION_PDF,
			// TipoDocumentoRegistrado.SOLICITUD_ADJUNTA.getNombreDocumentoPorDefecto() +
			// ".pdf");
		} catch (Exception e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.GENERICO,
					"No se pudo crear la instacia del DTO", Utils.getExceptionMessage(e));
		}
	}

	/**
	 * Registra la solicitud en la Sede Electrónica
	 */
	public void save() {
		save(genericoSolicitudRegistration, dto);
	}

	/**
	 * @param e
	 *            evento que contiene el fichero subido
	 */
	public void solicitudUploadListener(FileUploadEvent e) {
		log.info("Añadiendo solicitud firmada por el usuario ...");

		this.uploadedFile = e.getFile();

		if (this.uploadedFile == null) {
			log.warn("No hay fichero para adjuntar");
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("form:messages");
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.GENERICO, "Ficheros Adjuntos",
					"No se ha recibido ningún fichero para adjuntar");
			return;
		}

		log.infov("Uploaded File Name Is :: {0} :: Uploaded File Size :: {1} ::", uploadedFile.getFileName(),
				uploadedFile.getSize());

		// asignamos los ficheros adjuntos al DTO
		DocumentoRegistrado attachedFile = new DocumentoRegistrado(TipoDocumentoRegistrado.PERSONAL.toString(),
				this.uploadedFile.getFileName(), this.uploadedFile.getContents());
		getDto().setSolicitudPdf(attachedFile);
		log.info("documento solicitud pdf asignado!");
	}

	@Override
	public Tramite getTramite() {
		if (tramite == null) {
			tramite = tramiteRepository.findBySIA(getDto().getCodigoSIA());
			if (tramite == null)
				throw new SedeRuntimeException("No se ha encontrado trámite con el código SIA:" + dto.getCodigoSIA());
		}
		return tramite;
	}

	public StreamedContent getFormularioPdf() {
		return formularioPdf;
	}

	public void setFormularioPdf(StreamedContent formularioPdf) {
		this.formularioPdf = formularioPdf;
	}

	public GenericoDTO getDto() {
		return (GenericoDTO) dto;
	}

}
