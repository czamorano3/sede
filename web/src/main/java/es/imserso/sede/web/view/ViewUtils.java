package es.imserso.sede.web.view;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import org.jboss.logging.Logger;

import es.imserso.sede.model.TipoTramite;

public class ViewUtils {

	private static final String ERROR_MESSAGE_HEADER_FORMAT = "[%s] - %s";
	private static final String ERROR_MESSAGE_LOG_FORMAT = ERROR_MESSAGE_HEADER_FORMAT + ": %s";

	public static final Severity VALIDATION_ERROR_SEVERITY = FacesMessage.SEVERITY_WARN;
	public static final Severity VALIDATION_ERROR_ERROR = FacesMessage.SEVERITY_ERROR;
	public static final Severity SYSTEM_ERROR_SEVERITY = FacesMessage.SEVERITY_FATAL;

	/**
	 * Muestra al usuario mensajes de error
	 * 
	 * @param log
	 *            Logger del componente donde se ha producido el error
	 * @param ctx
	 *            Contexto de Faces
	 * @param tipoTramite
	 *            Trámite sobre el que se ha realizado la validación
	 * @param errorHeader
	 *            Título del mensaje de error
	 * @param errorBody
	 *            Texto del mensaje de error
	 */
	public static void manageViewError(Logger log, FacesContext ctx, TipoTramite tipoTramite, String errorHeader,
			String errorBody) {
		manageError(log, ctx, tipoTramite, errorHeader, errorBody, FacesMessage.SEVERITY_ERROR);
	}

	/**
	 * Muestra al usuario mensajes de errores de validación de campos de un
	 * formulario
	 * 
	 * @param log
	 *            Logger del componente donde se ha producido el error
	 * @param ctx
	 *            Contexto de Faces
	 * @param tipoTramite
	 *            Trámite sobre el que se ha realizado la validación
	 * @param errorHeader
	 *            Título del mensaje de error
	 * @param errorBody
	 *            Texto del mensaje de error
	 */
	public static void manageValidationError(Logger log, FacesContext ctx, TipoTramite tipoTramite, String errorHeader,
			String errorBody) {
		manageError(log, ctx, tipoTramite, errorHeader, errorBody, FacesMessage.SEVERITY_WARN);

	}

	/**
	 * Muestra al usuario mensajes de errores de validación de campos de un
	 * formulario
	 * 
	 * @param log
	 *            Logger del componente donde se ha producido el error
	 * @param ctx
	 *            Contexto de Faces
	 * @param tipoTramite
	 *            Trámite sobre el que se ha realizado la validación
	 * @param errorHeader
	 *            Título del mensaje de error
	 * @param errorBody
	 *            Texto del mensaje de error
	 * @param severity
	 *            Nivel del error
	 */
	private static void manageError(Logger log, FacesContext ctx, TipoTramite tipoTramite, String errorHeader,
			String errorBody, Severity severity) {
		log.error(String.format(ERROR_MESSAGE_LOG_FORMAT, tipoTramite.getNombre(), errorHeader, errorBody));
		ctx.addMessage(null, new FacesMessage(severity,
				String.format(ERROR_MESSAGE_HEADER_FORMAT, tipoTramite.getNombre(), errorHeader), errorBody));
	}

}
