package es.imserso.sede.web.view.alta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

import es.imserso.hermes.session.webservice.dto.SimpleDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.TermalismoRepository;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.impl.termalismo.DatoEconomicoTermalismoDTO;
import es.imserso.sede.data.dto.impl.termalismo.DatoEconomicoTermalismoDTOI;
import es.imserso.sede.data.dto.impl.termalismo.TermalismoDTO;
import es.imserso.sede.data.dto.rest.termalismo.EstadoCivilVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.SexoVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoFamiliaNumerosaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoPensionVOWS;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.GlobalTerma;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.impl.termalismo.TermalismoSolicitudRegistrationQ;
import es.imserso.sede.web.util.route.ParamValues.Params;
import es.imserso.sede.web.view.ViewUtils;



/**
 * Bean de respaldo para el dto de propósito general.
 * 
 * @author 11825775
 *
 */
@Named(value = "termalismoView")
@ViewScoped
@Secure
public class SolicitudTermalismoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	Logger log;

	@Inject
	@TermalismoSolicitudRegistrationQ
	private SolicitudRegistrationI termalismoSolicitudRegistration;

	private String quienSolicita = "";

	@Inject
	private TermalismoRepository termalismoRepository;

	private List<SimpleDTOI> listaSexos;
	private List<SimpleDTOI> estadosCiviles;
	private List<SimpleDTOI> listaProvincias;
	private List<SimpleDTOI> listaComunidadesAutonomas;
	private List<SimpleDTOI> listaTiposTurno;
	private List<SimpleDTOI> listaTiposPensiones;	
	private List<SimpleDTOI> listaFamiliaNumerosa;
	private SexoVOWS sexoSol;
	private SexoVOWS sexoCon;
	private EstadoCivilVOWS estadoCivilSol;
	private ProvinciaVOWS provinciaDomicilio;
	private ProvinciaVOWS provinciaNotificacion;
	
	private SimpleDTOI tipoTurno;
	

	private TipoPensionVOWS tipoPensionSol1;
	private TipoPensionVOWS tipoPensionSol2;
	private TipoPensionVOWS tipoPensionCon1;
	private TipoPensionVOWS tipoPensionCon2;

	private TipoFamiliaNumerosaVOWS tipoFamiliaNum;

	// Tratamientos: reumatológico, respiratorio, digestimo...
	// variables asociadas: solicitante -->tratSol1, tratSol2.... conyuge
	// -->tratCon1, tratCon2....
	private List<String> selectedTratamientosSolicitante = new ArrayList<String>();
	private List<String> selectedTratamientosConyuge = new ArrayList<String>();

	// Articulaciones afectadas: cadera, columna, hombro...
	// variables asociadas: solicitante -->artiSol1, artiSol2... conyuge
	// -->artiCon1, artiCon2...
	private List<String> selectedArticAfectadasSolicitante;
	private List<String> selectedArticAfectadasConyuge;
	// Tiene o padece: difcultades para moverse, dolor....
	// variables asociadas: defoSol1, defoSol2...
	private String[] selectedPadece;

	// Padece enfermades de: vias resp altas, bajas
	// variables asociadas solicitante --> vresSolA y vresSolB conyuge-->vresConA y
	// vresConB
	private List<String> selectedViasSolicitante;
	private List<String> selectedViasConyuge;

	// por los problemas tiene que: ingresos hospital medicación..
	// variables asociadas solicitante--> impoSol1, impoSol2... conyuge--> impoCon1,
	// impoCon2...
	private List<String> selectedConsecuenciasSolicitante;
	private List<String> selectedConsecuenciasConyuge;

	// tengo o padezco: dificultades para moverme, dolor, deformidad..
	private List<String> selectedTengoPadezcoSolicitante;
	private List<String> selectedTengoPadezcoConyuge;
	
	private Date conyugeFechaNacimiento;
	private String duracionTurno;
	private Integer[] selectedDays = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
			23, 24, 25, 26, 27, 28, 29, 30, 31 };
	private Integer[] selectedMonths = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	
	
	private SimpleDTOI turnoDefecto;
	
	
	
	/**
	 * Prestación económica que se declara
	 */
	private DatoEconomicoTermalismoDTOI datoEconomico;
	
	private SimpleDTOI balneario;
	private Long ccaa;
	
	private enum PersonaDeclarante {
		Solicitante, Conyuge
	}
	
	/**
	 * A la hora de declarar una prestación, indica si es del solicitante o del
	 * cónyuge
	 */
	private String selectedPersonaDeclarante;
	private String declaranteSolicitante;
	private String declaranteConyuge;
	


	@Inject
	TermalismoDTO dto;

	@PostConstruct
	public void onCreate() {
		super.init();
		
		selectedTratamientosSolicitante.add(GlobalTerma.REUMATOLOGICO);
		selectedTratamientosConyuge.add(GlobalTerma.REUMATOLOGICO);
		try {
			estadosCiviles = termalismoRepository.getEstadosCiviles();
			listaSexos = termalismoRepository.getSexos();
			listaProvincias = termalismoRepository.getProvincias();
			listaTiposTurno = termalismoRepository.getTiposTurno();
			listaComunidadesAutonomas = termalismoRepository.getComunidadesAutonomas();
			listaTiposPensiones = termalismoRepository.getTiposPensiones();
			listaFamiliaNumerosa = termalismoRepository.getCategoriasFamiliaNumerosa();
			datoEconomico = new DatoEconomicoTermalismoDTO();
			dto.setListaBalnearios(new ArrayList<SimpleDTOI>());
			dto.setListaTurnos(new ArrayList<SimpleDTOI>());
			setTurnoDefecto(turnoSinPreferencia());
			tramite = tramiteRepository.findBySIA(dto.getCodigoSIA());

			declaranteSolicitante = PersonaDeclarante.Solicitante.toString();
			declaranteConyuge = PersonaDeclarante.Conyuge.toString();
		} catch (Exception e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TERMALISMO,
					"No se pudo crear la instacia del DTO", Utils.getExceptionMessage(e));
		}

	}

	private SimpleDTOI turnoSinPreferencia() {
		SimpleDTOI turnoDef=null;
		for (SimpleDTOI turno : listaTiposTurno){
			if (turno.getCodigo().equals(GlobalTerma.TURNO_CUALQUIERA)){
				turnoDef = turno;
				break;
			}
		}
		return turnoDef;
	}

	@PreDestroy
	public void onTerminate() {
		// aseguramos que se persisten los apuntes de la operación realizada
		if (!regTramiteBean.persisted()) {
			try {
				regTramiteBean.endUnsuccessfully("la solicitud no se ha registrado!");
			} catch (Exception e) {
				log.warnv("error al intentar, in extremis, persistir los apuntes del registro de la solicitud: %s",
						Utils.getExceptionMessage(e));
			}
		}
	}

	/**
	 * Persiste la solicitud en el sistema
	 * <p>
	 * Lanza la ejecución de las fases de registro de la solicitud
	 */
	public void save() {
		try {
			datosForm();
			getDto().setNreg(Integer.valueOf(termalismoRepository.getNuevoNumeroExpediente()));
			save(termalismoSolicitudRegistration, dto);
		} catch (NumberFormatException | SedeException e) {
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(),
					TipoTramite.getTipoTramite(paramValues.getParamValue(Params.sia).getValue()),
					"No se pudo dar de alta la solicitud", Utils.getExceptionMessage(e));			
			RequestContext.getCurrentInstance().scrollTo("messages");
		}
	}




	/**
	 * Cancela el alta de la solicitud
	 */
	public String cancel() {
		try {
			log.info("operación cancelada por el usuario!");
			regTramiteBean.endUnsuccessfully("operación cancelada por el usuario!");
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

		} catch (Exception e) {
			log.errorv("Error al cancelar el alta: %s", Utils.getExceptionMessage(e));
		}
		return "backToUCM";
	}

	
	private void plazasSolicitadas() throws ValidationException {
		getDto().setPlazSol("");
		getDto().setPlazCony("");
		getDto().setPlazAmbos("");
		if (quienSolicita == null || Utils.isEmpty(quienSolicita)) {
			throw new ValidationException("Debe seleccionar las plazas solicitadas");
		}
		switch (quienSolicita) {
		case GlobalTerma.PLAZA_SOLICITANTE:
			getDto().setPlazSol(GlobalTerma.PLAZA_SOLICITANTE);
			break;
		case GlobalTerma.PLAZA_CONYUGE:
			getDto().setPlazCony(GlobalTerma.PLAZA_CONYUGE);
			break;
		case GlobalTerma.PLAZA_AMBOS:
			getDto().setPlazAmbos(GlobalTerma.PLAZA_AMBOS);
			break;
		}

	}

	private void turnoSolicitado() throws ValidationException {
		getDto().setTdt1("");
		getDto().setTdt2("");
		getDto().setTdt3("");
		if (duracionTurno == null) {
			throw new ValidationException("Debe seleccionar un tipo de turno");
		}
		switch (duracionTurno) {
		case GlobalTerma.DOCE_NOCHES:
			getDto().setTdt1(GlobalTerma.DOCE_NOCHES);
			break;
		case GlobalTerma.DIEZ_NOCHES:
			getDto().setTdt2(GlobalTerma.DIEZ_NOCHES);
			break;
		case GlobalTerma.SIN_PREFERENCIA:
			getDto().setTdt3(GlobalTerma.SIN_PREFERENCIA);
			break;
		}
	}
	
	protected void datosForm() throws ValidationException {
		if (Utils.isEmptyTrimmed(quienSolicita)) {
			throw new ValidationException("Debe seleccionar quién solicita la plaza");
		}
		plazasSolicitadas();
		turnoSolicitado();
		// si ha seleccionado solicitante o ambos
		if (!Utils.isEmptyTrimmed(getDto().getPlazSol()) || !Utils.isEmptyTrimmed(getDto().getPlazAmbos())) {
			tratamientoSolicitado(selectedTratamientosSolicitante, GlobalTerma.SOLICITANTE);
			articulacionesAfectadas(selectedArticAfectadasSolicitante, GlobalTerma.SOLICITANTE);
			viasRespiratorias(selectedViasSolicitante, GlobalTerma.SOLICITANTE, selectedTratamientosSolicitante);
			ingresosUltimoAnio(selectedConsecuenciasSolicitante, GlobalTerma.SOLICITANTE, selectedTratamientosConyuge);
			tengoPadezco(selectedTengoPadezcoSolicitante, GlobalTerma.SOLICITANTE);
		}
		// si ha seleccionado conyuge o ambos
		if (!Utils.isEmptyTrimmed(getDto().getPlazCony()) || !Utils.isEmptyTrimmed(getDto().getPlazAmbos())) {
			tratamientoSolicitado(selectedTratamientosConyuge, GlobalTerma.CONYUGE);
			articulacionesAfectadas(selectedArticAfectadasConyuge, GlobalTerma.CONYUGE);
			viasRespiratorias(selectedViasConyuge, GlobalTerma.CONYUGE, selectedTratamientosConyuge);
			ingresosUltimoAnio(selectedConsecuenciasConyuge, GlobalTerma.CONYUGE, selectedTratamientosConyuge);
			tengoPadezco(selectedTengoPadezcoConyuge, GlobalTerma.CONYUGE);
		}
	}

	// reumatológico, respiratorio...
	private void tratamientoSolicitado(List<String> tratamientos, String persona) throws ValidationException {
		if (tratamientos == null || tratamientos.size() == 0) {
			tratamientos.add(GlobalTerma.REUMATOLOGICO);
		}
		if (!tratamientos.contains(GlobalTerma.REUMATOLOGICO)) {
			tratamientos.add(GlobalTerma.REUMATOLOGICO);
		}
		if (tratamientos.size() > 2) {
			throw new ValidationException("Puede seleccionar hasta un máximo de 2 tratamientos");
		}

		for (String trat : tratamientos) {
			if (trat != null) {
				switch (trat) {
				case GlobalTerma.REUMATOLOGICO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setTratSol1(GlobalTerma.REUMATOLOGICO);
					} else {
						getDto().setTratCon1(GlobalTerma.REUMATOLOGICO);
					}
					break;
				case GlobalTerma.RESPIRATORIO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setTratSol2(GlobalTerma.RESPIRATORIO);
					} else {
						getDto().setTratCon2(GlobalTerma.RESPIRATORIO);
					}
					break;
				case GlobalTerma.DIGESTIVO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setTratSol3(GlobalTerma.DIGESTIVO);
					} else {
						getDto().setTratCon3(GlobalTerma.DIGESTIVO);
					}
					break;
				case GlobalTerma.RENAL:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setTratSol4(GlobalTerma.RENAL);
					} else {
						getDto().setTratCon4(GlobalTerma.RENAL);
					}
					break;
				case GlobalTerma.DERMATOLOGICO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setTratSol5(GlobalTerma.DERMATOLOGICO);
					} else {
						getDto().setTratCon5(GlobalTerma.DERMATOLOGICO);
					}
					break;
				case GlobalTerma.NEUROPSIQUICO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setTratSol6(GlobalTerma.NEUROPSIQUICO);
					} else {
						getDto().setTratCon6(GlobalTerma.NEUROPSIQUICO);
					}
					break;
				}
			}

		}
	}

	// articulaciones afectadas
	private void articulacionesAfectadas(List<String> articulacionesAfectadas, String persona)
			throws ValidationException {
		for (String trat : articulacionesAfectadas) {
			if (trat != null) {
				switch (trat) {
				case GlobalTerma.CADERA_RODILLA:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setArtiSol1(GlobalTerma.CADERA_RODILLA);
					} else {
						getDto().setArtiCon1(GlobalTerma.CADERA_RODILLA);
					}
					break;
				case GlobalTerma.COLUMNA:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setArtiSol2(GlobalTerma.COLUMNA);
					} else {
						getDto().setArtiCon2(GlobalTerma.COLUMNA);
					}
					break;
				case GlobalTerma.HOMBRO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setArtiSol3(GlobalTerma.HOMBRO);
					} else {
						getDto().setArtiCon3(GlobalTerma.HOMBRO);
					}
					break;
				case GlobalTerma.MUNECA_MANO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setArtiSol4(GlobalTerma.MUNECA_MANO);
					} else {
						getDto().setArtiCon4(GlobalTerma.MUNECA_MANO);
					}
					break;
				case GlobalTerma.CODO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setArtiSol5(GlobalTerma.CODO);
					} else {
						getDto().setArtiCon5(GlobalTerma.CODO);
					}
					break;
				case GlobalTerma.TOBILLO_PIE:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setArtiSol6(GlobalTerma.TOBILLO_PIE);
					} else {
						getDto().setArtiCon6(GlobalTerma.TOBILLO_PIE);
					}
					break;
				}
			}

		}
	}

	// articulaciones afectadas (SOLO SI HA SELECCIONADO TRATAMIENTO RESPIRATOTO)
	private void viasRespiratorias(List<String> viasRespiratorias, String persona,
			List<String> tratamientosSeleccionados) throws ValidationException {
		if (tratamientosSeleccionados.contains(GlobalTerma.RESPIRATORIO)) {
			for (String trat : viasRespiratorias) {
				if (trat != null) {
					switch (trat) {
					case GlobalTerma.ALTAS:
						if (persona.equals(GlobalTerma.SOLICITANTE)) {
							getDto().setVresSolA(GlobalTerma.ALTAS);
						} else {
							getDto().setVresConA(GlobalTerma.ALTAS);
						}
						break;
					case GlobalTerma.BAJAS:
						if (persona.equals(GlobalTerma.SOLICITANTE)) {
							getDto().setVresSolB(GlobalTerma.BAJAS);
						} else {
							getDto().setVresConB(GlobalTerma.BAJAS);
						}
						break;
					}
				}
			}
		}
	}

	// tengo o padezco
	private void tengoPadezco(List<String> padezco, String persona) throws ValidationException {
		for (String trat : padezco) {
			if (trat != null) {
				switch (trat) {
				case GlobalTerma.DIFICULTAD_MOVI:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setDefoSol1(GlobalTerma.DIFICULTAD_MOVI);
					} else {
						getDto().setDefoCon1(GlobalTerma.DIFICULTAD_MOVI);
					}
					break;
				case GlobalTerma.DOLOR:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setDefoSol2(GlobalTerma.DOLOR);
					} else {
						getDto().setDefoCon2(GlobalTerma.DOLOR);
					}
					break;
				case GlobalTerma.DEFORMIDAD:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setDefoSol3(GlobalTerma.DEFORMIDAD);
					} else {
						getDto().setDefoCon3(GlobalTerma.DEFORMIDAD);
					}
					break;
				case GlobalTerma.RIGIDEZ:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						getDto().setDefoSol4(GlobalTerma.RIGIDEZ);
					} else {
						getDto().setDefoCon4(GlobalTerma.RIGIDEZ);
					}
					break;
				}
			}

		}
	}

	// articulaciones afectadas (SOLO SI HA SELECCIONADO TRATAMIENTO RESPIRATORIO)
	private void ingresosUltimoAnio(List<String> consecuencias, String persona, List<String> tratamientosSeleccionados)
			throws ValidationException {
		if (tratamientosSeleccionados.contains(GlobalTerma.RESPIRATORIO)) {
			for (String trat : consecuencias) {
				if (trat != null) {
					switch (trat) {
					case GlobalTerma.INGRESO_HOSPITAL:
						if (persona.equals(GlobalTerma.SOLICITANTE)) {
							getDto().setImpoSol1(GlobalTerma.INGRESO_HOSPITAL);
						} else {
							getDto().setImpoCon1(GlobalTerma.INGRESO_HOSPITAL);
						}
						break;
					case GlobalTerma.TOMAR_MAS_DE_DOS_MEDICAMENTOS_DIA:
						if (persona.equals(GlobalTerma.SOLICITANTE)) {
							getDto().setImpoSol2(GlobalTerma.TOMAR_MAS_DE_DOS_MEDICAMENTOS_DIA);
						} else {
							getDto().setImpoCon2(GlobalTerma.TOMAR_MAS_DE_DOS_MEDICAMENTOS_DIA);
						}
						break;
					case GlobalTerma.MUCHOS_SINTOMAS:
						if (persona.equals(GlobalTerma.SOLICITANTE)) {
							getDto().setImpoSol3(GlobalTerma.MUCHOS_SINTOMAS);
						} else {
							getDto().setImpoCon3(GlobalTerma.MUCHOS_SINTOMAS);
						}
						break;
					case GlobalTerma.OXIGENO_DIARIO:
						if (persona.equals(GlobalTerma.SOLICITANTE)) {
							getDto().setImpoSol4(GlobalTerma.OXIGENO_DIARIO);
						} else {
							getDto().setImpoCon4(GlobalTerma.OXIGENO_DIARIO);
						}
						break;
					}
				}
			}
		}
	}

	public void controlTratamientoSolicitante() {
		// si no ha seleccionado tratamiento respiratorio, borramos datos de
		// padecimientos y/o ingresos que sólo
		// pueden tener valor si se ha seleccionado respiratorio
		if (!selectedTratamientosSolicitante.contains(GlobalTerma.RESPIRATORIO)) {
			selectedViasSolicitante = new ArrayList<>();
			getDto().setNred1(null);
			selectedConsecuenciasSolicitante = new ArrayList<>();
		}
		// si no ha seleccionado renal, dermatológico o neurosíquico NO puede tener
		// valor cdef2
		boolean valor = false;
		for (String trat : selectedTratamientosSolicitante) {
			if (trat.equals(GlobalTerma.RENAL) || trat.equals(GlobalTerma.DERMATOLOGICO)
					|| trat.equals(GlobalTerma.NEUROPSIQUICO)) {
				valor = true;
				break;
			}
		}
		if (!valor) {
			getDto().setCdef1(null);
		}
	}

	public void controlTratamientoConyuge() {
		// si no ha seleccionado tratamiento respiratorio, borramos datos de
		// padecimientos y/o ingresos que sólo
		// pueden tener valor si se ha seleccionado respiratorio
		if (!selectedTratamientosConyuge.contains(GlobalTerma.RESPIRATORIO)) {
			selectedViasConyuge = new ArrayList<>();
			getDto().setNred2(null);
			selectedConsecuenciasConyuge = new ArrayList<>();
		}
		// si no ha seleccionado renal, dermatológico o neurosíquico NO puede tener
		// valor cdef2
		if (!selectedTratamientosConyuge.contains(GlobalTerma.RENAL)
				&& !selectedTratamientosConyuge.contains(GlobalTerma.DERMATOLOGICO)
				&& !selectedTratamientosConyuge.contains(GlobalTerma.NEUROPSIQUICO)) {
			getDto().setCdef2(null);
		}
	}

	public List<SimpleDTOI> getEstadosCiviles() {
		return estadosCiviles;
	}

	public List<SimpleDTOI> listaComunidadesAutonomas() {
		return listaComunidadesAutonomas;
	}

	public List<SimpleDTOI> getBalneariosPorCCAA(Long idComunidad) {
		List<SimpleDTOI> listaBalnearios = null;
		if (idComunidad != null) {
			try {
				listaBalnearios = termalismoRepository.getBalneariosPorCCAA(idComunidad);
			} catch (SedeException e) {
				//FIXME 
				log.error("No se han obtenido balnearios");;
			}
		}
		return listaBalnearios;
	}
	
	public void addPensionDeclarante() {		
		if (getSelectedPersonaDeclarante().equals(PersonaDeclarante.Solicitante.toString())) {
			addPension(dto.getDatosEconomicos().getDatosEconomicosSolicitante());
		} else {
			addPension(dto.getDatosEconomicos().getDatosEconomicosConyuge());
		}
	}

	private void addPension(List<DatoEconomicoDTOI> datosEconomicosPersona) {
		if (!datoEconomico.isValid()) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Datos insuficientes",
					datoEconomico.getValidationFailedMessage());
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}
		if (datosEconomicosPersona.contains(datoEconomico)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Duplicada",
					"Esta prestación ya se ha añadido");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}

		datosEconomicosPersona.add(datoEconomico);
		datoEconomico = new DatoEconomicoTermalismoDTO();
	}
	
	
	public void addBalneario() {
		if (balneario==null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Datos insuficientes","Introduzca balneario");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}		
		if (dto.getListaBalnearios().contains(balneario)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Duplicada",
				"Este balneario ya se ha añadido");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}	
		dto.getListaBalnearios().add(balneario);	
		balneario = new SimpleDTO();
	}
	
	public void addTurno() {
		if (tipoTurno==null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Datos insuficientes","Introduzca turno");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}		
		if (dto.getListaTurnos().contains(tipoTurno)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Duplicada",
				"Este turno ya se ha añadido");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}	
		dto.getListaTurnos().add(tipoTurno);	
		tipoTurno = new SimpleDTO();
	}

	public List<SimpleDTOI> getListaTiposPensiones() {
		return listaTiposPensiones;
	}
	public void setListaTiposPensiones(List<SimpleDTOI> listaTiposPensiones) {
		this.listaTiposPensiones = listaTiposPensiones;
	}
	public List<SimpleDTOI> getListaFamiliaNumerosa() {
		return listaFamiliaNumerosa;
	}

	public void setListaFamiliaNumerosa(List<SimpleDTOI> listaFamiliaNumerosa) {
		this.listaFamiliaNumerosa = listaFamiliaNumerosa;
	}

	public String getQuienSolicita() {
		return quienSolicita;
	}

	public void setQuienSolicita(String quienSolicita) {
		this.quienSolicita = quienSolicita;
	}

	public String getDuracionTurno() {
		return duracionTurno;
	}

	public void setDuracionTurno(String duracionTurno) {
		this.duracionTurno = duracionTurno;
	}

	public String[] getSelectedPadece() {
		return selectedPadece;
	}

	public void setSelectedPadece(String[] selectedPadece) {
		this.selectedPadece = selectedPadece;
	}

	public List<String> getSelectedArticAfectadasSolicitante() {
		return selectedArticAfectadasSolicitante;
	}

	public void setSelectedArticAfectadasSolicitante(List<String> selectedArticAfectadasSolicitante) {
		this.selectedArticAfectadasSolicitante = selectedArticAfectadasSolicitante;
	}

	public List<String> getSelectedArticAfectadasConyuge() {
		return selectedArticAfectadasConyuge;
	}

	public void setSelectedArticAfectadasConyuge(List<String> selectedArticAfectadasConyuge) {
		this.selectedArticAfectadasConyuge = selectedArticAfectadasConyuge;
	}

	public List<String> getSelectedTratamientosSolicitante() {
		return selectedTratamientosSolicitante;
	}

	public void setSelectedTratamientosSolicitante(List<String> selectedTratamientosSolicitante) {
		this.selectedTratamientosSolicitante = selectedTratamientosSolicitante;
	}

	public List<String> getSelectedViasSolicitante() {
		return selectedViasSolicitante;
	}

	public void setSelectedViasSolicitante(List<String> selectedViasSolicitante) {
		this.selectedViasSolicitante = selectedViasSolicitante;
	}

	public List<String> getSelectedViasConyuge() {
		return selectedViasConyuge;
	}

	public void setSelectedViasConyuge(List<String> selectedViasConyuge) {
		this.selectedViasConyuge = selectedViasConyuge;
	}

	public List<String> getSelectedConsecuenciasSolicitante() {
		return selectedConsecuenciasSolicitante;
	}

	public void setSelectedConsecuenciasSolicitante(List<String> selectedConsecuenciasSolicitante) {
		this.selectedConsecuenciasSolicitante = selectedConsecuenciasSolicitante;
	}

	public List<String> getSelectedConsecuenciasConyuge() {
		return selectedConsecuenciasConyuge;
	}

	public void setSelectedConsecuenciasConyuge(List<String> selectedConsecuenciasConyuge) {
		this.selectedConsecuenciasConyuge = selectedConsecuenciasConyuge;
	}

	public List<String> getSelectedTratamientosConyuge() {
		return selectedTratamientosConyuge;
	}

	public void setSelectedTratamientosConyuge(List<String> selectedTratamientosConyuge) {
		this.selectedTratamientosConyuge = selectedTratamientosConyuge;
	}

	public Integer[] getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(Integer[] selectedDays) {
		this.selectedDays = selectedDays;
	}

	public Integer[] getSelectedMonths() {
		return selectedMonths;
	}

	public void setSelectedMonths(Integer[] selectedMonths) {
		this.selectedMonths = selectedMonths;
	}

	public SexoVOWS getSexoSol() {
		return sexoSol;
	}

	public void setSexoSol(SexoVOWS sexoSol) {
		this.sexoSol = sexoSol;
	}

	public SexoVOWS getSexoCon() {
		return sexoCon;
	}

	public void setSexoCon(SexoVOWS sexoCon) {
		this.sexoCon = sexoCon;
	}

	public EstadoCivilVOWS getEstadoCivilSol() {
		return estadoCivilSol;
	}

	public void setEstadoCivilSol(EstadoCivilVOWS estadoCivilSol) {
		this.estadoCivilSol = estadoCivilSol;
	}

	public ProvinciaVOWS getProvinciaDomicilio() {
		return provinciaDomicilio;
	}

	public void setProvinciaDomicilio(ProvinciaVOWS provinciaDomicilio) {
		this.provinciaDomicilio = provinciaDomicilio;
	}

	public ProvinciaVOWS getProvinciaNotificacion() {
		return provinciaNotificacion;
	}

	public void setProvinciaNotificacion(ProvinciaVOWS provinciaNotificacion) {
		this.provinciaNotificacion = provinciaNotificacion;
	}



	public List<SimpleDTOI> getListaSexos() {
		return listaSexos;
	}

	public List<SimpleDTOI> getListaProvincias() {
		return listaProvincias;
	}

	public void setListaProvincias(List<SimpleDTOI> listaProvincias) {
		this.listaProvincias = listaProvincias;
	}

	public List<SimpleDTOI> getListaTiposTurno() {
		return listaTiposTurno;
	}

	public SimpleDTOI getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(SimpleDTOI tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	

	public TipoPensionVOWS getTipoPensionSol1() {
		return tipoPensionSol1;
	}

	public void setTipoPensionSol1(TipoPensionVOWS tipoPensionSol1) {
		this.tipoPensionSol1 = tipoPensionSol1;
	}

	public TipoPensionVOWS getTipoPensionSol2() {
		return tipoPensionSol2;
	}

	public void setTipoPensionSol2(TipoPensionVOWS tipoPensionSol2) {
		this.tipoPensionSol2 = tipoPensionSol2;
	}

	public TipoPensionVOWS getTipoPensionCon1() {
		return tipoPensionCon1;
	}

	public void setTipoPensionCon1(TipoPensionVOWS tipoPensionCon1) {
		this.tipoPensionCon1 = tipoPensionCon1;
	}

	public TipoPensionVOWS getTipoPensionCon2() {
		return tipoPensionCon2;
	}

	public void setTipoPensionCon2(TipoPensionVOWS tipoPensionCon2) {
		this.tipoPensionCon2 = tipoPensionCon2;
	}

	public TipoFamiliaNumerosaVOWS getTipoFamiliaNum() {
		return tipoFamiliaNum;
	}

	public void setTipoFamiliaNum(TipoFamiliaNumerosaVOWS tipoFamiliaNum) {
		this.tipoFamiliaNum = tipoFamiliaNum;
	}



	

	public List<String> getSelectedTengoPadezcoSolicitante() {
		return selectedTengoPadezcoSolicitante;
	}

	public void setSelectedTengoPadezcoSolicitante(List<String> selectedTengoPadezcoSolicitante) {
		this.selectedTengoPadezcoSolicitante = selectedTengoPadezcoSolicitante;
	}

	public List<String> getSelectedTengoPadezcoConyuge() {
		return selectedTengoPadezcoConyuge;
	}

	public void setSelectedTengoPadezcoConyuge(List<String> selectedTengoPadezcoConyuge) {
		this.selectedTengoPadezcoConyuge = selectedTengoPadezcoConyuge;
	}

	public Date getConyugeFechaNacimiento() {
		return conyugeFechaNacimiento;
	}

	public void setConyugeFechaNacimiento(Date conyugeFechaNacimiento) {
		this.conyugeFechaNacimiento = conyugeFechaNacimiento;
	}
	public String getSelectedPersonaDeclarante() {
		return selectedPersonaDeclarante;
	}

	public void setSelectedPersonaDeclarante(String selectedPersonaDeclarante) {
		this.selectedPersonaDeclarante = selectedPersonaDeclarante;
	}

	public String getDeclaranteSolicitante() {
		return declaranteSolicitante;
	}

	public void setDeclaranteSolicitante(String declaranteSolicitante) {
		this.declaranteSolicitante = declaranteSolicitante;
	}

	public String getDeclaranteConyuge() {
		return declaranteConyuge;
	}

	public void setDeclaranteConyuge(String declaranteConyuge) {
		this.declaranteConyuge = declaranteConyuge;
	}
	public DatoEconomicoTermalismoDTOI getDatoEconomico() {
		return datoEconomico;
	}

	public void setDatoEconomico(DatoEconomicoTermalismoDTOI datoEconomico) {
		this.datoEconomico = datoEconomico;
	}
	@Override
	public TermalismoDTO getDto() {
		return (TermalismoDTO) dto;
	}
	public SimpleDTOI getTurnoDefecto() {
		return turnoDefecto;
	}

	public void setTurnoDefecto(SimpleDTOI turnoDefecto) {
		this.turnoDefecto = turnoDefecto;
	}
	
	public SimpleDTOI getBalneario() {
		return balneario;
	}

	public void setBalneario(SimpleDTOI balneario) {
		this.balneario = balneario;
	}
	public Long getCcaa() {
		return ccaa;
	}

	public void setCcaa(Long ccaa) {
		this.ccaa = ccaa;
	}
}
