package es.imserso.sede.web.view.alta;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.impl.turismo.DatoEconomicoTurismoDTO;
import es.imserso.sede.data.dto.impl.turismo.TurismoDTO;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.impl.turismo.TurismoSolicitudRegistrationQ;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de genérico
 * 
 * @author 11825775
 *
 */
@Named(value = "solicitudTurismoView")
@ViewScoped
@Secure
public class SolicitudTurismoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 6441965433909918407L;

	@Inject
	Logger log;

	private enum PersonaDeclarante {
		Solicitante, Conyuge
	}

	@Inject
	@TurismoSolicitudRegistrationQ
	private SolicitudRegistrationI turismoSolicitudRegistration;

	@Inject
	TurismoRepository turismoRepository;

	@Inject
	private TurismoDTO dto;

	PlazoDTO plazoDTO;

	// Datos auxiliares
	private List<SimpleDTOI> provincias;
	private List<SimpleDTOI> estadosCiviles;
	private List<SimpleDTOI> categoriasFamiliaNumerosa;
	private List<SimpleDTOI> opciones;
	private List<SimpleDTOI> clasesPension;
	private List<SimpleDTOI> procedenciasPension;
	private List<SimpleDTOI> sexos;

	/**
	 * Prestación económica que se declara
	 */
	private DatoEconomicoDTOI datoEconomico;

	/**
	 * A la hora de declarar una prestación, indica si es del solicitante o del
	 * cónyuge
	 */
	private String selectedPersonaDeclarante;
	private String declaranteSolicitante;
	private String declaranteConyuge;

	@PostConstruct
	public void onCreate() {
		super.init();
		try {

			log.debugv("onCreate!");

			log.debug("obteniendo plazo...");
			plazoDTO = turismoRepository.getPlazoTurnoPorDefecto();
			log.debug("obteniendo turno...");
			log.debug("obteniendo temporada...");

			log.debug("obteniendo datos auxiliares...");
			provincias = turismoRepository.getProvincias();
			estadosCiviles = turismoRepository.getEstadosCiviles();
			categoriasFamiliaNumerosa = turismoRepository.getCategoriasFamiliaNumerosa();
			clasesPension = turismoRepository.getClasesPensiones();
			procedenciasPension = turismoRepository.getProcedenciasPensiones();
			opciones = turismoRepository.getOpciones();
			sexos = turismoRepository.getSexos();

			datoEconomico = new DatoEconomicoTurismoDTO();

			tramite = tramiteRepository.findBySIA(dto.getCodigoSIA());

			declaranteSolicitante = PersonaDeclarante.Solicitante.toString();
			declaranteConyuge = PersonaDeclarante.Conyuge.toString();

		} catch (Exception e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TURISMO,
					"No se pudo crear la instacia del DTO", Utils.getExceptionMessage(e));
		}

	}

	/**
	 * Registra la solicitud en la Sede Electrónica
	 */
	public void save() {
		save(turismoSolicitudRegistration, dto);
	}

	/**
	 * No hace nada
	 */
	public void reinit() {
		datoEconomico = new DatoEconomicoTurismoDTO();
	}

	public void addPensionDeclarante() {
		if (getSelectedPersonaDeclarante().equals(PersonaDeclarante.Solicitante.toString())) {
			addPension(dto.getDatosEconomicos().getDatosEconomicosSolicitante());
		} else {
			addPension(dto.getDatosEconomicos().getDatosEconomicosConyuge());
		}
	}

	private void addPension(List<DatoEconomicoDTOI> datosEconomicosPersona) {
		if (!datoEconomico.isValid()) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Datos insuficientes",
					datoEconomico.getValidationFailedMessage());
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}
		if (datosEconomicosPersona.contains(datoEconomico)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Duplicada",
					"Esta prestación ya se ha añadido");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}

		datosEconomicosPersona.add(datoEconomico);
		datoEconomico = new DatoEconomicoTurismoDTO();
	}

	public List<SimpleDTOI> getProvincias() {
		return provincias;
	}

	public List<SimpleDTOI> getEstadosCiviles() {
		return estadosCiviles;
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() {
		return categoriasFamiliaNumerosa;
	}

	public List<SimpleDTOI> getClasesPensiones() {
		return clasesPension;
	}

	public List<SimpleDTOI> getProcedenciasPensiones() {
		return procedenciasPension;
	}

	public List<SimpleDTOI> getOpciones() {
		return opciones;
	}

	public List<SimpleDTOI> getSexos() {
		return sexos;
	}

	public PlazoDTO getPlazoTurnoPorDefecto() {
		return plazoDTO;
	}

	public TurismoDTO getDto() {
		return (TurismoDTO) dto;
	}

	public DatoEconomicoDTOI getDatoEconomico() {
		return datoEconomico;
	}

	public void setDatoEconomico(DatoEconomicoDTOI datoEconomico) {
		this.datoEconomico = datoEconomico;
	}

	public String getSelectedPersonaDeclarante() {
		return selectedPersonaDeclarante;
	}

	public void setSelectedPersonaDeclarante(String selectedPersonaDeclarante) {
		this.selectedPersonaDeclarante = selectedPersonaDeclarante;
	}

	public String getDeclaranteSolicitante() {
		return declaranteSolicitante;
	}

	public void setDeclaranteSolicitante(String declaranteSolicitante) {
		this.declaranteSolicitante = declaranteSolicitante;
	}

	public String getDeclaranteConyuge() {
		return declaranteConyuge;
	}

	public void setDeclaranteConyuge(String declaranteConyuge) {
		this.declaranteConyuge = declaranteConyuge;
	}

}