package es.imserso.sede.web.util.socket;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.websocket.CloseReason.CloseCode;

import org.jboss.logging.Logger;
import org.omnifaces.cdi.push.SocketEvent;
import org.omnifaces.cdi.push.SocketEvent.Closed;
import org.omnifaces.cdi.push.SocketEvent.Opened;

@ApplicationScoped
public class SocketObserver {
	
	Logger log = Logger.getLogger(SocketObserver.class);

    public void onOpen(@Observes @Opened SocketEvent event) {
        String channel = event.getChannel(); // Returns <o:socket channel>.
        Long userId = event.getUser(); // Returns <o:socket user>, if any.
        
        log.infov("opened socket on channel {0} and user id {1}!", channel, userId);
        // Do your thing with it. E.g. collecting them in a concurrent/synchronized collection.
        // Do note that a single person can open multiple sockets on same channel/user.
    }

    public void onClose(@Observes @Closed SocketEvent event) {
        String channel = event.getChannel(); // Returns <o:socket channel>.
        Long userId = event.getUser(); // Returns <o:socket user>, if any.
        
        log.infov("opened closed on channel {0} and user id {1}!", channel, userId);
        
        CloseCode code = event.getCloseCode(); // Returns close reason code.
        // Do your thing with it. E.g. removing them from collection.
        log.warn(code.toString());
    }

}