package es.imserso.sede.web.util.route;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;

import es.imserso.sede.model.TipoAccion;
import es.imserso.sede.model.TipoTramite;

@Dependent
@Named("paramValues")
public class ParamValues implements Serializable {

	private static final long serialVersionUID = -7327551726696067761L;

	@Inject
	Logger log;

	private Map<Params, ParamValue> paramValues;

	/**
	 * Parámetros que puede contener la petición
	 * 
	 * @author 11825775
	 *
	 */
	public enum Params {
		sia, action, representante, locale, aut
	}

	@PostConstruct
	public void onCreate() {
		paramValues = new HashMap<Params, ParamValue>();
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		for (Params param : Params.values()) {
			paramValues.put(param, new ParamValue(request.getParameter(param.toString())));
		}

	}

	public ParamValue getParamValue(Params param) {
		return getParamValues().get(param);
	}

	public Map<Params, ParamValue> getParamValues() {
		return paramValues;
	}

	/**
	 * @return accion especificada en los parámetros de la URI
	 */
	public TipoAccion getTipoAccion() {
		String value = getParamValue(Params.action).getValue();
		return value == null ? null : TipoAccion.valueOf(value);
	}

	/**
	 * @return Tipo de Trámite (TURISMO, TERMALISMO, PROPOSITO_GENERAL o GENERICO)
	 *         determinado a partir del parámetro 'sia' de la URI
	 */
	public TipoTramite getTipoTramite() {
		return TipoTramite.getTipoTramite(getParamValue(Params.sia).getValue());
	}

	/**
	 * @return Código sia recibido en el parámtro 'sia' de la URI
	 */
	public String getSia() {
		return getParamValue(Params.sia).getValue();
	}

	public void setParamValues(Map<Params, ParamValue> paramValues) {
		this.paramValues = paramValues;
	}

	public boolean isTurismo() {
		ParamValue paramSIA = getParamValues().get(Params.sia);
		return (!paramSIA.isEmpty() && paramSIA.getValue().equals(TipoTramite.TURISMO.getSia()));
	}

	public boolean isTermalismo() {
		ParamValue paramSIA = getParamValues().get(Params.sia);
		return (!paramSIA.isEmpty() && paramSIA.getValue().equals(TipoTramite.TERMALISMO.getSia()));
	}

	public boolean isPropositoGeneral() {
		ParamValue paramSIA = getParamValues().get(Params.sia);
		return (!paramSIA.isEmpty() && paramSIA.getValue().equals(TipoTramite.PROPOSITO_GENERAL.getSia()));
	}

	// No tendremos un método isGenerico pues consideraremos Genérico cualquier
	// trámite que no sea Turismo, Termalismo o Propósito General
	// public boolean isGenerico() {
	// ParamValue paramSIA = getParamValues().get(Params.sia);
	// return (!paramSIA.isEmpty() &&
	// paramSIA.getValue().equals(TipoTramite.GENERICO.getSia()));
	// }

	public boolean isRepresentante() {
		ParamValue paramRepresentanteA = getParamValues().get(Params.representante);
		return (!paramRepresentanteA.isEmpty() && paramRepresentanteA.getValue().equals("si"));
	}

	public boolean isAlta() {
		ParamValue paramAction = getParamValues().get(Params.action);
		return (!paramAction.isEmpty() && paramAction.getValue().equals(TipoAccion.alta.toString()));
	}

	public boolean isEdicion() {
		ParamValue paramAction = getParamValues().get(Params.action);
		return (!paramAction.isEmpty() && paramAction.getValue().equals(TipoAccion.edicion.toString()));
	}

	public boolean isConsulta() {
		ParamValue paramAction = getParamValues().get(Params.action);
		return (!paramAction.isEmpty() && paramAction.getValue().equals(TipoAccion.consulta.toString()));
	}

	public boolean isAuthenticationRequired() {
		ParamValue paramAction = getParamValues().get(Params.aut);
		return (!paramAction.isEmpty() && paramAction.getValue().equals("si"));
	}

}
