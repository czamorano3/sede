package es.imserso.sede.web.model.validator;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Configuration;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.service.ucm.UCMService;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;

public abstract class DtoValidator implements DtoValidatorI, Serializable {

	protected static final long serialVersionUID = 1934281065780884560L;

	@Inject
	Logger log;

	@Inject
	UCMService UCMservice;

	@Inject
	@ResourceQ
	Usuario usuario;

	@Inject
	ParamValues paramValues;
	
	@Inject
	PropertyComponent propertyComponent;

	/**
	 * Indica si se ha realizado ya la validación
	 */
	Boolean validated;
	/**
	 * Indica si ha pasado correctamente las validaciones
	 */
	Boolean validatedOK;
	/**
	 * Indica si ha pasado correctamente las validaciones
	 */
	Boolean validatedKO;

	Validator validator;

	@PostConstruct
	public void onCreate() {
		validated = Boolean.FALSE;
		validatedOK = Boolean.FALSE;
		validatedKO = Boolean.FALSE;

		Configuration<?> config = Validation.byDefaultProvider().configure();
		ValidatorFactory factory = config.buildValidatorFactory();
		validator = factory.getValidator();
		factory.close();
	}

}
