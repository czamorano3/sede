package es.imserso.sede.web.view.log;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.jboss.logging.annotations.Cause;
import org.jboss.logging.annotations.LogMessage;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageLogger;

@MessageLogger(projectCode = "SEDECDI")
public interface FormValidationLogger {
	FormValidationLogger LOGGER = Logger.getMessageLogger(FormValidationLogger.class,
			FormValidationLogger.class.getPackage().getName());

	@LogMessage(level = Level.ERROR)
	@Message(id = 3, value = "Invalid date passed as string: %s")
	void logStringCouldntParseAsDate(String dateString, @Cause Throwable exception);

	@LogMessage
	@Message(id = 4, value = "Requested number of days until '%s'")
	void logDaysUntilRequest(String dateString);

}
