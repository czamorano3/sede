package es.imserso.sede.web.util.socket;

import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

@Named
public class PushMock {
	
	@Inject @Push(channel="foo")
	private PushContext someChannel;
	
	public void sendMessage() {
	    String message = "ZZZZZZZZZZZZZXXXXXXXXXXXXXXXXX";
	    someChannel.send(message);
	}

}
