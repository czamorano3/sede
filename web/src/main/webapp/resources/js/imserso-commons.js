/**
 * Cambiar el skin de la aplicación.
 * 
 * @param skinName
 *            Nombre del skin
 */
function setSkinValue(skinName) {
	Seam.Component.getInstance('skinBean').setSkin(skinName, function() {
		Seam.Remoting.log('Cambiando el aspecto');
		window.location.reload(false);
	});
}


function isMSIE() {
	return '\v' == 'v';
}


