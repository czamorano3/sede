package es.imserso.sedecdi.test;

import static org.junit.Assert.assertNotNull;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.imserso.sede.util.Greeter;

@RunWith(Arquillian.class)
public class GreeterTest {

	@Deployment
	public static JavaArchive createDeployment() {
		System.out.println("*************** GreeterTest *******************");
		JavaArchive ja =ShrinkWrap.create(JavaArchive.class).addClass(Greeter.class).addAsManifestResource(EmptyAsset.INSTANCE,
				"beans.xml");
		System.out.println(ja.toString());
		return ja;
	}

	@Test
	public void should_create_greeting() {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//		Assert.fail("Not yet implemented");
		assertNotNull("");
		System.out.println("\n\n *****  FIN DEL TEST *****\n\n");
	}
}