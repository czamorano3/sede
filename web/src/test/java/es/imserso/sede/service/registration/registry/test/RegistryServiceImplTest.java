package es.imserso.sede.service.registration.registry.test;

import static org.junit.Assert.assertNotNull;

import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.DocumentoRegistradoI;
import es.imserso.sede.PackageRoot;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;
import es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI;
import es.imserso.sede.util.Utils;

@RunWith(Arquillian.class)
public class RegistryServiceImplTest {

	private static int BOOK_YEAR = 2017;

	@Deployment
	public static WebArchive createTestArchive() {
		System.out.println("\n\n************************* Lanzando RegistryServiceImplTest ***************\n\n");

		WebArchive archive = ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, PackageRoot.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

		System.out.println(archive.toString(true));

		return archive;
	}

	@Inject
	RegistryServiceI registryService;

	@Inject
	PropertyComponent propertyCompnent;

	@Inject
	Logger log;

	@Test
	@InSequence(1)
	public void testNewInputRegister() throws Exception {
		log.info("iniciando test de registro de nueva solicitud ...");
		try {
			log.debug("preparando los archivos adjuntos...");
			List<DocumentoRegistradoI> attachedFiles = new ArrayList<DocumentoRegistradoI>();
			attachedFiles.add(new DocumentoRegistrado("certificado minusvalía", "certificado minusvalia.docx",
					Utils.getBytesFromFile(Paths.get(propertyCompnent.getTestFolder()
							+ "/entrada/ISWebServiceRegistration/certificado minusvalia.docx"))));
			attachedFiles.add(new DocumentoRegistrado("parte médico", "parte medico.docx", Utils.getBytesFromFile(Paths
					.get(propertyCompnent.getTestFolder() + "/entrada/ISWebServiceRegistration/parte medico.docx"))));

			log.debug("preparando el pdf de la solicitud...");
			byte[] solicitudPdf = Utils.getBytesFromFile(Paths.get(propertyCompnent.getTestFolder()
					+ "/entrada/ISWebServiceRegistration/Formulario de Turismo del Imserso.pdf"));

			InputRegisterResponseI newRegisterResponse = registryService.registerNewSolicitud(BOOK_YEAR, attachedFiles,
					solicitudPdf, 1, "xxx");
			assertNotNull("", newRegisterResponse);
			log.info("el documento ha sido registrado!");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		System.out.println("\n\n *****  FIN DEL TEST *****\n\n");
	}

	@Test
	@InSequence(2)
	public void testGetInputRegister() throws Exception {
		log.info("iniciando test de obtención de documento registrado ...");
		try {

			log.debug("obteniendo entrada 16 ...");

			InputRegisterI newRegistryInput = registryService.getInputRegister(BOOK_YEAR, 16);
			log.info("la entrada ha sido recuperada del registro!");
			assertNotNull("el objeto recibido es nulo!", newRegistryInput);

			log.info(newRegistryInput.toString());
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		System.out.println("\n\n *****  FIN DEL TEST *****\n\n");
	}

	@Test
	@InSequence(3)
	public void testGetAttachedDocument() throws Exception {
		log.info("iniciando test de obtención de documento adjunto ...");
		try {

			log.debug("obteniendo documento 16 ...");

			byte[] documentoObtenido = registryService.getAttachedDocument(BOOK_YEAR, 16, 1, 1);
			log.info("el documento adjunto ha sido recuperado del registro!");

			log.info("escribiendo bytes en fichero...");
			FileOutputStream fos = new FileOutputStream(
					propertyCompnent.getTestFolder() + "/entrada/ISWebServiceRegistration/documentorecuperado.pdf");
			fos.write(documentoObtenido);
			fos.close();

			assertNotNull("", documentoObtenido);
			log.info("el documento adjunto ha sido recuperado del registro!");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		System.out.println("\n\n *****  FIN DEL TEST *****\n\n");
	}

}
